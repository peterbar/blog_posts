---
title: "Working with Statistics Canada Data in R, Part 2: Retrieving CANSIM Data"
date: "2019-11-15T00:00:00"
slug: "statcan-data-in-r-part-2-retrieving-cansim-data"
excerpt: "A detailed tutorial on using R to retrieve and clean Statistics Canada CANSIM data, step-by-step."
status: "publish"
output: hugodown::md_document
categories: Statistics Canada
tags:
  - cansim (package)
  - Statistics Canada Data
  - Consumer Price Index (CPI)
  - inflation adjustment
comment_status: open
ping_status: open
rmd_hash: 3cb52aab3b9f6540

---

Most CANSIM data can be accessed in two formats: as data tables and, at a much more granular level, as individual data vectors. This post is structured accordingly:

<a name="searching-for-data"></a> Searching for Data
----------------------------------------------------

CANSIM data is stored in tables containing data on a specific subject. Individual entries (rows) and groups of entries in these tables are usually assigned vector codes. Thus, unless we already know what table or vector we need, finding the correct table should be our first step.

As always, start with loading packages:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='s'><a href='https://github.com/mountainMath/cansim'>"cansim"</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='s'><a href='http://tidyverse.tidyverse.org'>"tidyverse"</a></span><span class='o'>)</span>
</code></pre>

</div>

Now we can start retrieving CANSIM data. How we do this, depends on whether we already know the table or vector numbers. If we do, things are simple: just use [`get_cansim()`](https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html) to [retrieve data tables](#retrieving-data-tables), or [`get_cansim_vector()`](https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html) to retrieve vectors.

But usually we don't. In this case one option is to use StatCan's online search tool. Eventually you will find what you've been looking for, although you might also miss a few things -- CANSIM is not that easy to search and work with manually.

A much better option is to let R do the tedious work for you with a few lines of code.

### <a name="searching-by-index"></a> Searching by Index

In this example (based on the <a href="https://gitlab.com/peterbar/aborig_sk_economy" target="_blank">code</a> I wrote for a research project), let's look for CANSIM tables that refer to Aboriginal (Indigenous) Canadians anywhere in the tables' titles, descriptions, keywords, notes, etc.:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># create an index to subset list_cansim_tables() output</span>
<span class='nv'>index</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html'>list_cansim_tables</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/funprog.html'>Map</a></span><span class='o'>(</span><span class='nv'>grepl</span>, <span class='s'>"(?i)aboriginal|(?i)indigenous"</span>, <span class='nv'>.</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/funprog.html'>Reduce</a></span><span class='o'>(</span><span class='s'>"|"</span>, <span class='nv'>.</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/which.html'>which</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='c'># list all tables with Aboriginal data, drop redundant cols</span>
<span class='nv'>tables</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html'>list_cansim_tables</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>[</span><span class='nv'>index</span>, <span class='o'>]</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"title"</span>, <span class='s'>"keywords"</span>, <span class='s'>"notes"</span>, <span class='s'>"subject"</span>,
           <span class='s'>"date_published"</span>, <span class='s'>"time_period_coverage_start"</span>,
           <span class='s'>"time_period_coverage_end"</span>, <span class='s'>"url_en"</span>, 
           <span class='s'>"cansim_table_number"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Let's look in detail at what this code does. First, we call the [`list_cansim_tables()`](https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html) function, which returns a <a href="https://blog.rstudio.com/2016/03/24/tibble-1-0-0/" target="_blank">tibble</a> dataframe, where each row provides a description of one CANSIM table. To get a better idea of [`list_cansim_tables()`](https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html) output, run:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>glimpse</span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html'>list_cansim_tables</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Then we search through the dataframe for Regex patterns matching our keywords. Note the `(?i)` flag -- it tells Regex to ignore case when searching for patterns; alternatively, you can pass `ignore.case = TRUE` argument to [`grepl()`](https://rdrr.io/r/base/grep.html). The [`Map()`](https://rdrr.io/r/base/funprog.html) function allows to search for patterns in every column of the dataframe returned by [`list_cansim_tables()`](https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html). This step returns a very long list of logical values.

Our next step is to [`Reduce()`](https://rdrr.io/r/base/funprog.html) the list to a logical vector, where each value is either `FALSE` if there was not a single search term match per CANSIM table description (i.e. per row of list\_cansim\_tables output), or `TRUE` if there were one or more matches. The [`which()`](https://rdrr.io/r/base/which.html) function gives us the numeric indices of `TRUE` elements in the vector.

Finally, we subset the [`list_cansim_tables()`](https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html) output by index. Since there are many redundant columns in the resulting dataframe, we select only the ones that contain potentially useful information.

### <a name="searching-with-filter"></a> Searching with dplyr::filter

There is also a simpler approach, which immediately returns a dataframe of tables:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>tables</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html'>list_cansim_tables</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"(?i)aboriginal|(?i)indigenous"</span>, <span class='nv'>title</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>select</span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"title"</span>, <span class='s'>"keywords"</span>, <span class='s'>"notes"</span>, <span class='s'>"subject"</span>,
           <span class='s'>"date_published"</span>, <span class='s'>"time_period_coverage_start"</span>,
           <span class='s'>"time_period_coverage_end"</span>, <span class='s'>"url_en"</span>, 
           <span class='s'>"cansim_table_number"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

However, keep in mind that this code would search only through the column or columns of the [`list_cansim_tables()`](https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html) output, which were specified inside the [`grepl()`](https://rdrr.io/r/base/grep.html) call (in this case, in the title column). This results in fewer tables listed in `tables`: 60 vs 73 you get if you [search by index](#searching-by-index) (as of the time of writing this). Often simple filtering would be sufficient, but if you want to be extra certain you haven't missed anything, search by index as shown above.

( ! ) Note that some CANSIM data tables do not get updated after the initial release, so always check the `date_published` and `time_period_coverage_*` attributes of the tables you are working with.

### <a name="saving-search-results"></a> Saving Search Results

Finally, it could be a good idea to externally save the dataframe with the descriptions of CANSIM data tables in order to be able to view it as a spreadsheet. Before saving, let's re-arrange the columns in a more logical order for viewers' convenience, and sort the dataframe by CANSIM table number.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># re-arrange columns for viewing convenience</span>
<span class='nv'>tables</span> <span class='o'>&lt;-</span> <span class='nv'>tables</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"cansim_table_number"</span>, <span class='s'>"title"</span>, <span class='s'>"subject"</span>, 
                   <span class='s'>"keywords"</span>, <span class='s'>"notes"</span>, <span class='s'>"date_published"</span>, 
                   <span class='s'>"time_period_coverage_start"</span>, 
                   <span class='s'>"time_period_coverage_end"</span>, <span class='s'>"url_en"</span><span class='o'>)</span><span class='o'>]</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>arrange</span><span class='o'>(</span><span class='nv'>cansim_table_number</span><span class='o'>)</span>

<span class='c'># and save externally</span>
<span class='nf'>write_delim</span><span class='o'>(</span><span class='nv'>tables</span>, <span class='s'>"selected_data_tables.txt"</span>, delim <span class='o'>=</span> <span class='s'>"|"</span><span class='o'>)</span>
</code></pre>

</div>

( ! ) Note that I am using `write_delim()` function instead of a standard [`write.csv()`](https://rdrr.io/r/utils/write.table.html) or `tidyverse::write_csv()`, with [`|`](https://rdrr.io/r/base/Logic.html) as a delimiter. I am doing this because there are many commas inside strings in CANSIM data, and saving as a comma-separated file would cause incorrect breaking down into columns.

Now, finding the data tables can be as simple as looking through the tables `dataframe` or through the `selected_data_tables.txt` file.

<a name="retrieving-data-tables"></a> Retrieving Data Tables
------------------------------------------------------------

In order for the examples here to feel relevant and practical, let's suppose we were asked to compare and visualize the weekly wages of Aboriginal and Non-Aboriginal Canadians of 25 years and older, living in a specific province (let's say, Saskatchewan), adjusted for inflation.

Since we have [already](#searching-by-index) searched for all CANSIM tables that contain data about Aboriginal Canadians, we can easily figure out that we need CANSIM table \#14-10-0370. Let's retrieve it:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html'>get_cansim</a></span><span class='o'>(</span><span class='s'>"14-10-0370"</span><span class='o'>)</span>
</code></pre>

</div>

Note that a few CANSIM tables are too large to be downloaded and processed in R as a single dataset. However, [below](#stc-search-tool) I'll show you a simple way how you can work with them.

### <a name="cleaning-data-tables"></a> Cleaning Data Tables

CANSIM tables have <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-part-1-cansim/#what-to-expect" target="_blank">a lot of redundant data</a>, so let's quickly examine the dataset to decide which variables can be safely dropped in order to make working with the dataset more convenient:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>wages_0370</span><span class='o'>)</span>
<span class='nf'>glimpse</span><span class='o'>(</span><span class='nv'>wages_0370</span><span class='o'>)</span>
</code></pre>

</div>

( ! ) Before we proceed further, take a look at the `VECTOR` variable -- this is how we can find out individual vector codes for specific CANSIM data. More on that [below](#retrieving-data-vectors).

Let's now subset the data by province, drop redundant variables, and rename the remaining in a way that makes them easier to process in the R language. Generally, I suggest following <a href="https://style.tidyverse.org/" target="_blank">The tidyverse Style Guide</a> by Hadley Wickham. For instance, variable names should use only lowercase letters, numbers, and underscores instead of spaces:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nv'>wages_0370</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>GEO</span> <span class='o'>==</span> <span class='s'>"Saskatchewan"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>7</span><span class='o'>:</span><span class='m'>12</span>, <span class='m'>14</span><span class='o'>:</span><span class='m'>24</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"year"</span>, <span class='s'>"group"</span>, <span class='s'>"type"</span>, <span class='s'>"age"</span>, <span class='s'>"current_dollars"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Next, let's explore the dataset again. Specifically, let's see what unique values categorical variables `year`, `group`, `type`, `age` have. No need to do this with `current_dollars`, as all or most of its values would inevitably be unique due to it being a continuous variable.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>map</span><span class='o'>(</span><span class='nv'>wages_0370</span><span class='o'>[</span><span class='m'>1</span><span class='o'>:</span><span class='m'>4</span><span class='o'>]</span>, <span class='nv'>unique</span><span class='o'>)</span>
</code></pre>

</div>

The output looks as follows:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'>#&gt; $ year [1] “2007” “2008” “2009” “2010” “2011” “2012” “2013” “2014” “2015” “2016” “2017” “2018”</span>
<span class='c'>#&gt; $ group [1] “Total population” “Aboriginal population” “First Nations” “Métis” “Non-Aboriginal population”</span>
<span class='c'>#&gt; $ type [1] “Total employees” “Average hourly wage rate” “Average weekly wage rate” “Average usual weekly hours”</span>
<span class='c'>#&gt; $ age [1] “15 years and over” “15 to 64 years” “15 to 24 years” “25 years and over” “25 to 54 years”</span>
</code></pre>

</div>

Now we can decide how to further subset the data.

We obviously need data for as many years as we can get, so we keep all the years from the `year` variable.

For the `group` variable, we need Aboriginal and Non-Aboriginal data, but the "Aboriginal" category has two sub-categories: "First Nations" and "Métis". It is our judgment call which to go with. Let's say we want our data to be more granular and choose "First Nations", "Métis", and "Non-Aboriginal population".

As far as the `type` variable is concerned, things are simple: we are only interested in the weekly wages, i.e. "Average weekly wage rate". Note that we are using the data on average wages because for some reason CANSIM doesn't provide the data on median wages for Aboriginal Canadians. Using average wages is not a commonly accepted way of analyzing wages, as it allows a small number of people with very high-paying jobs to distort the data, making wages look higher than they actually are. This happens because the mean is highly sensitive to large outliers, while the median is not. But well, one can only work with the data one has.

Finally, we need only one age group: "25 years and over".

Having made these decisions, we can subset the data. We also drop two categorical variables (`type` and `age`) we no longer need, as both these variables would now have only one level each:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nv'>wages_0370</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"25 years"</span>, <span class='nv'>age</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"First Nations|Métis|Non-Aboriginal"</span>, <span class='nv'>group</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"weekly wage"</span>, <span class='nv'>type</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"type"</span>, <span class='s'>"age"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

### <a name="using-pipe"></a> Using Pipe to Stitch Code Together

What I just did step-by-step for demonstration purposes, can be done with a single block of code to minimize typing and remove unnecessary repetition. The "pipe" operator `%>%` makes this super-easy. In R-Studio, you can use `Shift+Ctrl+M` shortcut to insert `%>%`:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html'>get_cansim</a></span><span class='o'>(</span><span class='s'>"14-10-0370"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>GEO</span> <span class='o'>==</span> <span class='s'>"Saskatchewan"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>7</span><span class='o'>:</span><span class='m'>12</span>, <span class='m'>14</span><span class='o'>:</span><span class='m'>24</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"year"</span>, <span class='s'>"group"</span>, <span class='s'>"type"</span>, 
             <span class='s'>"age"</span>, <span class='s'>"current_dollars"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"25 years"</span>, <span class='nv'>age</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"First Nations|Métis|Non-Aboriginal"</span>, <span class='nv'>group</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"weekly wage"</span>, <span class='nv'>type</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"type"</span>, <span class='s'>"age"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

<a name="retrieving-data-vectors"></a>Retrieving Data Vectors
-------------------------------------------------------------

### <a name="finding-vectors"></a> How to Find the Right Vector

Now that we have our weekly wages data, let's adjust the wages for inflation, otherwise the data simply won't be meaningful. In order to be able to do this, we need to get the information about the annual changes in the <a href="https://en.wikipedia.org/wiki/Consumer_price_index" target="_blank">Consumer Price Index (CPI)</a> in Canada, since the annual change in CPI is used as a measure of inflation. Let's take a look at what CANSIM has on the subject:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># list tables with CPI data, exclude the US</span>
<span class='nv'>cpi_tables</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/list_cansim_tables.html'>list_cansim_tables</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"Consumer Price Index"</span>, <span class='nv'>title</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"United States"</span>, <span class='nv'>title</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Even when we search using [`filter()`](https://rdrr.io/r/stats/filter.html) instead of indexing, and remove the U.S. data from search results, we still get a list of 20 CANSIM tables with multiple vectors in each. How do we choose the correct data vector? There are two main ways we can approach this.

First, we can use other sources to find out exactly which vectors to use. For example, we can take a look at how the Bank of Canada calculates inflation. According to the Bank of Canada's "Inflation Calculator" web page, they use CANSIM vector v41690973 (Monthly CPI Indexes for Canada) to calculate inflation rates. So we can go ahead and retrieve this vector:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># retrieve vector data</span>
<span class='nv'>cpi_monthly</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html'>get_cansim_vector</a></span><span class='o'>(</span><span class='s'>"v41690973"</span>, 
                                 start_time <span class='o'>=</span> <span class='s'>"2007-01-01"</span>, 
                                 end_time <span class='o'>=</span> <span class='s'>"2018-12-31"</span><span class='o'>)</span>
</code></pre>

</div>

Since the data in the `wages_0370` dataset covers the period from 2007 till 2018, we retrieve CPI data for the same period. The function takes two main arguments: `vectors` -- the list of vector numbers (as strings), and `start_time` -- starting date as a string in YYYY-MM-DD format. Since we don't need data past 2018, we also add an optional `end_time` argument. Let's take a look at the result of our [`get_cansim_vector()`](https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html) call:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>glimpse</span><span class='o'>(</span><span class='nv'>cpi_monthly</span><span class='o'>)</span>
</code></pre>

</div>

The resulting dataset contains monthly CPI indexes (take a look at the `REF_DATE` variable). However, our `wages_0370` dataset only has the annual data on wages. What shall we do?

Well, one option could be to calculate annual CPI averages ourselves:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># calculate mean annual CPI values</span>
<span class='nv'>cpi_annual</span> <span class='o'>&lt;-</span> <span class='nv'>cpi_monthly</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>year <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>REF_DATE</span>, <span class='s'>"-.*-01"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>year</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>transmute</span><span class='o'>(</span>cpi <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>VALUE</span><span class='o'>)</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Alternatively, we could look through CANSIM tables to find annual CPI values that have already been calculated by Statistics Canada.

*Thus, the second way to find which vectors to use, is by looking through the relevant CANSIM tables*. This might be more labor-intensive, but can lead to more precise results. Also, we can do this if we can't find vector numbers from other sources.

Let's look at `cpi_tables`. Table \# 18-10-0005 has "Consumer Price Index, annual average" in its title, so this is probably what we need.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get CANSIM table with annual CPI values</span>
<span class='nv'>cpi_annual_table</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html'>get_cansim</a></span><span class='o'>(</span><span class='s'>"18-10-0005"</span><span class='o'>)</span>
</code></pre>

</div>

Let's now explore the data:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># explore the data</span>
<span class='nf'>map</span><span class='o'>(</span><span class='nv'>cpi_annual_table</span><span class='o'>[</span><span class='m'>1</span><span class='o'>:</span><span class='m'>4</span><span class='o'>]</span>, <span class='nv'>unique</span><span class='o'>)</span>
<span class='c'># unique(cpi_annual_table$VECTOR)</span>
</code></pre>

</div>

Turns out, the data is much more detailed than in the vector v41690973. Remember that in `wages_0370` we selected the data for a specific province (Saskatchewan)? Well, table \# 18-10-0005 has CPI breakdown by province and even by specific groups of products. This is just what we need! However, if you run [`unique(cpi_annual_table$VECTOR)`](https://rdrr.io/r/base/unique.html), you'll see that table \# 18-10-0005 includes data from over 2000 different vectors -- it is a large dataset. So, how do we choose the right vector? By narrowing down the search:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># find out vector number from CANSIM table</span>
<span class='nv'>cpi_annual_table</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>product <span class='o'>=</span> <span class='s'>"Products and product groups"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>GEO</span> <span class='o'>==</span> <span class='s'>"Saskatchewan"</span> <span class='o'>&amp;</span>
         <span class='nv'>product</span> <span class='o'>==</span> <span class='s'>"All-items"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='nv'>VECTOR</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

This gives us CANSIM vector number for the "all items" group CPI for the province of Saskatchewan: v41694489.

### <a name="stc-search-tool"></a> Using StatCan Data Search Tool to Find Vectors

Some CANSIM tables are too large to be retrieved using `cansim` package. For example, running `get_cansim(“12-10-0136”)` will result in a long wait followed by "Problem downloading data, multiple timeouts" error. `cansim` will also advise you to "check your network connection", but network connection is not the problem, it is the size of the dataset.

CANSIM table 12-10-0136 is *very* large: 16.2 GB. By default, R loads the full dataset into RAM, which can make things painfully slow when dealing with huge datasets. There are <a href="https://rpubs.com/msundar/large_data_analysis" target="_blank">solutions for datasets &lt;10 GB</a> in size, but anything larger than 10 GB requires either distributed computing, or retrieving data chunk-by-chunk. In practice, in R you are likely to start experiencing difficulties and slowdowns if your datasets exceed 1 GB.

Suppose we need to know how much wheat (in dollars) Canada exports to all other countries. CANSIM table 12-10-0136 "Canadian international merchandise trade by industry for all countries" has this data. But how do we get the data from this table if we can't directly read it into R, and even if we manually download and unzip the dataset, R won't be able to handle 16.2 GB of data?

This is where CANSIM data vectors come to the rescue. We need to get only one vector instead of the whole enormous table. To do that, we need to know the vector's number, but we can't look for it inside the table because the table is too large.

The solution is to find the correct vector using <a href="https://www150.statcan.gc.ca/n1/en/type/data" target="_blank">Statistics Canada Data search tool</a>. Start with entering the table number in the "Keyword(s)" field. Obviously, you can search by keywords too, but searching by table number is more precise:

<div class="highlight">

<img src="figs/stc-search-field.png" width="503px" style="display: block; margin: auto auto auto 0;" />

</div>

Then click on the name of the table: "Canadian international merchandise trade by industry for all countries". After the table preview opens, click "Add/Remove data":

<div class="highlight">

<img src="figs/stc-add-remove-data.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

The "Customize table (Add/Remove data)" menu will open. It has the following tabs: "Geography", "Trade", "Trading Partners", "North American Industry Classification System (NAICS)", "Reference Period", and "Customize Layout". Note that the selection of tabs depends on the data in the table.

<div class="highlight">

<img src="figs/stc-customize-table.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

Now, do the following:

In the "Geography" tab, do nothing -- just make sure "Canada" is checked.

1.  In the "Trade" tab, uncheck "Imports", since we are looking for the exports data.
2.  In the "Trading Partners", check "Total of all countries" and uncheck everything else.
3.  Skip "Reference period" for now.
4.  In "Customize Layout", check "Display Vector Identifier and Coordinate".
5.  Finally, in the "North American Industry Classification System (NAICS)" tab, uncheck "Total of all industries", find our product of interest -- wheat -- and check it. Then click "Apply".

<div class="highlight">

<img src="figs/stc-naics.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

If you followed all the steps, here's what your output should look like:

<div class="highlight">

<img src="figs/stc-output.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

The vector number for Canada's wheat exports to all other countries is v1063958702.

Did you notice that the output on screen has data only for a few months in 2019? This is just a sample of what our vector has. If you click "Reference period", you'll see that the table 12-10-0136 has data for the period from January 2002 to October 2019:

<div class="highlight">

<img src="figs/stc-refdate.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

Now we can retrieve the data we've been looking for:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get and clean wheat exports data</span>
<span class='nv'>wheat_exports</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html'>get_cansim_vector</a></span><span class='o'>(</span><span class='s'>"v1063958702"</span>, <span class='s'>"2002-01-01"</span>, <span class='s'>"2019-10-31"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>mutate</span><span class='o'>(</span>ref_date <span class='o'>=</span> <span class='nf'>lubridate</span><span class='nf'>::</span><span class='nf'><a href='http://lubridate.tidyverse.org/reference/ymd.html'>ymd</a></span><span class='o'>(</span><span class='nv'>REF_DATE</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>dollars <span class='o'>=</span> <span class='nv'>VALUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>3</span><span class='o'>:</span><span class='m'>9</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'># check object size</span>
<span class='nf'><a href='https://rdrr.io/r/utils/object.size.html'>object.size</a></span><span class='o'>(</span><span class='nv'>wheat_exports</span><span class='o'>)</span>
</code></pre>

</div>

The resulting `wheat_exports` object is only 4640 bytes in size: about 3,500,000 times smaller than the table it came from!

Note that I used [`lubridate::ymd()`](http://lubridate.tidyverse.org/reference/ymd.html) function inside the `mutate()` call. This is not strictly required, but `wheat_exports` contains a time series, so it makes sense to convert the reference date column to an object of class "Date". Since `lubridate` is not loaded with `tidyverse` (it is part of the `tidyverse` ecosystem, but only core components are loaded by default), I had to call the `ymd()` function with `lubridate::`.

Finally, note that StatCan Data has another search tool that allows you to search by vector numbers. Unlike the data tables search tool, the vector search tool is very simple, so I'll not be covering it in detail. You can find it <a href="https://www150.statcan.gc.ca/t1/tbl1/en/sbv.action" target="_blank">here</a>.

### <a name="cleaning-vector-data"></a> Cleaning Vector Data

Let's now get provincial annual CPI data and clean it up a bit, removing all the redundant stuff and changing the `VALUE` variable name to something in line with <a href="https://style.tidyverse.org/" target="_blank">The tidyverse Style Guide</a>:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get mean annual CPI for Saskatchewan, clean up data</span>
<span class='nv'>cpi_sk</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html'>get_cansim_vector</a></span><span class='o'>(</span><span class='s'>"v41694489"</span>, <span class='s'>"2007-01-01"</span>, <span class='s'>"2018-12-31"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>year <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>REF_DATE</span>, <span class='s'>"-01-01"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>cpi <span class='o'>=</span> <span class='nv'>VALUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>3</span><span class='o'>:</span><span class='m'>9</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

As usual, you can directly feed the output of one code block into another with the `%>%` ("pipe") operator:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># feed the vector number directly into get_cansim_vector()</span>
<span class='nv'>cpi_sk</span> <span class='o'>&lt;-</span> <span class='nv'>cpi_annual_table</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>product <span class='o'>=</span> <span class='s'>"Products and product groups"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>GEO</span> <span class='o'>==</span> <span class='s'>"Saskatchewan"</span> <span class='o'>&amp;</span>
         <span class='nv'>product</span> <span class='o'>==</span> <span class='s'>"All-items"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='nv'>VECTOR</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/character.html'>as.character</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector.html'>get_cansim_vector</a></span><span class='o'>(</span><span class='nv'>.</span>, <span class='s'>"2007-01-01"</span>, <span class='s'>"2018-12-31"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>year <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>REF_DATE</span>, <span class='s'>"-01-01"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>cpi <span class='o'>=</span> <span class='nv'>VALUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>3</span><span class='o'>:</span><span class='m'>9</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

### <a name="find-table-by-vector"></a> How to Find a Table if All You Know is a Vector

By this point you may be wondering if there is an inverse operation, i.e. if we can find CANSIM *table* number if all we know is a *vector* number? We sure can! This is what [`get_cansim_vector_info()`](https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector_info.html) function is for. And we can retrieve the table, too:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># find out CANSIM table number if you know a vector number</span>
<span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector_info.html'>get_cansim_vector_info</a></span><span class='o'>(</span><span class='s'>"v41690973"</span><span class='o'>)</span><span class='o'>$</span><span class='nv'>table</span>

<span class='c'># get table if you know a vector number</span>
<span class='nv'>cpi_table</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim_vector_info.html'>get_cansim_vector_info</a></span><span class='o'>(</span><span class='s'>"v41690973"</span><span class='o'>)</span><span class='o'>$</span><span class='nv'>table</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html'>get_cansim</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

<a name="joining-data-inflation"></a> Joining Data (and Adjusting for Inflation)
--------------------------------------------------------------------------------

Now that we have both weekly wages data (`wages_0370`) and CPI data (`cpi_sk`), we can calculate the inflation rate and adjust the wages for inflation. The formula for calculating the inflation rate for the period from the base year to year X is: `(CPI in year X – CPI in base year) / CPI in base year`.

If we wanted the inflation rate to be expressed as a percentage, we would have multiplied the result by 100, but here it is more convenient to have inflation expressed as a proportion:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># calculate the inflation rate</span>
<span class='nv'>cpi_sk</span><span class='o'>$</span><span class='nv'>infrate</span> <span class='o'>&lt;-</span> <span class='o'>(</span><span class='nv'>cpi_sk</span><span class='o'>$</span><span class='nv'>cpi</span> <span class='o'>-</span> <span class='nv'>cpi_sk</span><span class='o'>$</span><span class='nv'>cpi</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>)</span> <span class='o'>/</span> <span class='nv'>cpi_sk</span><span class='o'>$</span><span class='nv'>cpi</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span>
</code></pre>

</div>

Now join the resulting dataset to `wages_0370` with [`dplyr::left_join()`](https://dplyr.tidyverse.org/reference/mutate-joins.html). Then use the inflation rates data to adjust wages for inflation with the following formula: `base year $ = current year $ / (1 + inflation rate)`:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># join inflation rate data to wages_0370; adjust wages for inflation</span>
<span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nv'>wages_0370</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>left_join</span><span class='o'>(</span><span class='nv'>cpi_sk</span>, by <span class='o'>=</span> <span class='s'>"year"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>dollars_2007 <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>current_dollars</span> <span class='o'>/</span> <span class='o'>(</span><span class='m'>1</span> <span class='o'>+</span> <span class='nv'>infrate</span><span class='o'>)</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

We are now ready to move on to the next article in this series and plot the resulting data.

