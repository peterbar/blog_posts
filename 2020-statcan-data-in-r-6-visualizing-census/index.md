---
title: "Working with Statistics Canada Data in R, Part 6: Visualizing Census Data"
date: "2020-06-12T00:00:00"
slug: "statcan-data-in-r-6-visualizing-census-data"
excerpt: "A detailed tutorial on visualizing Canadian Census data using R."
status: "publish"
output: hugodown::md_document
categories: 
  - Data visualization
  - Statistics Canada
tags:
  - cancensus (package)
  - Canadian Census data
  - data visualization
  - ggplot2
comment_status: open
ping_status: open
rmd_hash: 7eeff15fe848e35c

---

<a name="intro-viz"></a> Introduction
-------------------------------------

In the <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/" target="_blank">previous part</a> of the Working with Statistics Canada Data in R series, we have retrieved the following key labor force indicators from the 2016 Canadian census for Canada as a country and for the largest metropolitan areas in each of Canada's <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/#annex-notes-and-definitions" target="_blank">five geographic regions</a>:

-   labor force participation rate, employment rate, and unemployment rate,
-   percent of workers by work situation: full time vs part time, by gender, and
-   education levels of people aged 25 to 64, by gender.

Now we are going to plot the labor force participation rates and the percent of workers by work situation. And in the next post, I'll show how to write functions to automate repetitive plotting tasks using the 2016 Census education data as an example.

As always, let's start with loading the required packages. Note the `ggrepel` package, which helps to <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-part-3-visualizing-cansim-data/#preventing-overlaps-with-ggrepel" target="_blank">prevent overlapping</a> of data points and text labels in our graphics.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># load packages</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://tidyverse.tidyverse.org'>tidyverse</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/slowkow/ggrepel'>ggrepel</a></span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

</div>

<a name="ordered-bar-plot"></a> Ordered Bar Plot: Labor Force Involvement Rates
-------------------------------------------------------------------------------

Why the bar plot for this data? Well, the bar plot is one of the simplest and thus easiest to interpret plots, and the data -- labor force involvement rates -- fits this type of plot nicely. We will plot the rates for all our regions in the same graphic, and we are going to order regions by unemployment rate.

### <a name="ordering-vector"></a> Creating an Ordering Vector

In the previous part of this series, we <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/#retrieve-census-data" target="_blank">retrieved</a> 2016 Census data for labor force involvement rates, did some preparatory work required to plot the data with `ggplot2` package, and saved the data as the `labor` dataframe. There is one more step we need to complete before we can plot this data: we need to create an ordering vector with unemployment numbers and append this vector to `labor`.

<div class="highlight">

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># prepare 'labor' dataset for plotting: </span>
<span class='c'># create an ordering vector to set the order of regions in the plot</span>
<span class='nv'>labor</span> <span class='o'>&lt;-</span> <span class='nv'>labor</span> <span class='o'>%&gt;%</span>
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>region</span><span class='o'>)</span> <span class='o'>%&gt;%</span>  <span class='c'># groups data by region</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>indicator</span> <span class='o'>==</span> <span class='s'>"unemployment rate"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nv'>indicator</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>rename</span><span class='o'>(</span>unemployment <span class='o'>=</span> <span class='nv'>rate</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>left_join</span><span class='o'>(</span><span class='nv'>labor</span>, by <span class='o'>=</span> <span class='s'>"region"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>indicator <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/factor.html'>factor</a></span><span class='o'>(</span><span class='nv'>indicator</span>, 
                            levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"participation rate"</span>,
                                       <span class='s'>"employment rate"</span>,
                                       <span class='s'>"unemployment rate"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Note the `left_join()` call, which joins the result of manipulating the `labor` dataframe back onto `labor`. If it seems confusing, take a look at this code, which returns the same output:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># alt. (same output):</span>
<span class='nv'>labor_order</span> <span class='o'>&lt;-</span> <span class='nv'>labor</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>indicator</span> <span class='o'>==</span> <span class='s'>"unemployment rate"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nv'>indicator</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>rename</span><span class='o'>(</span>unemployment <span class='o'>=</span> <span class='nv'>rate</span><span class='o'>)</span>

<span class='nv'>labor</span> <span class='o'>&lt;-</span> <span class='nv'>labor</span> <span class='o'>%&gt;%</span>
<span class='nf'>left_join</span><span class='o'>(</span><span class='nv'>labor_order</span>, by <span class='o'>=</span> <span class='s'>"region"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
<span class='nf'>mutate</span><span class='o'>(</span>indicator <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/factor.html'>factor</a></span><span class='o'>(</span><span class='nv'>indicator</span>,
                          levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"participation rate"</span>,
                                     <span class='s'>"employment rate"</span>,
                                     <span class='s'>"unemployment rate"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Also note the `mutate()` call that manually re-assigns factor levels of the `indicator` variable, so that labor force indicators are plotted in the logical order: first labor force participation rate, then employment rate, and finally the unemployment rate. Remember that `ggplot2` plots categorical variables in the order of factor levels.

### <a name="making-bar-plot"></a> Making an Ordered Bar Plot

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># plot data</span>
<span class='nv'>plot_labor</span> <span class='o'>&lt;-</span> 
  <span class='nv'>labor</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>ggplot</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/reorder.factor.html'>reorder</a></span><span class='o'>(</span><span class='nv'>region</span>, <span class='nv'>unemployment</span><span class='o'>)</span>, 
             y <span class='o'>=</span> <span class='nv'>rate</span>, 
             fill <span class='o'>=</span> <span class='nv'>indicator</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_col</span><span class='o'>(</span>width <span class='o'>=</span> <span class='m'>.6</span>, position <span class='o'>=</span> <span class='s'>"dodge"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_text</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nv'>rate</span><span class='o'>)</span>,
            position <span class='o'>=</span> <span class='nf'>position_dodge</span><span class='o'>(</span>width <span class='o'>=</span> <span class='m'>.6</span><span class='o'>)</span>,
            show.legend <span class='o'>=</span> <span class='kc'>FALSE</span>,
            size <span class='o'>=</span> <span class='m'>2.5</span>,
            vjust <span class='o'>=</span> <span class='o'>-</span><span class='m'>.4</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>coord_cartesian</span><span class='o'>(</span>ylim <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>0</span>, <span class='m'>82</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># expand Y axis to prevent labels overlap</span>
  <span class='nf'>scale_y_continuous</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Percent"</span>, 
                     breaks <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span><span class='m'>0</span>, <span class='m'>80</span>, by <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>scale_x_discrete</span><span class='o'>(</span>name <span class='o'>=</span> <span class='kc'>NULL</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>scale_fill_manual</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Indicator:"</span>,
                    values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"participation rate"</span> <span class='o'>=</span> <span class='s'>"deepskyblue2"</span>,
                               <span class='s'>"employment rate"</span> <span class='o'>=</span> <span class='s'>"olivedrab3"</span>,
                               <span class='s'>"unemployment rate"</span> <span class='o'>=</span> <span class='s'>"tomato"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme_bw</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>hjust <span class='o'>=</span> <span class='m'>.5</span>, 
                                  size <span class='o'>=</span> <span class='m'>10</span>, 
                                  face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        plot.subtitle <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>hjust <span class='o'>=</span> <span class='m'>.5</span>, 
                                     size <span class='o'>=</span> <span class='m'>9</span>, 
                                     margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>b <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span>,
        panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"grey88"</span><span class='o'>)</span>,
        panel.grid.minor <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
        axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        axis.title.y <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, 
                                    face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                    margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>r <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span><span class='o'>)</span>,
        legend.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
        legend.position <span class='o'>=</span> <span class='s'>"bottom"</span>,
        plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, 
                                    hjust <span class='o'>=</span> <span class='m'>0</span>,
                                    margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>labs</span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Labor Force Indicators in Canada's Geographic Regions' Largest Cities in 2016"</span>,
       subtitle <span class='o'>=</span> <span class='s'>"Compared to Canada, Ordered by Unemployment Rate"</span>,
       caption <span class='o'>=</span> <span class='s'>"Data: Statistics Canada 2016 Census."</span><span class='o'>)</span>
</code></pre>

</div>

Note the `x = reorder(region, unemployment)` inside the `aes()` call: this is where we order the plot's X axis by unemployment rates. Remember that we have grouped our data by region so that we could put regions on the X axis.

Note also the `scale_fill_manual()` function, where we manually assign colors to the plot's `fill` aesthetic.

### <a name="saving-plot"></a> Saving the Plot

Now that we have made the plot, let's create the directory where we will be saving our graphics, and save our plot to it:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.create</a></span><span class='o'>(</span><span class='s'>"output"</span><span class='o'>)</span> <span class='c'># creates folder</span>
<span class='nf'>ggsave</span><span class='o'>(</span><span class='s'>"output/plot_labor.png"</span>, 
       <span class='nv'>plot_labor</span>,
       width <span class='o'>=</span> <span class='m'>11</span>, height <span class='o'>=</span> <span class='m'>8.5</span>, units <span class='o'>=</span> <span class='s'>"in"</span><span class='o'>)</span>
</code></pre>

</div>

Finally, let's print the plot to screen:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>plot_labor</span><span class='o'>)</span>

</code></pre>
<img src="figs/print-plot-labor-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

<a name="faceted-plot"></a> Faceted Plot: Full Time vs Part Time Workers, by Gender
-----------------------------------------------------------------------------------

This will be a more complex task compared to plotting labor force participation rates. Here we have the data that is broken down by work situation (full-time vs part-time), and by gender, and also by region. And ideally, we also want the total numbers for full-time and part-time workers to be presented in the same plot. This is too complex to be visualized as a simple bar plot like the one we've just made.

To visualize all these data in a single plot, we'll use *faceting*: breaking down one plot into multiple sub-plots. And I suggest a donut chart -- a variation on a pie chart that has a round hole in the center. Note that generally speaking, pie charts have a <a href="https://www.businessinsider.com/pie-charts-are-the-worst-2013-6" target="_blank">well-deserved bad reputation</a>, which boils down to two facts: humans have difficulty visually comparing angles, and if you have many categories in your data, pie charts become an unreadable mess. <a href="http://varianceexplained.org/r/improving-pie-chart/" target="_blank">Here</a> and <a href="https://www.data-to-viz.com/caveat/pie.html" target="_blank">here</a> you can read more about pie charts' shortcomings, and which plots can best replace pie charts.

So why an I using a pie chart? Well, three reasons, really. First, we'll only have four categories inside the chart, so it won't be messy. Second, it is technically a donut chart, not a pie chart, and it is the empty space inside each donut where I will put the total numbers for full- and part-time workers. And third, I'd like to show how to make donut charts with `ggplot2` in case you ever need this, which is not as straightforward as with most other charts, since `ggplot2` doesn't gave a 'donut' geom.

### <a name="preparing-data"></a> Preparing Data for Plotting

In the <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/" target="_blank">previous post</a>, we have retrieved the 2016 Census data on the percentage of full-time and part-time workers, by gender, and saved it in the `work` dataframe. Let's now prepare the data for plotting. For that, we'll need to add three more variables. `type_gender` will be a categorical variable that combines work type and gender -- currently these are two different variables. `percent` will contain percentages for each combination of work type and gender, by region. And `percent_type` will contain total percentages for full-time and part-time workers, by region.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># prepare 'work' dataset for plotting</span>
<span class='nv'>work</span> <span class='o'>&lt;-</span> <span class='nv'>work</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>region</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>type_gender <span class='o'>=</span> <span class='nf'>str_c</span><span class='o'>(</span><span class='nv'>type</span>, <span class='nv'>gender</span>, sep <span class='o'>=</span> <span class='s'>" "</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='c'># percent of workers by region, work type, and gender:</span>
  <span class='nf'>mutate</span><span class='o'>(</span>percent <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>count</span><span class='o'>/</span><span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>count</span><span class='o'>)</span><span class='o'>*</span><span class='m'>100</span>, <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='c'># percent of workers by work type, total:</span>
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>region</span>, <span class='nv'>type</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>percent_type <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>percent</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

### <a name="making-donut-plot"></a> Making a Faceted Donut Plot

Now the dataset is ready for plotting, so let's make a *faceted* plot. Since `ggplot2` doesn't like pie charts (of which a donut chart is a variant), there is no 'pie' geom, and we'll have to get a bit hacky with the code. Pay close attention to the in-code comments.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># plot work data (as a faceted plot)</span>
<span class='nv'>plot_work</span> <span class='o'>&lt;-</span>
  <span class='nv'>work</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>ggplot</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>""</span>, 
             y <span class='o'>=</span> <span class='nv'>percent</span>, 
             fill <span class='o'>=</span> <span class='nv'>type_gender</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_col</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"white"</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># sectors' separator color</span>
  <span class='nf'>coord_polar</span><span class='o'>(</span>theta <span class='o'>=</span> <span class='s'>"y"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html'>geom_text_repel</a></span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nv'>percent</span><span class='o'>)</span>,
                  <span class='c'># put text labels inside corresponding sectors:</span>
                  position <span class='o'>=</span> <span class='nf'>position_stack</span><span class='o'>(</span>vjust <span class='o'>=</span> <span class='m'>.5</span><span class='o'>)</span>, 
                  force <span class='o'>=</span> <span class='m'>.005</span>, <span class='c'># repelling force</span>
                  size <span class='o'>=</span> <span class='m'>2.5</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html'>geom_label_repel</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nf'>distinct</span><span class='o'>(</span><span class='nf'>select</span><span class='o'>(</span><span class='nv'>work</span>, <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"region"</span>,
                                                  <span class='s'>"type"</span>,
                                                  <span class='s'>"percent_type"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>,
                   <span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='m'>0</span>, <span class='c'># turn pie chart into donut chart</span>
                       y <span class='o'>=</span> <span class='nv'>percent_type</span>, 
                       label <span class='o'>=</span> <span class='nv'>percent_type</span>, 
                       fill <span class='o'>=</span> <span class='nv'>type</span><span class='o'>)</span>,
                   size <span class='o'>=</span> <span class='m'>2.5</span>,
                   fontface <span class='o'>=</span> <span class='s'>"bold"</span>,
                   force <span class='o'>=</span> <span class='m'>.007</span>, <span class='c'># repelling force</span>
                   show.legend <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span> <span class='o'>+</span>  
  <span class='nf'>scale_fill_manual</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Work situation"</span>,
                    labels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"full time"</span> <span class='o'>=</span> <span class='s'>"all full-time"</span>,
                               <span class='s'>"part time"</span> <span class='o'>=</span> <span class='s'>"all part-time"</span><span class='o'>)</span>,
                    values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"full time male"</span> <span class='o'>=</span> <span class='s'>"olivedrab4"</span>,
                               <span class='s'>"full time female"</span> <span class='o'>=</span> <span class='s'>"olivedrab1"</span>,
                               <span class='s'>"part time male"</span> <span class='o'>=</span> <span class='s'>"tan4"</span>,
                               <span class='s'>"part time female"</span> <span class='o'>=</span> <span class='s'>"tan1"</span>,
                               <span class='s'>"full time"</span> <span class='o'>=</span> <span class='s'>"green3"</span>,
                               <span class='s'>"part time"</span> <span class='o'>=</span> <span class='s'>"orange3"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>facet_wrap</span><span class='o'>(</span><span class='o'>~</span> <span class='nv'>region</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>guides</span><span class='o'>(</span>fill <span class='o'>=</span> <span class='nf'>guide_legend</span><span class='o'>(</span>nrow <span class='o'>=</span> <span class='m'>3</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='nf'>theme_void</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>10</span>, 
                                  face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                  margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>10</span>, b <span class='o'>=</span> <span class='m'>20</span><span class='o'>)</span>,
                                  hjust <span class='o'>=</span> <span class='m'>.5</span><span class='o'>)</span>,
        strip.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>, 
        plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, 
                                    hjust <span class='o'>=</span> <span class='m'>0</span>,
                                    margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>20</span>, b <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span>,
        legend.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
        <span class='c'># change size of symbols (colored squares) in legend:</span>
        legend.key.size <span class='o'>=</span> <span class='nf'>unit</span><span class='o'>(</span><span class='m'>1</span>, <span class='s'>"lines"</span><span class='o'>)</span>, 
        legend.position <span class='o'>=</span> <span class='s'>"bottom"</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>labs</span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Percentage of Workers, by Work Situation &amp; Gender, 2016"</span>,
       caption <span class='o'>=</span> <span class='s'>"Note: Percentages may not add up to 100% due to values rounding.\nData source: Statistics Canada 2016 Census."</span><span class='o'>)</span>
</code></pre>

</div>

There are a number of things in the plot's code that I'd like to draw your attention to. First, a `ggplot2` pie chart is a stacked bar chart (`geom_col`) made in the polar coordinate system: `coord_polar(theta = “y”)`. For `geom_col()`, `position = “stack”` is the default, so it is not specified in the code. Note also that `geom_col()` needs the `x` aesthetic, but a pie chart doesn't have an `x` coordinate. So I used `x = “”` to trick `geom_col()` into thinking it has the `x` aesthetic, otherwise it would have thrown an error: `“geom_col requires the following missing aesthetics: x”`.

But how do you turn a pie chart into a donut chart? To do this, I set `x = 0` inside the [`ggrepel::geom_label_repel()`](https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html) `aes()` call. Try passing different values to x to see how it works: for example, `x = 1` turns the plot into a standard pie chart, while `x = -1` turns a donut into a narrow ring.

In order to prevent labels overlap, I used [`ggrepel::geom_text_repel()`](https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html) and [`ggrepel::geom_label_repel()`](https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html) to add text labels to our plot instead of [`ggplot2::geom_text()`](https://ggplot2.tidyverse.org/reference/geom_text.html) and [`ggplot2::geom_label()`](https://ggplot2.tidyverse.org/reference/geom_text.html). And `position = position_stack(vjust = .5)` inside [`geom_text_repel()`](https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html) puts text labels in the middle of their respective sectors of the donut plot.

The `data = distinct(select(work, c(“region”, “type”, “percent_type”))` argument to [`geom_label_repel()`](https://rdrr.io/pkg/ggrepel/man/geom_text_repel.html) prevents the duplication of labels containing total numbers for full-time and part-time workers.

The `scale_fill_manual()` is used to manually assign colors and names to our plot's legend items, and `guides(fill = guide_legend(nrow = 3))` changes the order of legend items.

Finally, `facet_wrap(~ region)` creates a faceted plot, by region.

And just as we did with the previous plot, let's save our plot to the 'output' folder and print it to screen:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>ggsave</span><span class='o'>(</span><span class='s'>"output/plot_work.png"</span>, 
       <span class='nv'>plot_work</span>,
       width <span class='o'>=</span> <span class='m'>11</span>, height <span class='o'>=</span> <span class='m'>8.5</span>, units <span class='o'>=</span> <span class='s'>"in"</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>plot_work</span><span class='o'>)</span>

</code></pre>
<img src="figs/print-plot-work-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

