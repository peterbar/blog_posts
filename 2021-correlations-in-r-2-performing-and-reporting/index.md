---
title: 'Correlation Analysis in R, Part 2: Performing and Reporting Correlation Analysis'
date: '2021-01-31T02:00:00'
slug: 'correlation-analysis-in-r-part-2-performing-and-reporting'
output: hugodown::md_document
status: 'publish'
excerpt: 'Part 2 of the series of tutorials on correlation analysis in R. In this part, I will provide an overview of the relevant packages and functions. I will also address some of the best practices to write up and visualize correlations as text, tables, and correlation matrices in online and print publications.'
categories:
- Statistics
- Correlation Analysis
- Data Reporting
tags:
- statistics (theory)
- statistical methods
- correlation analysis
- data visualization
- data reporting
comment_status: open
ping_status: open
bibliography: 'Data Science.bib'
csl: 'chicago-author-date-17th-ed.csl'
link-citations: yes
rmd_hash: a22ed4f9085006d3

---

This is the second part of the <a href="https://dataenthusiast.ca/category/correlation-analysis/" target="_blank">Correlation Analysis in R</a> series. In this post, I will provide an overview of some of the packages and functions used to perform correlation analysis in R, and will then address reporting and visualizing correlations as text, tables, and correlation matrices in online and print publications.

<a name="performing-corr-analysis-basic-tools"></a> Performing Correlation Analysis: Basic Tools
------------------------------------------------------------------------------------------------

### <a name="comparing-stats-rstatix-correlation"></a> Comparing stats::cor.test, rstatix::cor\_test, and correlation::cor\_test

There are multiple packages that allow to perform basic correlation analysis and provide a sufficiently detailed output. By sufficiently detailed I mean more detailed than that of [`stats::cor()`](https://rdrr.io/r/stats/cor.html). Of those, I prefer [`rstatix`](https://github.com/kassambara/rstatix) and [`correlation`](https://github.com/easystats/correlation) (the latter is part of the [`easystats`](https://github.com/easystats/easystats#easystats) ecosystem). Both have the function [`cor_test()`](https://rpkgs.datanovia.com/rstatix/reference/cor_test.html), and both are better than `stats::cor_test()` because they work in a pipe and return output as a dataframe.

Let's illustrate the use of [`cor_test()`](https://rpkgs.datanovia.com/rstatix/reference/cor_test.html) from both packages with the data collected by Gorman, Williams, and Fraser ([2014](#ref-Gorman2014)), which is available as the [`palmerpenguins`](https://github.com/allisonhorst/palmerpenguins) package. First, let's install and load the packages, then get data for one penguin species:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># install packages</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"rstatix"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"correlation"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"palmerpenguins"</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># load packages</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://dplyr.tidyverse.org'>dplyr</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://rpkgs.datanovia.com/rstatix/'>rstatix</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://easystats.github.io/correlation/'>correlation</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://allisonhorst.github.io/palmerpenguins/'>palmerpenguins</a></span><span class='o'>)</span>

<span class='c'># select Adelie penguins</span>
<span class='nv'>adelie</span> <span class='o'>&lt;-</span> <span class='nv'>penguins</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>species</span> <span class='o'>==</span> <span class='s'>"Adelie"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://dplyr.tidyverse.org/reference/select.html'>select</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>2</span>, <span class='m'>3</span>, <span class='m'>6</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='c'># keep only relevant data</span>
  <span class='nf'><a href='https://tidyr.tidyverse.org/reference/drop_na.html'>drop_na</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Advantages of [`rstatix::cor_test()`](https://rpkgs.datanovia.com/rstatix/reference/cor_test.html):

-   works in a pipe, unlike [`stats::cor.test()`](https://rdrr.io/r/stats/cor.test.html),
-   output is a tibble dataframe, unlike [`stats::cor.test()`](https://rdrr.io/r/stats/cor.test.html), which returns a list, and unlike [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html), which returns a dataframe but not a tibble,
-   supports quaziquotation, unlike [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html) -- you don't have to remember to put variable names in quotes.

Disadvantages of [`rstatix::cor_test()`](https://rpkgs.datanovia.com/rstatix/reference/cor_test.html):

-   only allows to calculate confidence intervals (CIs) for Pearson's $r$, unlike [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html), which can calculate CIs for Spearman's rho $\rho$ and Kendall's tau $\tau$,
-   knows only three methods for correlation analysis -- Pearson's, Spearman's, and Kendall's -- vs. 15 (!) methods available in [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html) including the `"auto"` method, where R tries to guess the best method for you, and
-   doesn't report sample size and/or degrees of freedom, unlike [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html).

Let's illustrate:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># rstatix::cor_test()</span>
<span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, <span class='nv'>bill_length_mm</span>, <span class='nv'>body_mass_g</span>, method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 1 x 6</span></span>
<span class='c'>#&gt;   var1           var2          cor statistic        p method  </span>
<span class='c'>#&gt;   <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>          </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>     </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>   </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_length_mm body_mass_g  0.55   </span><span style='text-decoration: underline;'>258</span><span>553. 2.77</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-13</span><span> Spearman</span></span>

<span class='c'># correlation::cor_test()</span>
<span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, x <span class='o'>=</span> <span class='s'>"bill_length_mm"</span>, y <span class='o'>=</span> <span class='s'>"body_mass_g"</span>, method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span>

<span class='c'>#&gt; Parameter1     |  Parameter2 |  rho |       95% CI |        S |      p |   Method | n_Obs</span>
<span class='c'>#&gt; -----------------------------------------------------------------------------------------</span>
<span class='c'>#&gt; bill_length_mm | body_mass_g | 0.55 | [0.42, 0.65] | 2.59e+05 | &lt; .001 | Spearman |   151</span>
</code></pre>

</div>

Most R packages, including `stats`, `rstatix`, and `correlation`, use <a href="https://dataenthusiast.ca/2021/correlation-analysis-in-r-part-1-basic-theory/#correlation_coefficient" target="_blank">Pearson’s</a> correlation coefficient $r$ as the default method for correlation analysis, so you'll need to expressly assign a `method` argument if you need to compute a different coefficient.

Both [`rstatix::cor_test()`](https://rpkgs.datanovia.com/rstatix/reference/cor_test.html) and [`correlation::cor_test()`](https://easystats.github.io/correlation/reference/cor_test.html) support <a href="https://dataenthusiast.ca/2021/correlation-analysis-in-r-part-1-basic-theory/#directional-testing" target="_blank">directional hypothesis testing</a>, even though in the latter case the directional option is not documented in the help returned by [`?correlation::cor_test`](https://easystats.github.io/correlation/reference/cor_test.html):

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, <span class='nv'>bill_length_mm</span>, <span class='nv'>body_mass_g</span>,
                  alternative <span class='o'>=</span> <span class='s'>"greater"</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 1 x 8</span></span>
<span class='c'>#&gt;   var1           var2          cor statistic        p conf.low conf.high method </span>
<span class='c'>#&gt;   <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>          </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>     </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>     </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>  </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_length_mm body_mass_g  0.55      8.01 1.48</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-13</span><span>    0.447         1 Pearson</span></span>


<span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, x <span class='o'>=</span> <span class='s'>"bill_length_mm"</span>, y <span class='o'>=</span> <span class='s'>"body_mass_g"</span>, 
                      alternative <span class='o'>=</span> <span class='s'>"greater"</span><span class='o'>)</span>

<span class='c'>#&gt; Parameter1     |  Parameter2 |    r |       95% CI | t(149) |      p |  Method | n_Obs</span>
<span class='c'>#&gt; --------------------------------------------------------------------------------------</span>
<span class='c'>#&gt; bill_length_mm | body_mass_g | 0.55 | [0.45, 1.00] |   8.01 | &lt; .001 | Pearson |   151</span>
</code></pre>

</div>

### <a name="retrieving-p-value-and-ci"></a> Retrieving p-values and Confidence Intervals

Even if your analysis does not immediately return a p-value or a CI for your chosen method, `correlation` package provides two functions that can calculate them for nearly any method in existence: [`cor_to_p()`](https://easystats.github.io/correlation/reference/cor_to_p.html) and [`cor_to_ci()`](https://easystats.github.io/correlation/reference/cor_to_p.html). These functions take:

-   your correlation coefficient (or a correlation matrix),
-   sample size,
-   confidence level (95% set as default), and
-   method (see [`?correlation::cor_to_ci`](https://easystats.github.io/correlation/reference/cor_to_p.html) for a full list).

Let's illustrate using the values returned by our analysis of correlation between bill length and body mass in Adelie penguins, for Spearman's $\rho$ coefficient:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># p-value</span>
<span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_to_p.html'>cor_to_p</a></span><span class='o'>(</span><span class='m'>.55</span>, n <span class='o'>=</span> <span class='m'>151</span>, method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span>

<span class='c'>#&gt; $p</span>
<span class='c'>#&gt; [1] 2.581667e-13</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $statistic</span>
<span class='c'>#&gt; [1] 8.038661</span>

<span class='c'># CI with default confidence level </span>
<span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_to_p.html'>cor_to_ci</a></span><span class='o'>(</span><span class='m'>.55</span>, n <span class='o'>=</span> <span class='m'>151</span>, method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span>

<span class='c'>#&gt; $CI_low</span>
<span class='c'>#&gt; [1] 0.4239604</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $CI_high</span>
<span class='c'>#&gt; [1] 0.6551406</span>

<span class='c'># CI with 99% confidence level</span>
<span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_to_p.html'>cor_to_ci</a></span><span class='o'>(</span><span class='m'>.55</span>, n <span class='o'>=</span> <span class='m'>151</span>, ci <span class='o'>=</span> <span class='m'>0.99</span>, method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span>

<span class='c'>#&gt; $CI_low</span>
<span class='c'>#&gt; [1] 0.3802826</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $CI_high</span>
<span class='c'>#&gt; [1] 0.683883</span>
</code></pre>

</div>

As of the time of writing this, [`cor_to_p()`](https://easystats.github.io/correlation/reference/cor_to_p.html) and [`cor_to_ci()`](https://easystats.github.io/correlation/reference/cor_to_p.html) do not support directional hypothesis testing.

### <a name="correlation-matrix"></a> Correlation Matrix

A correlation matrix is simply a table containing correlation coefficients for pairs of variables. It is useful when you need to report coefficients (and sometimes their p-values too) for more than two variables. Here is what it looks like:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># clean up missing data </span>
<span class='nv'>penguins</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://tidyr.tidyverse.org/reference/drop_na.html'>drop_na</a></span><span class='o'>(</span><span class='nv'>penguins</span><span class='o'>)</span>

<span class='c'># make correlation matrix</span>
<span class='nv'>cmat</span> <span class='o'>&lt;-</span> <span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_mat.html'>cor_mat</a></span><span class='o'>(</span><span class='nv'>penguins</span>, <span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nf'><a href='https://dplyr.tidyverse.org/reference/select_all.html'>select_if</a></span><span class='o'>(</span><span class='nv'>penguins</span>, <span class='nv'>is.numeric</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
<span class='nv'>cmat</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 5 x 6</span></span>
<span class='c'>#&gt;   rowname           bill_length_mm bill_depth_mm flipper_length_mm body_mass_g   year</span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>*</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>                      </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>         </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>             </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_length_mm             1            -</span><span style='color: #BB0000;'>0.23</span><span>              0.65        0.59   0.033</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>2</span><span> bill_depth_mm             -</span><span style='color: #BB0000;'>0.23</span><span>          1                -</span><span style='color: #BB0000;'>0.580</span><span>      -</span><span style='color: #BB0000;'>0.47</span><span>  -</span><span style='color: #BB0000;'>0.048</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>3</span><span> flipper_length_mm          0.65         -</span><span style='color: #BB0000;'>0.580</span><span>             1           0.87   0.15 </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>4</span><span> body_mass_g                0.59         -</span><span style='color: #BB0000;'>0.47</span><span>              0.87        1      0.022</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>5</span><span> year                       0.033        -</span><span style='color: #BB0000;'>0.048</span><span>             0.15        0.022  1</span></span>
</code></pre>

</div>

You can reorder a correlation matrix by coefficient:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># correlation matrix, ordered by coefficient</span>
<span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_reorder.html'>cor_reorder</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 5 x 6</span></span>
<span class='c'>#&gt;   rowname           bill_depth_mm   year bill_length_mm flipper_length_mm body_mass_g</span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>*</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>                     </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>          </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>             </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_depth_mm             1     -</span><span style='color: #BB0000;'>0.048</span><span>         -</span><span style='color: #BB0000;'>0.23</span><span>             -</span><span style='color: #BB0000;'>0.580</span><span>      -</span><span style='color: #BB0000;'>0.47</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>2</span><span> year                     -</span><span style='color: #BB0000;'>0.048</span><span>  1              0.033             0.15        0.022</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>3</span><span> bill_length_mm           -</span><span style='color: #BB0000;'>0.23</span><span>   0.033          1                 0.65        0.59 </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>4</span><span> flipper_length_mm        -</span><span style='color: #BB0000;'>0.580</span><span>  0.15           0.65              1           0.87 </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>5</span><span> body_mass_g              -</span><span style='color: #BB0000;'>0.47</span><span>   0.022          0.59              0.87        1</span></span>
</code></pre>

</div>

It is also possible to extract significance levels from the correlation matrix with [`rstatix::cor_get_pval()`](https://rpkgs.datanovia.com/rstatix/reference/cor_mat.html), which returns a table of numeric p-values:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># matrix of p-values</span>
<span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_mat.html'>cor_get_pval</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 5 x 6</span></span>
<span class='c'>#&gt;   rowname           bill_length_mm bill_depth_mm flipper_length_mm body_mass_g    year</span>
<span class='c'>#&gt;   <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>                      </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>         </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>             </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>   </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_length_mm          0.  </span><span style='color: #949494;'> </span><span>         2.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 5</span><span>         7.21</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 42</span><span>   1.54</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 32</span><span> 0.553  </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>2</span><span> bill_depth_mm           2.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 5</span><span>      0.  </span><span style='color: #949494;'> </span><span>            4.78</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 31</span><span>   7.02</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 20</span><span> 0.381  </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>3</span><span> flipper_length_mm       7.21</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-42</span><span>      4.78</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-31</span><span>         0.  </span><span style='color: #949494;'> </span><span>       3.13</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-105</span><span> 0.005</span><span style='text-decoration: underline;'>74</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>4</span><span> body_mass_g             1.54</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-32</span><span>      7.02</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-20</span><span>         3.13</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-105</span><span>   0.  </span><span style='color: #949494;'> </span><span>     0.691  </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>5</span><span> year                    5.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 1</span><span>      3.81</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 1</span><span>         5.74</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-  3</span><span>   6.91</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-  1</span><span> 0</span></span>
</code></pre>

</div>

You can also get a correlation matrix with both coefficients (as numbers) *and* p-values (as symbols). By default, the symbols and their meanings are: \*\*\*\* $\leq$ .0001, \*\*\* $\leq$ .001, \*\* $\leq$ .01, \* $\leq$ .05, no symbol $=$ not significant. You can assign your own symbols and significance cut-off points with the `symbols` and `cutpoints` arguments, respectively.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_mark_significant.html'>cor_mark_significant</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span>

<span class='c'>#&gt;             rowname bill_length_mm bill_depth_mm flipper_length_mm body_mass_g year</span>
<span class='c'>#&gt; 1    bill_length_mm                                                                </span>
<span class='c'>#&gt; 2     bill_depth_mm      -0.23****                                                 </span>
<span class='c'>#&gt; 3 flipper_length_mm       0.65****     -0.58****                                   </span>
<span class='c'>#&gt; 4       body_mass_g       0.59****     -0.47****          0.87****                 </span>
<span class='c'>#&gt; 5              year          0.033        -0.048            0.15**       0.022</span>
</code></pre>

</div>

If you don't like the matrix format, you can pivot the matrix to a long format with [`rstatix::cor_gather()`](https://rpkgs.datanovia.com/rstatix/reference/cor_reshape.html) as a dataframe of paired variables. The returned table will show both the coefficients and the p-values as numbers. Note that the table might get quite long depending on the number of correlated variables:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_reshape.html'>cor_gather</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 25 x 4</span></span>
<span class='c'>#&gt;    var1              var2              cor        p</span>
<span class='c'>#&gt;    <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>             </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>           </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>    </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 1</span><span> bill_length_mm    bill_length_mm  1     0.  </span><span style='color: #949494;'> </span><span>   </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 2</span><span> bill_depth_mm     bill_length_mm -</span><span style='color: #BB0000;'>0.23</span><span>  2.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 5</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 3</span><span> flipper_length_mm bill_length_mm  0.65  7.21</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-42</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 4</span><span> body_mass_g       bill_length_mm  0.59  1.54</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-32</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 5</span><span> year              bill_length_mm  0.033 5.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 1</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 6</span><span> bill_length_mm    bill_depth_mm  -</span><span style='color: #BB0000;'>0.23</span><span>  2.53</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 5</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 7</span><span> bill_depth_mm     bill_depth_mm   1     0.  </span><span style='color: #949494;'> </span><span>   </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 8</span><span> flipper_length_mm bill_depth_mm  -</span><span style='color: #BB0000;'>0.580</span><span> 4.78</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-31</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 9</span><span> body_mass_g       bill_depth_mm  -</span><span style='color: #BB0000;'>0.47</span><span>  7.02</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>-20</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>10</span><span> year              bill_depth_mm  -</span><span style='color: #BB0000;'>0.048</span><span> 3.81</span><span style='color: #949494;'>e</span><span style='color: #BB0000;'>- 1</span></span>
<span class='c'>#&gt; <span style='color: #949494;'># … with 15 more rows</span></span>
</code></pre>

</div>

An opposite function [`rstatix::cor_spread()`](https://rpkgs.datanovia.com/rstatix/reference/cor_reshape.html) spreads a long correlation dataframe into a correlation matrix:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>cmat_long</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_reshape.html'>cor_gather</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span>
<span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_reshape.html'>cor_spread</a></span><span class='o'>(</span><span class='nv'>cmat_long</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 5 x 6</span></span>
<span class='c'>#&gt;   rowname           bill_length_mm bill_depth_mm flipper_length_mm body_mass_g   year</span>
<span class='c'>#&gt;   <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>                      </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>         </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>             </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>       </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span>  </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> bill_length_mm             1            -</span><span style='color: #BB0000;'>0.23</span><span>              0.65        0.59   0.033</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>2</span><span> bill_depth_mm             -</span><span style='color: #BB0000;'>0.23</span><span>          1                -</span><span style='color: #BB0000;'>0.580</span><span>      -</span><span style='color: #BB0000;'>0.47</span><span>  -</span><span style='color: #BB0000;'>0.048</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>3</span><span> flipper_length_mm          0.65         -</span><span style='color: #BB0000;'>0.580</span><span>             1           0.87   0.15 </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>4</span><span> body_mass_g                0.59         -</span><span style='color: #BB0000;'>0.47</span><span>              0.87        1      0.022</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>5</span><span> year                       0.033        -</span><span style='color: #BB0000;'>0.048</span><span>             0.15        0.022  1</span></span>
</code></pre>

</div>

<a name="reporting-correlation-analysis"></a> Reporting Correlation Analysis
----------------------------------------------------------------------------

### <a name="reporting-as-text"></a> Reporting as Text

Reporting correlation coefficients is pretty easy: you just have to say how big they are and what their significance value is. When reporting, keep the following things in mind (Field, Miles, and Field [2012](#ref-Field2012a), 241):

1.  Coefficients are usually (for example, in [APA style](https://www.socscistatistics.com/tutorials/correlation/default.aspx)) reported to two decimal places. There should be no zero before the decimal point for the correlation coefficient or the probability value (because neither can exceed 1).
2.  There are standard probabilities you can use when reporting $p$ (.05, .01, .001, and .0001). If $p \geq .001$, report the exact p-value, otherwise you can simply report $p < .001$.
3.  If you are reporting a one-tailed probability, you should expressly state so, as by default probabilities are assumed to be two-tailed.
4.  Use a correct letter to represent your correlation coefficient, such as Pearson's $r$, Kendall's $\tau$, or Spearman's $\rho$.
5.  Remember to report your sample size (as $n = sample\:size$) or degrees of freedom (APA requires degrees of freedom in parentheses next to $r$). Just remember that for Pearson's $r$, $df = n\:–\:2$. For non-parametric tests, report only sample size.
6.  It is also recommended to report the test statistic (for Pearson's $r$, it would be $t$-statistic).
7.  Confidence intervals should be provided whenever possible in addition to the results of the hypothesis test, with confidence level matched to the significance level chosen for the test (e.g. 95% CI for $p \leq .05$, 99% CI for $p \leq .01$); if no inference to the population is intended, report standard deviation of the mean instead of CI (Sim and Reid [1999](#ref-Sim1999))⁠.

For example, the results of this test:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>correlation</span><span class='nf'>::</span><span class='nf'><a href='https://easystats.github.io/correlation/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, x <span class='o'>=</span> <span class='s'>"bill_length_mm"</span>, y <span class='o'>=</span> <span class='s'>"body_mass_g"</span><span class='o'>)</span>

<span class='c'>#&gt; Parameter1     |  Parameter2 |    r |       95% CI | t(149) |      p |  Method | n_Obs</span>
<span class='c'>#&gt; --------------------------------------------------------------------------------------</span>
<span class='c'>#&gt; bill_length_mm | body_mass_g | 0.55 | [0.43, 0.65] |   8.01 | &lt; .001 | Pearson |   151</span>
</code></pre>

</div>

Can be reported as follows:

> Our research shows a highly significant positive correlation between bill length and body mass among Adelie penguins: $t$ = 8.01, $p$ \< .001, Pearson's $r$(149) = .55, $n$ = 151, 95% CI \[0.43, 0.65\].

Reporting Spearman's or Kendall's correlation coefficients would be similar, but without degrees of freedom.[^1]

### <a name="reporting-as-table"></a> Reporting as Table

You have no doubt noticed that the results of statistical models are often reported as nicely formatted tables in peer-reviewed journals. So far, our correlations have been reported as plain text tables with a monospaced font. Which means they look a bit ugly. Fortunately, R has a multitude of packages designed to format tables. You can find a brief overview of most (although certainly not all) of them [here](https://hughjonesd.github.io/huxtable/design-principles.html).

My criteria for choosing the best packages to format tables are simple. First and foremost, the package should be fully compatible with [R Markdown](https://rmarkdown.rstudio.com/), which I use for nearly all my writing (and you should too, because of how much better it is than ~~MS Word~~ legacy software with horrible UI).[^2] This means that when you knit your Rmd, the table should render correctly in at least the following formats: HTML, PDF, Word, PowerPoint, and ideally, also OpenDocument and LaTeX. The package should also be easy to use and well-documented.

Upon some research, I think that the best options are:

-   [`huxtable`](https://hughjonesd.github.io/huxtable/) -- supports most formats and is [well-documented](https://hughjonesd.github.io/huxtable/huxtable.html),
-   [`flextable`](https://davidgohel.github.io/flextable/) -- the best [documented](https://ardata-fr.github.io/flextable-book/index.html), and
-   [`gtsummary`](http://www.danieldsjoberg.com/gtsummary/index.html) -- the simplest. Although `gtsummary` renders natively as HTML only, its output can be converted to huxtable or flextable objects, which in turn can be rendered as pretty much anything. Also, `huxtable` and `flextable` are highly versatile and can be used to format any tables, regardless of their contents. `gtsummary` is primarily intended to format the output of commonly used statistical models.

I personally prefer `huxtable` because it supports the largest number of formats (for some to work, you may still [need](https://hughjonesd.github.io/huxtable/huxtable.html#using-huxtables-in-knitr-and-rmarkdown) `flextable` to be installed) and has a simple, straightforward syntax.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># install packages for table formatting</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"huxtable"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"flextable"</span><span class='o'>)</span>
</code></pre>

</div>

Avoid loading `huxtable` and `flextable` at the same time, as there will be conflicts between some of their functions. Or if you have to, call functions using the `packagename::` syntax.

Let's now demonstrate `huxtable` in action by formatting the results of our correlation analysis:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># load huxtable</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://hughjonesd.github.io/huxtable/'>huxtable</a></span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># make huxtable</span>
<span class='nv'>adelie_ht</span> <span class='o'>&lt;-</span> <span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_test.html'>cor_test</a></span><span class='o'>(</span><span class='nv'>adelie</span>, 
                               <span class='nv'>bill_length_mm</span>, <span class='nv'>body_mass_g</span>, 
                               method <span class='o'>=</span> <span class='s'>"spearman"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/as_huxtable.html'>as_huxtable</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/set-multiple.html'>set_all_padding</a></span><span class='o'>(</span>row <span class='o'>=</span> <span class='nv'>everywhere</span>, col <span class='o'>=</span> <span class='nv'>everywhere</span>, value <span class='o'>=</span> <span class='m'>6</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/bold.html'>set_bold</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='nv'>everywhere</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/borders.html'>set_top_border</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='nv'>everywhere</span>, value <span class='o'>=</span> <span class='m'>0.8</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/borders.html'>set_bottom_border</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='nv'>everywhere</span>, value <span class='o'>=</span> <span class='m'>0.4</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/caption.html'>set_caption</a></span><span class='o'>(</span><span class='s'>"Correlation between Body Mass and Bill Length in Adelie Penguins"</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># render huxtable</span>
<span class='nv'>adelie_ht</span>
</code></pre>

</div>

In some situations, you might need to render a huxtable object as an image, e.g. to combine it with a ggplot object or for other purposes. For example, I had to do this for compatibility with the [`goodpress`](https://maelle.github.io/goodpress/) package, which for some reason can't process `huxtable` HTML output. To render your huxtable as an image, you'll first need to convert it to a flextable object with [`huxtable::as_flextable()`](https://rdrr.io/pkg/huxtable/man/as_flextable.html), and then render with [`flextable::as_raster()`](https://davidgohel.github.io/flextable/reference/as_raster.html).[^3]

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># render huxtable as an image</span>
<span class='nv'>adelie_ht</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/as_flextable.html'>as_flextable</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>flextable</span><span class='nf'>::</span><span class='nf'><a href='https://davidgohel.github.io/flextable/reference/as_raster.html'>as_raster</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>)</span>

</code></pre>
<img src="figs/render-adelie-ht-as-image-1.png" width="70%" style="display: block; margin: auto;" />

</div>

Refer to the `huxtable` [documentation](https://hughjonesd.github.io/huxtable/reference/index.html) for the details about what these functions do. Note that some table formatting options work only for the specific output types. For example, higher visual weight of the top border (`value = 0.8`) renders correctly in PDF, but in HTML both borders render as having equal weight. Not sure if this is a bug or a feature.

As a more advanced example, let's format our correlation matrix `cmat`. First, let's reorder it by correlation coefficient, and then let's render coefficients in different color fonts depending on the coefficient's sign and magnitude:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>cmat_ht</span> <span class='o'>&lt;-</span> <span class='nf'>rstatix</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/rstatix/reference/cor_reorder.html'>cor_reorder</a></span><span class='o'>(</span><span class='nv'>cmat</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/as_huxtable.html'>as_huxtable</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/set-multiple.html'>set_all_padding</a></span><span class='o'>(</span>row <span class='o'>=</span> <span class='nv'>everywhere</span>, col <span class='o'>=</span> <span class='nv'>everywhere</span>, value <span class='o'>=</span> <span class='m'>6</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/bold.html'>set_bold</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='nv'>everywhere</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/background_color.html'>set_background_color</a></span><span class='o'>(</span><span class='nv'>evens</span>, <span class='nv'>everywhere</span>, <span class='s'>"grey92"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/text_color.html'>map_text_color</a></span><span class='o'>(</span><span class='o'>-</span><span class='m'>1</span>, <span class='o'>-</span><span class='m'>1</span>, <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/by_colorspace.html'>by_colorspace</a></span><span class='o'>(</span><span class='s'>"red4"</span>, <span class='s'>"darkgreen"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/caption.html'>set_caption</a></span><span class='o'>(</span><span class='s'>"Correlation Matrix for Pygoscelis Penguins"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/col_width.html'>set_col_width</a></span><span class='o'>(</span><span class='nv'>everywhere</span>, value <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>.16</span>, <span class='m'>.15</span>, <span class='m'>.11</span>, <span class='m'>.2</span>, <span class='m'>.2</span>, <span class='m'>.2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/width.html'>set_width</a></span><span class='o'>(</span><span class='m'>1.02</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='c'># note how sum of col widths == total table width</span>
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/themes.html'>theme_article</a></span><span class='o'>(</span><span class='o'>)</span> <span class='c'># yes, there are themes!</span>
</code></pre>

</div>

<div class="highlight">

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>cmat_ht</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/pkg/huxtable/man/as_flextable.html'>as_flextable</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>flextable</span><span class='nf'>::</span><span class='nf'><a href='https://davidgohel.github.io/flextable/reference/as_raster.html'>as_raster</a></span><span class='o'>(</span><span class='nv'>.</span><span class='o'>)</span>

</code></pre>
<img src="figs/render-cmat-ht-as-image-1.png" width="84%" style="display: block; margin: auto;" />

</div>

In case you are using R Markdown for your reporting and are rendering to PDF, keep in mind that you won't be able to format table captions with HTML tags like here: [`set_caption("<b>Correlation Matrix for Pygoscelis Penguins</b>")`](https://rdrr.io/pkg/huxtable/man/caption.html), because they will be rendered literally (as "\<b\>" and "\</b\>"). However, this works for HTML.

Also keep in mind that the best YAML settings for PDF output would be:

    output: 
      pdf_document:
        latex_engine: xelatex

This will work well for complex or unusual LaTeX syntax, which may otherwise cause "Unicode character ... not set up for use with LaTeX when knitting to pdf" error.

Just to illustrate how PDF output would look, <a href="https://dataenthusiast.ca/wp-content/uploads/2021/01/correlations-in-r-2-performing-and-reporting.pdf" target="_blank">here is this post</a> rendered to PDF from R Markdown. Looks nice, doesn't it?

### <a name="visualizing-correlation-matrix"></a> Visualizing Correlation Matrix

`rstatix` has a function to visualize correlation matrices: [`cor_plot()`](https://rpkgs.datanovia.com/rstatix/reference/cor_plot.html). However, [`rstatix::cor_plot()`](https://rpkgs.datanovia.com/rstatix/reference/cor_plot.html) does not return a ggplot object, and thus:

-   can't take `ggplot2` themes or custom theme objects,
-   makes it harder to define a custom color palette, and
-   most importantly, [`rstatix::cor_plot()`](https://rpkgs.datanovia.com/rstatix/reference/cor_plot.html) output can't be further customized or annotated using `ggplot2` themes or packages such as [`ggpubr`](https://rpkgs.datanovia.com/ggpubr/).[^4]

Therefore, I would instead recommend using [`ggcorrplot::ggcorrplot()`](https://rdrr.io/pkg/ggcorrplot/man/ggcorrplot.html) which returns a ggplot object that can be altered, customized, or annotated using a broad ecosystem of `ggplot2`-based packages. Another great package is [`latex2exp`](https://cran.r-project.org/web/packages/latex2exp/vignettes/using-latex2exp.html). It allows you to render LaTeX expressions inside plot objects, which is very handy if you'd like to use special symbols or Greek letters in your plot's text elements.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># install ggcorrplot and ggpubr</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"ggcorrplot"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"ggpubr"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"latex2exp"</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># load ggcorrplot and ggpubr</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://www.sthda.com/english/wiki/ggcorrplot'>ggcorrplot</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://rpkgs.datanovia.com/ggpubr/'>ggpubr</a></span><span class='o'>)</span>
</code></pre>

</div>

There are two main ways to visualize a correlation matrix: as a square plot where correlations are duplicated (remember that $CORxy = CORyx$) and self-correlations ($r = 1$) are included, and as a half-square plot where correlation coefficients are not duplicated and self-correlations are excluded. Optionally, you can also add correlation coefficients to the plot, mark statistically non-significant correlations or completely exclude them, change the plot's color scheme, etc. Since [`ggcorrplot()`](https://rdrr.io/pkg/ggcorrplot/man/ggcorrplot.html) returns a `ggplot2` object, it can be further altered (e.g. by adding subtitle, annotations, captions, etc.) using [`ggpubr::ggpar()`](https://rpkgs.datanovia.com/ggpubr/reference/ggpar.html), [`ggpubr::annotate_figure()`](https://rpkgs.datanovia.com/ggpubr/reference/annotate_figure.html), and similar functions.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># ggcorrplot - basic </span>
<span class='nf'><a href='https://rdrr.io/pkg/ggcorrplot/man/ggcorrplot.html'>ggcorrplot</a></span><span class='o'>(</span><span class='nv'>cmat</span>, title <span class='o'>=</span> <span class='s'>"Penguins Correlated"</span><span class='o'>)</span>

</code></pre>
<img src="figs/ggcorrplot-basic-1.png" width="70%" style="display: block; margin: auto auto auto 0;" />

</div>

Let's now customize the plot by removing self-correlations, leaving non-significant coefficients blank, assigning a different color palette, reordering the plot by correlation coefficient, choosing plot theme, and adding subtitle and caption. Pay attention to comments in the code:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># ggcorrplot - customized</span>
<span class='nf'><a href='https://rdrr.io/pkg/ggcorrplot/man/ggcorrplot.html'>ggcorrplot</a></span><span class='o'>(</span><span class='nv'>cmat</span>, <span class='c'># takes correlation matrix</span>
           title <span class='o'>=</span> <span class='s'>"Penguins Correlated"</span>,
           ggtheme <span class='o'>=</span> <span class='nv'>theme_classic</span>, <span class='c'># takes ggplot2 and custom themes</span>
           colors <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"red"</span>, <span class='s'>"white"</span>, <span class='s'>"forestgreen"</span><span class='o'>)</span>, <span class='c'># custom color palette</span>
           hc.order <span class='o'>=</span> <span class='kc'>TRUE</span>, <span class='c'># reorders matrix by corr. coeff.</span>
           type <span class='o'>=</span> <span class='s'>"upper"</span>, <span class='c'># prevents duplication; also try "lower"</span>
           lab <span class='o'>=</span> <span class='kc'>TRUE</span>, <span class='c'># adds corr. coeffs. to the plot</span>
           insig <span class='o'>=</span> <span class='s'>"blank"</span>, <span class='c'># wipes non-significant coeffs.</span>
           lab_size <span class='o'>=</span> <span class='m'>3.5</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='c'># add subtitle and caption; note rendering LaTeX symbols in ggplot objects</span>
  <span class='nf'>ggpubr</span><span class='nf'>::</span><span class='nf'><a href='https://rpkgs.datanovia.com/ggpubr/reference/ggpar.html'>ggpar</a></span><span class='o'>(</span>subtitle <span class='o'>=</span> <span class='nf'>latex2exp</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/latex2exp/man/TeX.html'>TeX</a></span><span class='o'>(</span><span class='s'>"Significant correlations only (p$\\leq$.05)"</span>,
                                          output <span class='o'>=</span> <span class='s'>"text"</span><span class='o'>)</span>, 
                caption <span class='o'>=</span> <span class='s'>"Data: Gorman, Williams, and Fraser 2014"</span><span class='o'>)</span>

</code></pre>
<img src="figs/ggcorrplot-customized-1.png" width="70%" style="display: block; margin: auto auto auto 0;" />

</div>

Note how you can render LaTeX symbols inside ggplot objects with [`latex2exp::TeX()`](https://rdrr.io/pkg/latex2exp/man/TeX.html) function.

Hopefully, I've managed to provide some useful tips on performing and reporting correlation analysis. The next post in <a href="https://dataenthusiast.ca/category/correlation-analysis/" target="_blank">this series</a> will be dedicated to robust methods for correlation analysis.

This post is also available <a href="https://dataenthusiast.ca/wp-content/uploads/2021/01/correlations-in-r-2-performing-and-reporting.pdf" target="_blank">as a PDF</a>.

<a name="bibliography"></a> Bibliography
----------------------------------------

<div id="refs" class="references">

<div id="ref-Field2012a">

Field, Andy, Jeremy Miles, and Zoë Field. 2012. *Discovering Statistics Using R*. First edit. London, Thousand Oaks, New Delhi, Singapore: SAGE Publications.

</div>

<div id="ref-Gorman2014">

Gorman, Kristen B., Tony D. Williams, and William R. Fraser. 2014. "Ecological sexual dimorphism and environmental variability within a community of Antarctic penguins (Genus Pygoscelis)." *PLoS ONE* 9 (3). <https://doi.org/10.1371/journal.pone.0090081>.

</div>

<div id="ref-Sim1999">

Sim, Julius, and Norma Reid. 1999. "Statistical inference by confidence intervals: Issues of interpretation and utilization." *Physical Therapy* 79 (2): 186--95. <https://doi.org/10.1093/ptj/79.2.186>.

</div>

</div>

[^1]: Also note that the magnitude of Spearman's correlation is usually very close to Pearson's, but Kendall's is not. For small samples, Kendall's $\tau$ gives a more accurate estimate of the correlation in the population, particularly when your ranked data (ranked because it is a non-parametric test) has a lot of tied ranks (Field, Miles, and Field [2012](#ref-Field2012a), 225). More on this later in the <a href="https://dataenthusiast.ca/category/correlation-analysis/" target="_blank">series</a>.

[^2]: For example, all posts in my blog are written in R Markdown and deployed to WordPress directly from R using the [`goodpress`](https://maelle.github.io/goodpress/) package. Changing a single setting in the post's YAML header (which takes a few seconds) can turn it into a nicely formatted HTML page, PDF article, MS Word or LibreOffice document, etc.

[^3]: You'll also need to have packages [`webshot`](https://github.com/wch/webshot) and [`magick`](https://github.com/ropensci/magick) installed, along with their system dependencies.

[^4]: If you try, it will return "Error in ggpubr...: Can't handle an object of class matrix".

