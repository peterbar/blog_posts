---
title: "Working with Statistics Canada Data in R, Part 5: Retrieving Census Data"
date: "2020-03-21T00:00:00"
slug: "statcan-data-in-r-5-retrieving-census-data"
excerpt: "A detailed tutorial on using R to retrieve, and work with Canadian Census data."
status: "publish"
output: hugodown::md_document
categories: Statistics Canada
tags:
  - cancensus (package)
  - Canadian Census data
comment_status: open
ping_status: open
rmd_hash: a876bf681189248d

---

<a name="intro-census"></a> Introduction
----------------------------------------

Now that we are <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-4-census-data-cancensus-setup/" target="_blank">ready</a> to start working with Canadian Census data, let's first briefly address the question why you may need it. After all, <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-part-1-cansim/" target="_blank">CANSIM</a> data is often more up-to-date and covers a much broader range of topics than the national census data, which is gathered every five years in respect of a limited number of questions.

The main reason is that CANSIM data is far less granular geographically. Most of it is collected at the provincial or even higher regional level. You may be able to find CANSIM data on a limited number of questions for some of the country's largest metropolitan areas, but if you need the data for a specific census division, city, town, or village, you'll have to use the Census.

To illustrate the use of the <a href="https://cran.r-project.org/package=cancensus" target="_blank"><code>cancensus</code></a> package, let's do a small research project. First, in this post we'll retrieve the following key labor force characteristics of the largest metropolitan areas in each of the [five geographic regions of Canada](#annex-notes):

-   Labor force participation rate, employment rate, and unemployment rate.
-   Percent of workers by work situation: full time vs part time, by gender.
-   Education levels of people aged 25 to 64, by gender.

The cities (metropolitan areas) that we are going to look at, are: Calgary, Halifax, Toronto, Vancouver, and Whitehorse. We'll also get these data for Canada as a whole for comparison and to illustrate the retrieval of data at different geographic levels

Next, in Part 6 of the "Working with Statistics Canada Data in R" series, we will visualize these data, including making a faceted plot and writing a function to automate repetitive plotting tasks.

Keep in mind that `cancensus` also allows you to retrieve geospatial data, that is, borders of census regions at various geographic levels, in <a href="https://cran.r-project.org/package=sp" target="_blank"><code>sp</code></a> and <a href="https://cran.r-project.org/package=sf" target="_blank"><code>sf</code></a> formats. Retrieving and visualizing Statistics Canada geospatial data will be covered later in these series.

So, let's get started by loading the required packages:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/mountainMath/cancensus'>cancensus</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://tidyverse.tidyverse.org'>tidyverse</a></span><span class='o'>)</span>
</code></pre>

</div>

<a name="searching-for-data"></a> Searching for Data
----------------------------------------------------

`cancensus` retrieves census data with the [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) function. [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) can take a number of arguments, the most important of which are `dataset`, `regions`, and `vectors`, which have no defaults. Thus, in order to be able to retrieve census data, you'll first need to figure out:

-   your dataset,
-   your region(s), and
-   your data vector(s).

### <a name="find-datasets"></a> Find Census Datasets

Let's see which census datasets are available through the <a href="https://censusmapper.ca/api" target="_blank">CensusMapper API</a>:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/list_census_datasets.html'>list_census_datasets</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Currently, datasets earlier than 1996 are not available, so if you need to work with pre-1996 census data, you won't be able to retrieve it with `cancensus`.

### <a name="find-regions"></a> Find Census Regions

Next, let's find the regions that we'll be getting the data for. To search for census regions, use the [`search_census_regions()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html) function.

Let's take a look at what region search returns for Toronto. Note that `cancensus` functions return their output as dataframes, making it is easy to subset. Here I limited the output to the most relevant columns to make sure it fits on screen. You can run the code without `[c(1:5, 8)]` to see all of it.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># all census levels</span>
<span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html'>search_census_regions</a></span><span class='o'>(</span>searchterm <span class='o'>=</span> <span class='s'>"Toronto"</span>, 
                      dataset <span class='o'>=</span> <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>5</span>, <span class='m'>8</span><span class='o'>)</span><span class='o'>]</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 3 x 6</span></span>
<span class='c'>#&gt;   region  name    level     pop municipal_status PR_UID</span>
<span class='c'>#&gt;   <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>   </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>   </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>   </span><span style='color: #949494;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>            </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>1</span><span> 35535   Toronto CMA   5</span><span style='text-decoration: underline;'>928</span><span>040 B                35    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>2</span><span> 3520    Toronto CD    2</span><span style='text-decoration: underline;'>731</span><span>571 CDR              35    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>3</span><span> 3520005 Toronto CSD   2</span><span style='text-decoration: underline;'>731</span><span>571 C                35</span></span>
</code></pre>

</div>

You may have expected to get only one region: the city of Toronto, but instead you got three! So, what is the difference? Look at the column `level` for the answer. Often, the same geographic region can be represented by several <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/figures/f1_1-eng.cfm" target="_blank">census levels</a>, as is the case here. There are three levels for Toronto, which is simultaneously a <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/geo009-eng.cfm" target="_blank">census metropolitan area</a>, a <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/geo008-eng.cfm" target="_blank">census division</a>, and a <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/geo012-eng.cfm" target="_blank">census sub-division</a>. Note also the `PR_UID` column that contains numeric codes for Canada's provinces and territories. These codes can help you distinguish between different census regions that have same or similar names but are located in different provinces. For an example, run the code above replacing "Toronto" with "Windsor".

Remember that we were going to plot the data for census metropolitan areas? You can choose the geographic level with the `level` argument, which can take the following values: 'C' for Canada (national level), 'PR' for province, 'CMA' for census metropolitan area, 'CD' for census division, 'CSD' for census sub-division, or NA:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># specific census level</span>
<span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html'>search_census_regions</a></span><span class='o'>(</span><span class='s'>"Toronto"</span>, <span class='s'>"CA16"</span>, level <span class='o'>=</span> <span class='s'>"CMA"</span><span class='o'>)</span>
</code></pre>

</div>

Let's now list census regions that may be relevant for our project:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># explore available census regions</span>
<span class='nv'>names</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Canada"</span>, <span class='s'>"Calgary"</span>, <span class='s'>"Halifax"</span>, 
           <span class='s'>"Toronto"</span>, <span class='s'>"Vancouver"</span>, <span class='s'>"Whitehorse"</span><span class='o'>)</span>
<span class='nf'>map_df</span><span class='o'>(</span><span class='nv'>names</span>, <span class='o'>~</span> <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html'>search_census_regions</a></span><span class='o'>(</span><span class='nv'>.</span>, dataset <span class='o'>=</span> <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>5</span>, <span class='m'>8</span><span class='o'>)</span><span class='o'>]</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 19 x 6</span></span>
<span class='c'>#&gt;    region  name                    level      pop municipal_status PR_UID</span>
<span class='c'>#&gt;    <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>   </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>                   </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>    </span><span style='color: #949494;font-style: italic;'>&lt;int&gt;</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span>            </span><span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span> </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 1</span><span> 01      Canada                  C     35</span><span style='text-decoration: underline;'>151</span><span>728 </span><span style='color: #BB0000;'>NA</span><span>               </span><span style='color: #BB0000;'>NA</span><span>    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 2</span><span> 48825   Calgary                 CMA    1</span><span style='text-decoration: underline;'>392</span><span>609 B                48    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 3</span><span> 4806016 Calgary                 CSD    1</span><span style='text-decoration: underline;'>239</span><span>220 CY               48    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 4</span><span> 12205   Halifax                 CMA     </span><span style='text-decoration: underline;'>403</span><span>390 B                12    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 5</span><span> 1209    Halifax                 CD      </span><span style='text-decoration: underline;'>403</span><span>390 CTY              12    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 6</span><span> 1209034 Halifax                 CSD     </span><span style='text-decoration: underline;'>403</span><span>131 RGM              12    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 7</span><span> 2432023 Sainte-Sophie-d'Halifax CSD        612 MÉ               24    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 8</span><span> 35535   Toronto                 CMA    5</span><span style='text-decoration: underline;'>928</span><span>040 B                35    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 9</span><span> 3520    Toronto                 CD     2</span><span style='text-decoration: underline;'>731</span><span>571 CDR              35    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>10</span><span> 3520005 Toronto                 CSD    2</span><span style='text-decoration: underline;'>731</span><span>571 C                35    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>11</span><span> 59933   Vancouver               CMA    2</span><span style='text-decoration: underline;'>463</span><span>431 B                59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>12</span><span> 5915    Greater Vancouver       CD     2</span><span style='text-decoration: underline;'>463</span><span>431 RD               59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>13</span><span> 5915022 Vancouver               CSD     </span><span style='text-decoration: underline;'>631</span><span>486 CY               59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>14</span><span> 5915046 North Vancouver         CSD      </span><span style='text-decoration: underline;'>85</span><span>935 DM               59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>15</span><span> 5915051 North Vancouver         CSD      </span><span style='text-decoration: underline;'>52</span><span>898 CY               59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>16</span><span> 5915055 West Vancouver          CSD      </span><span style='text-decoration: underline;'>42</span><span>473 DM               59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>17</span><span> 5915020 Greater Vancouver A     CSD      </span><span style='text-decoration: underline;'>16</span><span>133 RDA              59    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>18</span><span> 6001009 Whitehorse              CSD      </span><span style='text-decoration: underline;'>25</span><span>085 CY               60    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>19</span><span> 6001060 Whitehorse, Unorganized CSD        326 NO               60</span></span>
</code></pre>

</div>

[`purrr::map_df()`](https://purrr.tidyverse.org/reference/map.html) function applies [`search_census_regions()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html) iteratively to each element of the `names` vector and returns output as a single dataframe. Note also the `~ .` syntax. Think of it as the tilde taking each element of `names` and passing it as an argument to a place indicated by the dot in the [`search_census_regions()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_regions.html) function. You can find more about the tilde-dot syntax <a href="http://www.rebeccabarter.com/blog/2019-08-19_purrr/#the-tilde-dot-shorthand-for-functions" target="_blank">here</a>. It may be a good idea to read the whole <a href="http://www.rebeccabarter.com/blog/2019-08-19_purrr/" target="_blank">tutorial</a>: `purrr` is a super-useful package, but not the easiest to learn, and this tutorial does a great job explaining the basics.

Since there are multiple entries for each search term, we'll need to choose the results for census metropolitan areas, or in case of Whitehorse, for census sub-division, since Whitehorse is too small to be considered a metropolitan area:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># select only the regions we need: CMAs (and CSD for Whitehorse)</span>
<span class='nv'>regions</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/list_census_regions.html'>list_census_regions</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>"CA16"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"Calgary|Halifax|Toronto|Vancouver"</span>, <span class='nv'>name</span><span class='o'>)</span> <span class='o'>&amp;</span>
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"CMA"</span>, <span class='nv'>level</span><span class='o'>)</span> <span class='o'>|</span> 
         <span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"Canada|Whitehorse$"</span>, <span class='nv'>name</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/as_census_region_list.html'>as_census_region_list</a></span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Pay attention to how the logical operators are used to filter the output by several conditions at once; also note using [`$`](https://rdrr.io/r/base/Extract.html) regex meta-character to choose from the `names` column the entry ending with 'Whitehorse' (to filter out 'Whitehorse, Unorganized'.

Finally, [`as_census_region_list()`](https://mountainmath.github.io/cancensus/index.html/reference/as_census_region_list.html) converts [`list_census_regions()`](https://mountainmath.github.io/cancensus/index.html/reference/list_census_regions.html) output to a data object of type list that can be passed to the [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) function as its regions argument.

### <a name="find-vectors"></a> Find Census Vectors

Canadian census data is made up of individual variables, aka census vectors. Vector number(s) is another argument you need to specify in order to retrieve data with the [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) function.

`cancensus` has two functions that allow you to search through census data variables: [`list_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html) and [`search_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html).

[`list_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html) returns all available vectors for a given dataset as a single dataframe containing vectors and their descriptions:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># structure of list_census_vectors output</span>
<span class='nf'><a href='https://rdrr.io/r/utils/str.html'>str</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html'>list_census_vectors</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>'CA16'</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; tibble [6,623 × 7] (S3: tbl_df/tbl/data.frame)</span>
<span class='c'>#&gt;  $ vector       : chr [1:6623] "v_CA16_401" "v_CA16_402" "v_CA16_403" "v_CA16_404" ...</span>
<span class='c'>#&gt;  $ type         : Factor w/ 3 levels "Female","Male",..: 3 3 3 3 3 3 3 3 2 1 ...</span>
<span class='c'>#&gt;  $ label        : chr [1:6623] "Population, 2016" "Population, 2011" "Population percentage change, 2011 to 2016" "Total private dwellings" ...</span>
<span class='c'>#&gt;  $ units        : Factor w/ 6 levels "Number","Percentage ratio (0.0-1.0)",..: 1 1 1 1 1 4 1 1 1 1 ...</span>
<span class='c'>#&gt;  $ parent_vector: chr [1:6623] NA NA NA NA ...</span>
<span class='c'>#&gt;  $ aggregation  : chr [1:6623] "Additive" "Additive" "Average of v_CA16_402" "Additive" ...</span>
<span class='c'>#&gt;  $ details      : chr [1:6623] "CA 2016 Census; Population and Dwellings; Population, 2016" "CA 2016 Census; Population and Dwellings; Population, 2011" "CA 2016 Census; Population and Dwellings; Population percentage change, 2011 to 2016" "CA 2016 Census; Population and Dwellings; Total private dwellings" ...</span>
<span class='c'>#&gt;  - attr(*, "last_updated")= POSIXct[1:1], format: "2020-12-21 23:27:47"</span>
<span class='c'>#&gt;  - attr(*, "dataset")= chr "CA16"</span>


<span class='c'># count variables in 'CA16' dataset</span>
<span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html'>list_census_vectors</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>'CA16'</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'>#&gt; [1] 6623</span>
</code></pre>

</div>

As you can see, there are 6623 (as of the time of writing this) variables in the 2016 census dataset, so [`list_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html) won't be the most convenient function to find a specific vector. Note however that there are situations (such as when you need to <a href="https://gitlab.com/peterbar/community_profiles/-/blob/master/community_profiles.R" target="_blank">select a lot of vectors at once</a>), in which [`list_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/list_census_vectors.html) would be appropriate.

Usually it is more convenient to use [`search_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html) to search for vectors. Just pass the text string of what you are looking for as the `searchterm` argument. You don't have to be precise: this function works even if you <a href="https://mountainmath.github.io/cancensus/articles/cancensus.html#variable-search" target="_blank">make a typo or are uncertain about the spelling</a> of your search term.

Let's now find census data vectors for labor force involvement rates:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get census data vectors for labor force involvement rates</span>
<span class='nv'>lf_vectors</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span>searchterm <span class='o'>=</span> <span class='s'>"employment rate"</span>, 
                        dataset <span class='o'>=</span> <span class='s'>"CA16"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/sets.html'>union</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"participation rate"</span>, <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>type</span> <span class='o'>==</span> <span class='s'>"Total"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>pull</span><span class='o'>(</span><span class='nv'>vector</span><span class='o'>)</span>
</code></pre>

</div>

Let's take a look at what this code does. Since `searchterm` doesn't have to be a precise match, "employment rate" search term retrieves unemployment rate vectors too. In the next line, [`union()`](https://rdrr.io/r/base/sets.html) merges dataframes returned by [`search_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html) into a single dataframe. Note that in this case [`union()`](https://rdrr.io/r/base/sets.html) could be substituted with `bind_rows()`. I recommend using [`union()`](https://rdrr.io/r/base/sets.html) in order to <a href="https://blog.exploratory.io/merging-two-data-frames-with-union-or-bind-rows-a55e79766d0" target="_blank">avoid data duplication</a>. Next, we choose only the "Total" numbers, since we are not going to plot labor force indicators by gender. Finally, the `pull()` command extracts a single vector from the dataframe, just like the [`$`](https://rdrr.io/r/base/Extract.html) subsetting operator: we need `lf_vectors` to be a data object of type vector in order to pass it to the `vectors` argument of the [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) function.

There is another way to figure out search terms to put inside the [`search_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html) function: use Statistics Canada online <a href="https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/index.cfm" target="_blank">Census Profile tool</a>. It can be used to quickly explore census data as well as to figure out variables' names (search terms) and their hierarchical structure.

For example, let's look at the <a href="https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/details/page.cfm?Lang=E&amp;Geo1=CMACA&amp;Code1=825&amp;Geo2=PR&amp;Code2=48&amp;SearchText=Calgary&amp;SearchType=Begins&amp;SearchPR=01&amp;B1=Labour&amp;TABID=1&amp;type=0" target="_blank">census labor data for Calgary metropolitan area</a>. Scrolling down, you will quickly find the numbers and text labels for full-time and part-time workers:

<div class="highlight">

<img src="figs/labour_structure.png" width="60%" style="display: block; margin: auto auto auto 0;" />

</div>

Now we know the exact search terms, so we can get precisely the vectors we need, free from any extraneous data:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get census data vectors for full- and part-time work</span>
<span class='c'># get vectors and labels    </span>
<span class='nv'>work_vectors_labels</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"full year, full time"</span>, <span class='s'>"CA16"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/base/sets.html'>union</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"part year and/or part time"</span>, <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>type</span> <span class='o'>!=</span> <span class='s'>"Total"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>3</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>label</span>, <span class='s'>".*, |.*and/or "</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>type <span class='o'>=</span> <span class='nf'>fct_drop</span><span class='o'>(</span><span class='nv'>type</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"vector"</span>, <span class='s'>"gender"</span>, <span class='s'>"type"</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'># extract vectors</span>
<span class='nv'>work_vectors</span> <span class='o'>&lt;-</span> <span class='nv'>work_vectors_labels</span><span class='o'>$</span><span class='nv'>vector</span>
</code></pre>

</div>

Note how this code differs from the code with which we extracted labor force involvement rates: since we need the data to be sub-divided both by the type of work *and* by gender (hence no "Total" values here), we are creating a dataframe that assigns respective labels to each vector number. This `work_vectors_labels` dataframe will supply categorical labels to be attached to the data [retrieved](#retrieve-census-data) with [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html).

Also, note these three lines:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'>  <span class='nf'>mutate</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>label</span>, <span class='s'>".*, |.*and/or "</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>type <span class='o'>=</span> <span class='nf'>fct_drop</span><span class='o'>(</span><span class='nv'>type</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"vector"</span>, <span class='s'>"gender"</span>, <span class='s'>"type"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

The first `mutate()` call removes all text up to and including `,` and `and/or` (spaces included) from the `label` column. The second drops unused factor level "Total" -- it is a good practice to make sure there are no unused factor levels if you are going to use `ggplot2` to plot your data. Finally, [`setNames()`](https://rdrr.io/r/stats/setNames.html) renames variables for convenience.

Finally, let's retrieve vectors for the education data for the age group from 25 to 64 years, by gender. Before we do this, I'd like to draw your attention to the fact that some of the census data is hierarchical, which means that some variables (census vectors) are included into parent and/or include child variables. It is very important to choose vectors at proper hierarchical levels so that you do not double-count or omit your data.

Education data is a good example of hierarchical data. You can explore data hierarchy using [`parent_census_vectors()`](https://mountainmath.github.io/cancensus/index.html/reference/parent_census_vectors.html) and `(child_census_vectors)` functions. However, you may find exploring the hierarchy <a href="https://www12.statcan.gc.ca/census-recensement/2016/dp-pd/prof/details/page.cfm?Lang=E&amp;Geo1=CMACA&amp;Code1=825&amp;Geo2=PR&amp;Code2=48&amp;SearchText=Calgary&amp;SearchType=Begins&amp;SearchPR=01&amp;B1=Education&amp;TABID=1&amp;type=0" target="_blank">visually</a> to be more convenient:

<div class="highlight">

<img src="figs/education_structure.png" width="60%" style="display: block; margin: auto auto auto 0;" />

</div>

So, let's now retrieve and label the education data vectors:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get vectors and labels</span>
<span class='nv'>ed_vectors_labels</span> <span class='o'>&lt;-</span>
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"certificate"</span>, <span class='s'>"CA16"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/base/sets.html'>union</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"degree"</span>, <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/base/sets.html'>union</a></span><span class='o'>(</span><span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/search_census_vectors.html'>search_census_vectors</a></span><span class='o'>(</span><span class='s'>"doctorate"</span>, <span class='s'>"CA16"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>type</span> <span class='o'>!=</span> <span class='s'>"Total"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/grep.html'>grepl</a></span><span class='o'>(</span><span class='s'>"25 to 64 years"</span>, <span class='nv'>details</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>slice</span><span class='o'>(</span><span class='o'>-</span><span class='m'>1</span>,<span class='o'>-</span><span class='m'>2</span>,<span class='o'>-</span><span class='m'>7</span>,<span class='o'>-</span><span class='m'>8</span>,<span class='o'>-</span><span class='m'>11</span><span class='o'>:</span><span class='o'>-</span><span class='m'>14</span>,<span class='o'>-</span><span class='m'>19</span>,<span class='o'>-</span><span class='m'>20</span>,<span class='o'>-</span><span class='m'>23</span><span class='o'>:</span><span class='o'>-</span><span class='m'>28</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>select</span><span class='o'>(</span><span class='m'>1</span><span class='o'>:</span><span class='m'>3</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>mutate</span><span class='o'>(</span>label <span class='o'>=</span>
           <span class='nf'>str_remove_all</span><span class='o'>(</span><span class='nv'>label</span>,
                          <span class='s'>" cert.*diploma| dipl.*cate|, CEGEP| level|"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>mutate</span><span class='o'>(</span>label <span class='o'>=</span>
           <span class='nf'>str_replace_all</span><span class='o'>(</span><span class='nv'>label</span>, 
                           <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"No.*"</span> <span class='o'>=</span> <span class='s'>"None"</span>,
                             <span class='s'>"Secondary.*"</span> <span class='o'>=</span> <span class='s'>"High school or equivalent"</span>,
                             <span class='s'>"other non-university"</span> <span class='o'>=</span> <span class='s'>"equivalent"</span>,
                             <span class='s'>"University above"</span> <span class='o'>=</span> <span class='s'>"Cert. or dipl. above"</span>,
                             <span class='s'>"medicine.*"</span> <span class='o'>=</span> <span class='s'>"health**"</span>,
                             <span class='s'>".*doctorate$"</span> <span class='o'>=</span> <span class='s'>"Doctorate*"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>mutate</span><span class='o'>(</span>type <span class='o'>=</span> <span class='nf'>fct_drop</span><span class='o'>(</span><span class='nv'>type</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"vector"</span>, <span class='s'>"gender"</span>, <span class='s'>"level"</span><span class='o'>)</span><span class='o'>)</span>

<span class='c'># extract vectors</span>
<span class='nv'>ed_vectors</span> <span class='o'>&lt;-</span> <span class='nv'>ed_vectors_labels</span><span class='o'>$</span><span class='nv'>vector</span>
</code></pre>

</div>

Note the `slice()` function that allows to manually select specific rows from a dataframe: positive numbers choose rows to keep, negative numbers choose rows to drop. I used `slice()` to drop the hierarchical levels from the data that are either too general or too granular. Note also that I had to edit text strings in the data. Finally, I added asterisks after "Doctorate" and "health". These are not regex symbols, but actual asterisks that will be used to refer to footnotes in plot captions later on.

Now that we have figured out our dataset, regions, and data vectors (and labeled the vectors, too), we are finally ready to retrieve the data.

<a name="retrieve-census-data"></a> Retrieve Census Data
--------------------------------------------------------

To retrieve census data, feed the dataset, regions, and data vectors into [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) as its' respective arguments. Note that [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) has the `use_cache` argument (set to `TRUE` by default), which tells [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) to retrieve data from cache if available. If there is no cached data, the function will query CensusMapper API for the data and will save it in the cache, while `use_cache = FALSE` will force [`get_census()`](https://mountainmath.github.io/cancensus/index.html/reference/get_census.html) to query the API and update the cache.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get census data for labor force involvement rates</span>
<span class='c'># feed regions and vectors into get_census()</span>
<span class='nv'>labor</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/get_census.html'>get_census</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>"CA16"</span>, 
             regions <span class='o'>=</span> <span class='nv'>regions</span>,
             vectors <span class='o'>=</span> <span class='nv'>lf_vectors</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>4</span><span class='o'>:</span><span class='m'>7</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/setNames.html'>setNames</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"region"</span>, <span class='s'>"employment rate"</span>, 
             <span class='s'>"unemployment rate"</span>, 
             <span class='s'>"participation rate"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>region <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>region</span>, <span class='s'>" (.*)"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>pivot_longer</span><span class='o'>(</span><span class='s'>"employment rate"</span><span class='o'>:</span><span class='s'>"participation rate"</span>, 
               names_to <span class='o'>=</span> <span class='s'>"indicator"</span>,
               values_to <span class='o'>=</span> <span class='s'>"rate"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate_if</span><span class='o'>(</span><span class='nv'>is.character</span>, <span class='nv'>as_factor</span><span class='o'>)</span>
</code></pre>

</div>

The `select()` call drops columns with irrelevant data. [`setNames()`](https://rdrr.io/r/stats/setNames.html) renames columns to remove vector numbers from variable names -- we don't need vector numbers in variable names because variable names will be converted to values in the `indicator` column. `str_remove()` inside the `mutate()` call drops municipal status codes '(B)' and '(CY)' from region names. Finally, `mutate_if()` line converts characters to factors for subsequent plotting.

An important function here is [`tidyr::pivot_longer()`](https://tidyr.tidyverse.org/reference/pivot_longer.html). It converts the dataframe <a href="https://cengel.github.io/R-data-wrangling/tidyr.html#about-long-and-wide-table-format" target="_blank">from wide to long format</a>. It takes three columns: `employment rate`, `unemployment rate`, and `participation rate`, and passes their names as values of the `indicator` variable, while their numeric values are passed to the `rate` variable. The reason for conversion is that we are going to plot the data for all three labor force indicators in the same graphic, which makes it necessary to store the indicators as a single factor variable.

Next, let's retrieve census data about the percent of full time vs part time workers, by gender, and the data about the education levels of people aged 25 to 64, by gender:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get census data for full-time and part-time work</span>
<span class='nv'>work</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/get_census.html'>get_census</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>"CA16"</span>, 
             regions <span class='o'>=</span> <span class='nv'>regions</span>,
             vectors <span class='o'>=</span> <span class='nv'>work_vectors</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>4</span><span class='o'>:</span><span class='m'>7</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>region <span class='o'>=</span> <span class='s'>"Region Name"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>pivot_longer</span><span class='o'>(</span><span class='m'>2</span><span class='o'>:</span><span class='m'>5</span>, names_to <span class='o'>=</span> <span class='s'>"vector"</span>, 
                    values_to <span class='o'>=</span> <span class='s'>"count"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>region <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>region</span>, <span class='s'>" (.*)"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>vector <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>vector</span>, <span class='s'>":.*"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>left_join</span><span class='o'>(</span><span class='nv'>work_vectors_labels</span>, by <span class='o'>=</span> <span class='s'>"vector"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>gender <span class='o'>=</span> <span class='nf'>str_to_lower</span><span class='o'>(</span><span class='nv'>gender</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate_if</span><span class='o'>(</span><span class='nv'>is.character</span>, <span class='nv'>as_factor</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># get census data for education levels</span>
<span class='nv'>education</span> <span class='o'>&lt;-</span> 
  <span class='nf'><a href='https://mountainmath.github.io/cancensus/index.html/reference/get_census.html'>get_census</a></span><span class='o'>(</span>dataset <span class='o'>=</span> <span class='s'>"CA16"</span>, 
             regions <span class='o'>=</span> <span class='nv'>regions</span>,
             vectors <span class='o'>=</span> <span class='nv'>ed_vectors</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>select</span><span class='o'>(</span><span class='o'>-</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span>, <span class='m'>4</span><span class='o'>:</span><span class='m'>7</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>rename</span><span class='o'>(</span>region <span class='o'>=</span> <span class='s'>"Region Name"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>pivot_longer</span><span class='o'>(</span><span class='m'>2</span><span class='o'>:</span><span class='m'>21</span>, names_to <span class='o'>=</span> <span class='s'>"vector"</span>, 
                     values_to <span class='o'>=</span> <span class='s'>"count"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>region <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>region</span>, <span class='s'>" (.*)"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>vector <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>vector</span>, <span class='s'>":.*"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>left_join</span><span class='o'>(</span><span class='nv'>ed_vectors_labels</span>, by <span class='o'>=</span> <span class='s'>"vector"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate_if</span><span class='o'>(</span><span class='nv'>is.character</span>, <span class='nv'>as_factor</span><span class='o'>)</span>
</code></pre>

</div>

Note one important difference from the code I used to retrieve the labor force involvement data: here I added the [`dplyr::left_join()`](https://dplyr.tidyverse.org/reference/mutate-joins.html) function that joins labels to the census data.

We now have the data and are ready to visualize it, which will be done in the next part of this series.

<a name="annex-notes"></a> Annex: Notes and Definitions
-------------------------------------------------------

For those of you who are outside of Canada, Canada's *geographic* regions and their largest metropolitan areas are:

-   The Atlantic Provinces -- Halifax
-   Central Canada -- Toronto
-   The Prairie Provinces -- Calgary
-   The West Coast -- Vancouver
-   The Northern Territories -- Whitehorse

These regions should not be confused with 10 provinces and 3 territories, which are Canada's sub-national *administrative* divisions, much like states in the U.S. Each region consists of several provinces or territories, except the West Coast, which includes only one province -- British Columbia. You can find more about Canada's geographic regions and territorial structure <a href="https://www.canada.ca/content/dam/ircc/migration/ircc/english/pdf/pub/discover.pdf" target="_blank">here (pages 44 to 51)</a>.

For the definitions of *employment rate*, *unemployment rate*, *labour force participation rate*, *full-time work*, and *part-time work*, see Statistics Canada's <a href="https://www150.statcan.gc.ca/n1/pub/71-543-g/71-543-g2018001-eng.htm" target="_blank">Guide to the Labour Force Survey</a>.

You can find more about census geographic areas <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/figures/f1_1-eng.cfm" target="_blank">here</a> and <a href="https://www.statcan.gc.ca/eng/subjects/standard/sgc/2016/introduction" target="_blank">here</a>. There is also a <a href="https://www12.statcan.gc.ca/census-recensement/2016/ref/dict/az2-eng.cfm?topic=az2" target="_blank">glossary</a> of census-related geographic concepts.

