---
title: "ggwebthemes: Some ggplot2 Themes Optimized for Blogs"
date: "2020-09-05T00:00:00"
slug: "ggwebthemes-ggplot2-themes-optimized-for-blogs"
excerpt: "An R package containing themes for ggplot2 that have been optimized for blogs and similar online usage: theme_web_bw, theme_web_classic, and theme_web_void."
status: "publish"
output: hugodown::md_document
categories: Data visualization
tags:
  - R packages
  - data visualization
comment_status: open
ping_status: open
rmd_hash: eee1241c87ffe501

---

<a name="about-ggwebthemes"></a> About ggwebthemes
--------------------------------------------------

Not long ago, I published a post "Three ggplot2 Themes Optimized for the Web", where I made some tweaks to my three favorite `ggplot2` themes -- `theme_bw()`, `theme_classic()`, and `theme_void()` -- to make them more readable and generally look better in graphics posted online, particularly in blog posts and similar publications.

I was happy to see that some people liked those and suggested that I should make a package. I tended to view packages as large collections of code and functions, but as Sébastien Rochette wisely put it, "[If you have one function, create a package! If this simplifies your life, why not?](https://twitter.com/StatnMap/status/1293577913565159437)" And since I will be frequently using these themes in subsequent posts, I'd like to make it as convenient as possible for the reader to install and use them.

So here is the [`ggwebthemes`](https://gitlab.com/peterbar/ggwebthemes) package! It has the same three themes, which I have tweaked and improved some more.

The package is not yet on CRAN. You can install `ggwebthemes` from GitLab:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># option 1: install using devtools</span>
<span class='c'># install.packages("devtools")</span>
<span class='nf'>devtools</span><span class='nf'>::</span><span class='nf'><a href='https://devtools.r-lib.org//reference/remote-reexports.html'>install_gitlab</a></span><span class='o'>(</span><span class='s'>"peterbar/ggwebthemes"</span><span class='o'>)</span>

<span class='c'># option 2: install using remotes</span>
<span class='c'># install.packages("remotes")</span>
<span class='nf'>remotes</span><span class='nf'>::</span><span class='nf'><a href='https://remotes.r-lib.org/reference/install_gitlab.html'>install_gitlab</a></span><span class='o'>(</span><span class='s'>"peterbar/ggwebthemes"</span><span class='o'>)</span>

<span class='c'># option 3: build from source</span>
<span class='c'># use if you get error: package 'ggplot2' was built under R version...</span>
<span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"https://gitlab.com/peterbar/ggwebthemes/-/raw/master/tar/ggwebthemes_0.1.1.tar.gz"</span>,
                 repos <span class='o'>=</span> <span class='kc'>NULL</span>, type <span class='o'>=</span> <span class='s'>"source"</span><span class='o'>)</span>

<span class='c'># load ggwebthemes</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/peterbar/ggwebthemes/'>ggwebthemes</a></span><span class='o'>)</span>
</code></pre>

</div>

Please report bugs and/or submit feature requests [here](https://gitlab.com/peterbar/ggwebthemes/-/issues). Since I am currently using WordPress (but thinking about switching to a static site), I am particularly interested in how these themes would behave on Hugo and in blogs created with R Markdown/blogdown, so if you have any feedback, it will be most appreciated.

You can find the package's reference manual [here](https://gitlab.com/peterbar/ggwebthemes/-/blob/master/doc/ggwebthemes_0.1.1.pdf).

Note: To avoid confusing the readers, I will be removing the original post "Three ggplot2 Themes Optimized for the Web", which contains early versions of these themes. You can still find it [on R-Bloggers](https://www.r-bloggers.com/three-ggplot2-themes-optimized-for-the-web/) in case you need it.

<a name="examples"></a> Examples
--------------------------------

### <a name="theme_web_bw"></a> theme\_web\_bw

<div class="highlight">

<img src="figs/plot_theme_web_bw.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

### <a name="theme_web_classic"></a> theme\_web\_classic

<div class="highlight">

<img src="figs/plot_theme_web_classic.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

### <a name="theme_web_void"></a> theme\_web\_void

<div class="highlight">

<img src="figs/plot_theme_web_void.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

