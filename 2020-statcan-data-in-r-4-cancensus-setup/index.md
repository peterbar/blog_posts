---
title: "Working with Statistics Canada Data in R, Part 4: Canadian Census Data – cancensus Package Setup"
date: "2020-02-18T00:00:00"
slug: "statcan-data-in-r-4-census-data-cancensus-setup"
excerpt: "How to set up the 'cancensus' R package - the best tool to work with the Canadian Census data."
status: "publish"
output: hugodown::md_document
categories: Statistics Canada
tags:
  - cancensus (package)
  - Canadian Census data
  - Rprofile
comment_status: open
ping_status: open
rmd_hash: 9a6ac73aa55d9ae0

---

<a name="what-is-cancensus"></a> What is cancensus
--------------------------------------------------

In the <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-introduction/" target="_blank">Introduction</a> to the "Working with Statistics Canada Data in R" series, I discussed the <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-introduction/#data-types" target="_blank">three main types of data</a> available from Statistics Canada. It is now time to move on to the second of those data types -- the Canadian National Census data.

<a href="https://cran.r-project.org/package=cancensus" target="_blank"><code>cancensus</code></a> is a specialized R package that allows you to retrieve Statistics Canada census data and geography. It follows the <a href="https://www.jstatsoft.org/article/view/v059i10/v59i10.pdf" target="_blank">tidy</a> approach to data processing. The package's authors are Jens von Bergmann, Dmitry Shkolnik, and Aaron Jacobs. I am not associated with the authors, I just use this great package in my work on a regular basis. The package's GitHub repository can be found <a href="https://github.com/mountainMath/cancensus" target="_blank">here</a>. There is also a <a href="https://cran.r-project.org/web/packages/cancensus/vignettes/cancensus.html" target="_blank">tutorial</a> by the package's authors, which I recommend taking a look at before proceeding.

Further in this series, I will provide an in-depth real-life example of working with Canadian Census data using the `cancensus` package. But first, let's install `cancensus`:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"cancensus"</span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/mountainMath/cancensus'>cancensus</a></span><span class='o'>)</span>
</code></pre>

</div>

<a name="cancensus-setup"></a> Setting up cancensus: Adding API Key and Cache Path to .Rprofile
-----------------------------------------------------------------------------------------------

`cancensus` relies on queries to the CensusMapper API, which requires a CensusMapper API key. The key can be obtained for free as per the package authors' <a href="https://mountainmath.github.io/cancensus/index.html#api-key" target="_blank">instructions</a>. Once you have the key, you can add it to your R environment:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cancensus.api_key <span class='o'>=</span> <span class='s'>"CensusMapper_your_api_key"</span><span class='o'>)</span>
</code></pre>

</div>

Note that although the authors are warning that API requests are limited in volume, I have significantly exceeded my API quota on some occasions, and still had no issues with retrieving data.

That said, depending on how much data you need, you can draw down your quota very quickly, and here's where local cache comes to the rescue. `cancensus` caches data every time you retrieve it, but by default the cache is not persistent between sessions. To make it persistent, as well as to remove the need to enter your API key every time you use `cancensus`, you can add both the API key and the cache path to your `.Rprofile` file.

If you'd like to learn in-depth what `.Rprofile` is and how to edit it, consider taking a look at <a href="https://csgillespie.github.io/efficientR/set-up.html#an-overview-of-rs-startup-files" target="_blank">sections 2.4.2 to 2.4.5</a> of "Efficient R Programming" by Colin Gillespie and Robin Lovelace. For a quick and simple edit, just keep reading.

### <a name="editing-rprofile-linux"></a> Editing .Rprofile on Linux

First, find your R home directory with [`R.home()`](https://rdrr.io/r/base/Rhome.html). Most likely, it will be in /usr/lib/R. If that is where your R home is, in the Linux Terminal (**not** in R), run:

    sudo nano /usr/lib/R/library/base/R/.Rprofile # edit path if needed

On some systems, .Rprofile may not be hidden, so if the above command doesn't open the `.Rprofile` file, try removing the dot before 'Rprofile':

    sudo nano /usr/lib/R/library/base/R/Rprofile

( ! ) Note that this will edit the *system* `.Rprofile` file, which will always run on startup and will apply to *all* your R projects. The file itself will warn you that "it is a bad idea to use this file as a template for personal startup files". You can safely ignore this warning as long as the only edit you are making is the one shown below, i.e. adding `cancensus` cache path and API key.

In the "options" section, add these two lines (remember to paste in your CensusMapper API key and a path to the directory where you'd like to keep your `cancensus` cache):

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cancensus.api_key <span class='o'>=</span> <span class='s'>"CensusMapper_your_api_key"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cancensus.cache_path <span class='o'>=</span> <span class='s'>"/home/your_username/path_to_your_R_directory/.cancensus_cache"</span><span class='o'>)</span>
</code></pre>

</div>

Then hit `Ctrl+X`, choose `Y` for "yes", and press `Enter` to save changes. When you first retrieve data with `cancensus`, R will create `.cancensus_cache` directory for you.

### <a name="editing-rprofile-windows"></a> Editing .Rprofile on Windows

Editing `.Rprofile` on Windows is a bit tricky. The best thing for you would be not to touch the Windows system `.Rprofile`, or else risk weird errors and crashes (I was not able to figure out where they come from).

Instead, set up a project-specific `.Rprofile`. The downside is that you may need to set it separately for every project in which you are going to use `cancensus`. The upside is that the contents of the `.Rprofile` file should be exactly the same every time. In R or Rstudio (not in the command line), run:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/file.edit.html'>file.edit</a></span><span class='o'>(</span><span class='s'>".Rprofile"</span><span class='o'>)</span>
</code></pre>

</div>

Or use the [`usethis::edit_r_profile()`](https://usethis.r-lib.org/reference/edit.html) function from the <a href="https://cran.r-project.org/package=usethis" target="_blank"><code>usethis</code></a> package.

Then, add these two lines and save the file:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cancensus.api_key <span class='o'>=</span> <span class='s'>"CensusMapper_your_api_key"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cancensus.cache_path <span class='o'>=</span> <span class='s'>"C:\\Users\\Home\\Documents\\R\\cancensus_cache"</span><span class='o'>)</span>
</code></pre>

</div>

Note that when used inside R, backslash `\` symbols in file paths in Windows may need to be escaped with another `\` symbol. If this file path doesn't work, try replacing duplicate `\\` with a single `\`.

At this point you should be ready to start retrieving data with `cancensus`, which will be addressed in detail in my next post.

