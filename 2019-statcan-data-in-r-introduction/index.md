---
title: "Working with Statistics Canada Data in R, Introduction"
date: "2019-10-31T00:00:00"
slug: "statcan-data-in-r-introduction"
excerpt: "An introduction to the series on working with Statistics Canada data in the R language. The goal of the series is to provide some examples (accompanied by detailed in-depth explanations) of working with Statistics Canada data in R."
status: "publish"
output: hugodown::md_document
categories: Statistics Canada
tags:
  - cancensus (package)
  - CANSIM
  - Statistics Canada Data
comment_status: open
ping_status: open
rmd_hash: c278e11fc2339736

---

This is the introduction to the series on working with Statistics Canada data in the R language. The goal of the series is to provide some examples (accompanied by detailed in-depth explanations) of working with Statistics Canada data in R. Besides, I'd love to see more economists, policy analysts, and social scientists using R in their work, so I'll be doing my best to make this easy for people without STEM degrees.

<a name="data-types"></a> Data Types
------------------------------------

Statistics Canada data is routinely used for economic and policy analysis, as well as for social science research, journalism, and many other applications. It is expected that the reader has some basic R skills.

For the purposes of this series, let's assume that there are three main types of StatCan data:

-   <a href="https://www150.statcan.gc.ca/n1/en/type/data" target="_blank">Statistics Canada Data</a>, previously known as Canadian Socio-economic Information Management System (CANSIM),
-   <a href="https://www12.statcan.gc.ca/census-recensement/index-eng.cfm" target="_blank">Canadian Census data</a>, and
-   Geographic data such as <a href="https://www150.statcan.gc.ca/n1/en/catalogue/92-160-X#geographicfilesanddocumentation" target="_blank">boundary files</a> provided in a multitude of formats that can be used by GIS software: ArcGIS shapefiles (.shp), Geography Markup Language files (.gml), MapInfo files (.tab), etc.

The "Working with Statistics Canada Data in R" series will follow these data types, and will consist of this Introduction and several articles about working with CANSIM data, Canadian Census data, and StatCan geospatial data.

This is not an official classification of data types available from Statistics Canada. The classification into CANSIM, census, and geographic data is for convenience only, and is loosely based on the key tools used for StatCan data retrieval and processing in R.

<a name="the-tools-you-need"></a> The Tools You Need
----------------------------------------------------

To be more specific, <a href="https://cran.r-project.org/package=cansim" target="_blank"><code>cansim</code></a> is the package designed to retrieve CANSIM data, and <a href="https://cran.r-project.org/package=cancensus" target="_blank"><code>cancensus</code></a> is the package to get census data. Further data processing will be done with the <a href="https://www.tidyverse.org/" target="_blank"><code>tidyverse</code></a> meta-package (a collection of packages that is itself a package) which is some of the most powerful data manipulation software currently available. GIS data is a more complex matter, but at the very minimum you will need <a href="https://cran.r-project.org/package=sf" target="_blank"><code>sf</code></a>, <a href="https://cran.r-project.org/package=tmap" target="_blank"><code>tmap</code></a>, and <a href="https://cran.r-project.org/package=units" target="_blank"><code>units</code></a> packages. Obviously, just as the R language, all these are completely free and open source. I am not in any way associated with the authors of any of the above packages, I just use them a lot in my work.

Note that although CANSIM has been recently renamed to Statistics Canada Data, I will be using the historic name CANSIM throughout this series in order to distinguish the data obtained from Statistics Canada Data proper from other kinds of StatCan data, i.e. census and geographic data (see how confusing this can get?).

Finally, here's the code that installs the minimum suite of packages required to run the examples from this series. Note that you might be unable to install sf and units right now, since they have system requirements such as certain libraries being installed, which don't usually come available "out of the box". More on sf and units installation in the upcoming "Working with Statistics Canada Geospatial Data" post.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"cansim"</span>, <span class='s'>"cancensus"</span>, <span class='s'>"tidyverse"</span>, <span class='s'>"tmap"</span><span class='o'>)</span><span class='o'>)</span>
<span class='c'># install.packages(c("sf", "units"))</span>
</code></pre>

</div>

