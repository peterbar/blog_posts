---
title: "Working with Statistics Canada Data in R, Part 3: Visualizing CANSIM Data"
date: "2019-11-25T00:00:00"
slug: "statcan-data-in-r-part-3-visualizing-cansim-data"
excerpt: "A detailed example of using R to visualize Statistics Canada CANSIM data, step-by-step."
status: "publish"
output: hugodown::md_document
categories: 
  - Statistics Canada
  - Data visualization
tags:
  - cansim (package)
  - Statistics Canada Data
  - data visualization
  - ggplot2
comment_status: open
ping_status: open
rmd_hash: 329fcc9be09dd055

---

<div class="highlight">

</div>

In the <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-part-2-retrieving-cansim-data/" target="_blank">previous part</a> of this series, we have retrieved CANSIM data on the weekly wages of Aboriginal and Non-Aboriginal Canadians of 25 years and older, living in Saskatchewan, and the CPI data for the same province. We then used the CPI data to adjust the wages for inflation, and saved the results as `wages_0370` dataset.

<a name="exploring-data"></a> Exploring Data
--------------------------------------------

To get started, let's take a quick look at the dataset, what types of variables it contains, which should be considered categorical, and what unique values categorical variables have:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># explore wages_0370 before plotting</span>
<span class='nf'>glimpse</span><span class='o'>(</span><span class='nv'>wages_0370</span><span class='o'>)</span>

<span class='c'>#&gt; Rows: 39</span>
<span class='c'>#&gt; Columns: 6</span>
<span class='c'>#&gt; $ year            <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span> "2007", "2007", "2007", "2008", "2008", "2008", "2009", "2009", "2009", "…</span></span>
<span class='c'>#&gt; $ group           <span style='color: #949494;font-style: italic;'>&lt;chr&gt;</span><span> "First Nations", "Métis", "Non-Aboriginal population", "First Nations", "…</span></span>
<span class='c'>#&gt; $ current_dollars <span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> 707.93, 761.75, 797.31, 675.95, 812.45, 855.09, 793.98, 807.23, 892.58, 8…</span></span>
<span class='c'>#&gt; $ cpi             <span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> 112.2, 112.2, 112.2, 115.9, 115.9, 115.9, 117.1, 117.1, 117.1, 118.7, 118…</span></span>
<span class='c'>#&gt; $ infrate         <span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> 0.00000000, 0.00000000, 0.00000000, 0.03297683, 0.03297683, 0.03297683, 0…</span></span>
<span class='c'>#&gt; $ dollars_2007    <span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> 707.93, 761.75, 797.31, 654.37, 786.51, 827.79, 760.76, 773.45, 855.23, 8…</span></span>

<span class='nf'>map</span><span class='o'>(</span><span class='nv'>wages_0370</span>, <span class='nv'>class</span><span class='o'>)</span>

<span class='c'>#&gt; $year</span>
<span class='c'>#&gt; [1] "character"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $group</span>
<span class='c'>#&gt; [1] "character"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $current_dollars</span>
<span class='c'>#&gt; [1] "numeric"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $cpi</span>
<span class='c'>#&gt; [1] "numeric"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $infrate</span>
<span class='c'>#&gt; [1] "numeric"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $dollars_2007</span>
<span class='c'>#&gt; [1] "numeric"</span>

<span class='nf'>map</span><span class='o'>(</span><span class='nv'>wages_0370</span><span class='o'>[</span><span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>1</span>, <span class='m'>2</span><span class='o'>)</span><span class='o'>]</span>, <span class='nv'>unique</span><span class='o'>)</span>

<span class='c'>#&gt; $year</span>
<span class='c'>#&gt;  [1] "2007" "2008" "2009" "2010" "2011" "2012" "2013" "2014" "2015" "2016" "2017" "2018" "2019"</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; $group</span>
<span class='c'>#&gt; [1] "First Nations"             "Métis"                     "Non-Aboriginal population"</span>
</code></pre>

</div>

The first two variables -- `year` and `group` -- are of the type "character", and \`the rest are numeric of the type "double" ("double" simply means they are not integers, i.e. can have decimals).

Also, we can see that `wages_0370` dataset is already in the <a href="https://www.jstatsoft.org/index.php/jss/article/view/v059i10/v59i10.pdf" target="_blank">tidy format</a>, which is an important prerequisite for plotting with `ggplot2` package. Since `ggplot2` is included into `tidyverse`, there is no need to install it separately.

<a name="preparing-data"></a> Preparing Data for Plotting
---------------------------------------------------------

At this point, our data is *almost* ready to be plotted, but we need to make one final change. Looking at the unique values, we can see that the first two variables (`year` and `group`) should be numeric (integer) and categorical respectively, while the rest are continuous (as they should be).

In R, categorical variables are referred to as *factors*. It is important to expressly tell R which variables are categorical, because mapping `ggplot2` aesthetics -- things that go inside `aes()` -- to a factor variable makes `ggplot2` use a discrete color scale (distinctly different colors) for different categories (different factor levels in R terms). Otherwise, values would be plotted to a gradient, i.e. different hues of the same color. There are several other reasons to make sure you expressly identify categorical variables as factors if you are planning to visualize your data. I understand that this might be a bit too technical, so if you are interested, you can find more <a href="https://cran.r-project.org/web/packages/ggplot2/ggplot2.pdf" target="_blank">here</a> and <a href="https://stackoverflow.com/questions/15070738/when-to-use-factor-when-plotting-with-ggplot-in-r" target="_blank">here</a>. For now, just remember to convert your categorical variables to factors if you are going to plot your data. Ideally, do it always -- it is a good practice to follow.

( ! ) *It is a good practice to always convert categorical variables to factors*.

So, let's do it: convert `year` to an integer, and `group` to a factor. Before doing so, let's remove the word "population" from "Non-Aboriginal population" category, so that our plot's legend takes less space inside the plot. We can also replace accented "é" with ordinary "e" to make typing in our IDE easier. Note that the order is important: first we edit the string values of a "character" class variable, and only then convert it to a factor. Otherwise, our factor will have missing levels.

( ! ) *Converting a categorical variable to a factor should be the last step in cleaning your dataset*.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>wages_0370</span> <span class='o'>&lt;-</span> <span class='nv'>wages_0370</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>group <span class='o'>=</span> <span class='nf'>str_replace_all</span><span class='o'>(</span><span class='nv'>group</span>, <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>" population"</span> <span class='o'>=</span> <span class='s'>""</span>, <span class='s'>"é"</span> <span class='o'>=</span> <span class='s'>"e"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate_at</span><span class='o'>(</span><span class='s'>"year"</span>, <span class='nv'>as.integer</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate_if</span><span class='o'>(</span><span class='nv'>is.character</span>, <span class='nv'>as_factor</span><span class='o'>)</span>
</code></pre>

</div>

Note: if you only need to remove string(s), use str\_remove or str\_remove\_all:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>mutate</span><span class='o'>(</span>group <span class='o'>=</span> <span class='nf'>str_remove</span><span class='o'>(</span><span class='nv'>group</span>, <span class='s'>" population"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

<a name="ggplot2"></a> Plotting with ggplot2
--------------------------------------------

Finally, we are ready to plot the data with `ggplot2`:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>plot_wages_0370</span> <span class='o'>&lt;-</span> 
  <span class='nv'>wages_0370</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>drop_na</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>ggplot</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>year</span>, y <span class='o'>=</span> <span class='nv'>dollars_2007</span>, 
             color <span class='o'>=</span> <span class='nv'>group</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_point</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>2</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_line</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>.7</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_label</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>dollars_2007</span><span class='o'>)</span><span class='o'>)</span>,
  <span class='c'># alt: use geom_label_repel() # requires ggrepel</span>
             fontface <span class='o'>=</span> <span class='s'>"bold"</span>,
             label.size <span class='o'>=</span> <span class='m'>.4</span>, <span class='c'># label border thickness</span>
             size <span class='o'>=</span> <span class='m'>2</span>, <span class='c'># label size</span>
             <span class='c'># force = .005, # repelling force: requires ggrepel </span>
             show.legend <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>coord_cartesian</span><span class='o'>(</span>ylim <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>650</span>, <span class='m'>1000</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># best practice to set scale limits</span>
  <span class='nf'>scale_x_continuous</span><span class='o'>(</span>breaks <span class='o'>=</span> <span class='m'>2007</span><span class='o'>:</span><span class='m'>2018</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>scale_y_continuous</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"2007 dollars"</span>,
                     breaks <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span><span class='m'>650</span>, <span class='m'>1000</span>, by <span class='o'>=</span> <span class='m'>50</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='nf'>scale_color_manual</span><span class='o'>(</span>values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"First Nations"</span> <span class='o'>=</span> <span class='s'>"tan3"</span>,
                                <span class='s'>"Non-Aboriginal"</span> <span class='o'>=</span> <span class='s'>"royalblue"</span>,
                                <span class='s'>"Metis"</span> <span class='o'>=</span> <span class='s'>"forestgreen"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme_bw</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>10</span>, 
                                  face <span class='o'>=</span> <span class='s'>"bold"</span>, 
                                  hjust <span class='o'>=</span> <span class='m'>.5</span>,
                                  margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>b <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span>,
        plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
        panel.grid.minor <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
        panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"grey85"</span><span class='o'>)</span>,
        axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
        axis.title.x <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
        axis.title.y <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                    margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>r <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span>,
        legend.title <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
        legend.position <span class='o'>=</span> <span class='s'>"bottom"</span>,
        legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>labs</span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Average Weekly Wages, Adjusted for Inflation,\nby Aboriginal Group, 25 Years and Older"</span>,
       caption <span class='o'>=</span> <span class='s'>"Wages data: Statistics Canada Data Table 14-10-0370\nInflation data: Statistics Canada Data Vector v41694489"</span><span class='o'>)</span>

<span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>plot_wages_0370</span><span class='o'>)</span>

</code></pre>
<img src="figs/make-plot-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

Let's now look at what this code does. We start with feeding our data object `wages_0370` into `ggplot` function using pipe.

( ! ) Note that `ggplot2` internal syntax differs from `tidyverse` pipe-based syntax, and uses `+` instead of `%>%` to join code into blocks.

Next, inside `ggplot(aes())` call we assign common aesthetics for all layers, and then proceed to choosing the geoms we need. If needed, we can assign additional/override common aesthetics inside individual geoms, like we did when we told `geom_label()` to use `dollars_2007` variable values (rounded to a dollar) as labels. If you'd like to find out more about what layers are, and about `ggplot2` grammar of graphics, I recommend <a href="https://byrneslab.net/classes/biol607/readings/wickham_layered-grammar.pdf" target="_blank">this article</a> by Hadley Wickham.

### <a name="choosing-geoms"></a> Choosing Plot Type

Plot type in each layer is determined by `geom_*()` functions. This is where geoms are:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>geom_point</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>2</span><span class='o'>)</span> <span class='o'>+</span>
<span class='nf'>geom_line</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>.7</span><span class='o'>)</span> <span class='o'>+</span>
<span class='nf'>geom_label</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>label <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>dollars_2007</span><span class='o'>)</span><span class='o'>)</span>,
<span class='c'># alt: use geom_label_repel() # requires ggrepel</span>
           fontface <span class='o'>=</span> <span class='s'>"bold"</span>,
           label.size <span class='o'>=</span> <span class='m'>.4</span>, <span class='c'># label border thickness</span>
           size <span class='o'>=</span> <span class='m'>2.5</span>, <span class='c'># label size</span>
           <span class='c'># force = .005, # repelling force: requires ggrepel </span>
           show.legend <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span>
</code></pre>

</div>

Choosing plot type is largely a judgement call, but you should always make sure to choose the type of graphic that would best suite the data you have. In this case, our goal is to reveal the dynamics of wages in Saskatchewan over time, hence our choice of `geom_line()`. Note that the lines in our graphic are for visual aid only -- to make it easier for an eye to follow the trend. They are not substantively meaningful like they would be, for example, in a regression plot. `geom_point()` is also there primarily for visual purposes -- to make the plot's legend more visible. Note that unlike the lines, the points in this plot are substantively meaningful, i.e. they are exactly where our data is (but are covered by labels). If you don't like the labels in the graphic, you can use points instead.

Finally, `geom_label()` plots our substantive data. Note that I am using `show.legend = FALSE argument` -- this is simply because I don't like the look of `geom_label()` legend symbols, and prefer a combined line+point symbol instead. If you prefer label symbols in the plot's legend, remove `show.legend = FALSE` argument from `geom_label()` call, and add it to `geom_line()` and `geom_point()`.

### <a name="ggrepel"></a> Preventing Overlaps with ggrepel

You have noticed some commented lines in the `ggplot()` call. You may also have noticed that some labels in our graphic overlap slightly. In this case the overlap is minute and can be ignored. But what if there are a lot of overlapping data points, enough to affect readability?

Fortunately, there is a package to solve this problem for the graphics that use text labels: <a href="https://cran.r-project.org/package=ggrepel" target="_blank"><code>ggrepel</code></a>. It has `*_repel()` versions of `ggplot2::geom_label()` and `ggplot2::geom_text()` functions, which repel the labels away from each other and away from data points.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"ggrepel"</span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='s'><a href='https://github.com/slowkow/ggrepel'>"ggrepel"</a></span><span class='o'>)</span>
</code></pre>

</div>

`ggrepel` functions can take the same arguments as corresponding `ggplot2` functions, and also take the `force` argument that defines repelling force between overlapping labels. I recommend setting it to a small value, as the default 1 seems way too strong.

Here is what our graphic looks like now. Note that the nearby labels no longer overlap:

<div class="highlight">

<img src="figs/make-plot-ggrepel-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

### <a name="axes-and-scales"></a> Axes and Scales

This is where plot axes and scales are defined:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>coord_cartesian</span><span class='o'>(</span>ylim <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='m'>650</span>, <span class='m'>1000</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># best practice to set scale limits</span>
<span class='nf'>scale_x_continuous</span><span class='o'>(</span>breaks <span class='o'>=</span> <span class='m'>2007</span><span class='o'>:</span><span class='m'>2018</span><span class='o'>)</span> <span class='o'>+</span>
<span class='nf'>scale_y_continuous</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"2007 dollars"</span>,
                   breaks <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span><span class='m'>650</span>, <span class='m'>1000</span>, by <span class='o'>=</span> <span class='m'>50</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span> 
<span class='nf'>scale_color_manual</span><span class='o'>(</span>values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"First Nations"</span> <span class='o'>=</span> <span class='s'>"tan3"</span>,
                              <span class='s'>"Non-Aboriginal"</span> <span class='o'>=</span> <span class='s'>"royalblue"</span>,
                              <span class='s'>"Metis"</span> <span class='o'>=</span> <span class='s'>"forestgreen"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

`coord_cartesian()` is the function I'd like to draw your attention to, as it is the best way to zoom the plot, i.e. to get rid of unnecessary empty space. Since we don't have any values less than 650 or more than 950 (approximately), starting our Y scale at 0 would result in a less readable plot, where most space would be empty, and the space where we actually have data would be crowded. If you are interested in why `coord_cartesian()` is the best way to set axis limits, there is an <a href="https://www.datanovia.com/en/blog/ggplot-axis-limits-and-scales/" target="_blank">in-depth explanation</a>.

( ! ) *It is a good practice to use `coord_cartesian()` to change axis limits*.

### <a name="theme-title-caption"></a> Plot Theme, Title, and Captions

Next, we edit our plot theme:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>theme_bw</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
<span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>10</span>, 
                                face <span class='o'>=</span> <span class='s'>"bold"</span>, 
                                hjust <span class='o'>=</span> <span class='m'>.5</span>,
                                margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>b <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span>,
      plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
      panel.grid.minor <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
      panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"grey85"</span><span class='o'>)</span>,
      axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
      axis.title.x <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
      axis.title.y <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                  margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>r <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span><span class='o'>)</span>,
      legend.title <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
      legend.position <span class='o'>=</span> <span class='s'>"bottom"</span>,
      legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

First I selected a simple black-and-white theme theme\_bw, and then overrode some of the theme's default settings in order to improve the plot's readability and overall appearance. Which theme and settings to use is up to you, just make sure that whatever you do makes the plot easier to read and comprehend at a glance. Here you can <a href="https://ggplot2.tidyverse.org/reference/theme.html" target="_blank">find out more about editing plot theme</a>.

Finally, we enter the plot title and plot captions. Captions are used to provide information about the sources of our data. Note the use of (new line symbol) to break strings into multiple lines:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>labs</span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Average Weekly Wages, Adjusted for Inflation,\nby Aboriginal Group, 25 Years and Older"</span>,
     caption <span class='o'>=</span> <span class='s'>"Wages data: Statistics Canada Data Table 14-10-0370\nInflation data: Statistics Canada Data Vector v41694489"</span><span class='o'>)</span>
</code></pre>

</div>

### <a name="saving-plot"></a> Saving Your Plot

The last step is to save the plot so that we can use it externally: insert into reports and other publications, publish online, etc.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># save plot</span>
<span class='nf'>ggsave</span><span class='o'>(</span><span class='s'>"plot_wages_0370.svg"</span>, <span class='nv'>plot_wages_0370</span><span class='o'>)</span>
</code></pre>

</div>

`ggsave()` takes various arguments, but only one is mandatory: file name as a string. The second argument `plot` defaults to the last plot displayed, but it is advisable to name the plot expressly to make sure the right one gets saved. You can find out more about how `ggsave()` works <a href="https://ggplot2.tidyverse.org/reference/ggsave.html" target="_blank">here</a>.

My favorite format to save graphics is SVG, which stands for <a href="https://en.wikipedia.org/wiki/Scalable_Vector_Graphics" target="_blank">Scalable Vector Graphics</a> -- an extremely lightweight vectorized format that ensures the graphic stays pixel-perfect at any resolution. Note however, that SVG is not really a pixel image like JPEG or PNG, but a bunch of XML code, which entails certain <a href="https://kinsta.com/blog/wordpress-svg/" target="_blank">security implications when using SVG files online</a>.

This was the last of the three articles about working with CANSIM data. In the next article in the "Working with Statistics Canada Data in R" series, I'll move on to working with the national census data.

