---
title: "Working with Statistics Canada Data in R, Part 1: What is CANSIM?"
date: "2019-11-04T00:00:00"
slug: "statcan-data-in-r-part-1-cansim"
excerpt: "CANSIM, also known as Statistics Canada Data, stands for Canadian Socioeconomic Information Management System. Here I'll address some of the key features of CANSIM data and explain how to set up the R tools required to work with it."
status: "publish"
output: hugodown::md_document
categories: Statistics Canada
tags:
  - cansim (package)
  - Statistics Canada Data
  - Rprofile
comment_status: open
ping_status: open
rmd_hash: e644db2e6897d5da

---

<a name="cansim-and-cansim"></a> CANSIM and cansim
--------------------------------------------------

CANSIM stands for Canadian Socioeconomic Information Management System. Not long ago it was renamed to "Statistics Canada Data," but here I'll be using the repository's legacy name to avoid confusion with <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-introduction/#data-types" target="_blank">other kinds of data</a> available from Statistics Canada. At the time of writing this, there were over 8,700 data tables in CANSIM, with new tables being added virtually every day. It is the most current source of publicly available socioeconomic data collected by the Government of Canada.

Although CANSIM can be directly accessed <a href="https://www150.statcan.gc.ca/n1/en/type/data" target="_blank">online</a>, a much more convenient and reproducible way to access it, is through Census Mapper API using the excellent <a href="https://github.com/mountainMath/cansim" target="_blank"><code>cansim</code></a> package developed by Dmitry Shkolnik and Jens von Bergmann. This package allows to search for relevant data tables quickly and precisely, and to read data directly into R without the intermediate steps of manually downloading and unpacking multiple archives, followed by reading each dataset separately into statistical software.

-   <a href="https://mountainmath.github.io/cansim/" target="_blank">cansim tutorial</a>
-   <a href="https://cran.r-project.org/package=cansim" target="_blank">cansim on CRAN</a>

To install and load `cansim`, run:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"cansim"</span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='s'><a href='https://github.com/mountainMath/cansim'>"cansim"</a></span><span class='o'>)</span>
</code></pre>

</div>

Let's also install and load <a href="https://www.tidyverse.org/" target="_blank"><code>tidyverse</code></a>, as we'll be using it a lot:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span><span class='o'>(</span><span class='s'>"tidyverse"</span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='s'><a href='http://tidyverse.tidyverse.org'>"tidyverse"</a></span><span class='o'>)</span>
</code></pre>

</div>

<a name="what-to-expect"></a> What to Expect
--------------------------------------------

So why do we have to use `tidyverse` in addition to `cansim` (apart from the fact that `tidyverse` is probably the best data processing software in existence)? Well, data in Statistics Canada CANSIM repository has certain ~~bugs~~ features that one needs to be aware of, and which can best be fixed with the instruments included in `tidyverse`:

First, it is not always easy to find and retrieve the data manually. After all, you'll have to search through thousands of data tables looking for the data you need. <a href="https://www150.statcan.gc.ca/n1/en/type/data" target="_blank">Online search tool</a> often produces results many pages long, which are not well sorted by relevance (or at least that is my impression).

Second, StatCan data is not in the <a href="https://www.jstatsoft.org/article/view/v059i10/v59i10.pdf" target="_blank">tidy format</a>, and usually needs to be transformed as such, which is important for convenience and error prevention, as well as for plotting data.

Third, don't expect the main principles of organizing data into datasets to be observed. For example, multiple variables can be presented as values in the same column of a dataset, with corresponding values stored in the other column, instead of each variable assigned to its own column with values stored in that column (as it should be). If this sounds confusing, the code snippet below will give you a clear example.

Next, the datasets contain multiple irrelevant variables (columns), that result form how Statistics Canada processes and structures data, and do not have much substantive information.

Finally, CANSIM datasets contain a lot of text, i.e. strings, which often are unnecessarily long and cumbersome, and are sometimes prone to typos. Moreover, numeric variables are often incorrectly stored as class "character".

If you'd like an example of a dataset exhibiting most of these issues, let's looks at the responses from unemployed Aboriginal Canadians about why they experience difficulties in finding a job. To reduce the size of the dataset, let's limit it to one province. Run line-by-line:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># Get data</span>
<span class='nv'>jobdif_0014</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://mountainmath.github.io/cansim/index.html/reference/get_cansim.html'>get_cansim</a></span><span class='o'>(</span><span class='s'>"41-10-0014"</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>GEO</span> <span class='o'>==</span> <span class='s'>"Saskatchewan"</span><span class='o'>)</span>

<span class='c'># Examine the dataset - lots of redundant variables.</span>
<span class='nf'>glimpse</span><span class='o'>(</span><span class='nv'>jobdif_0014</span>, width <span class='o'>=</span> <span class='m'>120</span><span class='o'>)</span>

<span class='c'># All variables except VALUE are of class "character",</span>
<span class='nf'>map</span><span class='o'>(</span><span class='nv'>jobdif_0014</span>, <span class='nv'>class</span><span class='o'>)</span>

<span class='c'># although some contain numbers, not text.</span>
<span class='nf'>map</span><span class='o'>(</span><span class='nv'>jobdif_0014</span>, <span class='nv'>head</span><span class='o'>)</span>

<span class='c'># Column "Statistics" contains variables’ names instead of values,</span>
<span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='nv'>jobdif_0014</span><span class='o'>$</span><span class='nv'>Statistics</span><span class='o'>)</span>

<span class='c'># while corresponding values are in a totally different column.</span>
<span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>jobdif_0014</span><span class='o'>$</span><span class='nv'>VALUE</span>, <span class='m'>10</span><span class='o'>)</span>
</code></pre>

</div>

<a name="before-you-start"></a> Before You Start
------------------------------------------------

This is an optional step. You can skip it, although if you are planning to use `cansim` often, you probably shouldn't.

Before you start working with the package, `cansim` authors recommend to set up `cansim` cache to substantially increase the speed of repeated data retrieval from StatCan repositories and to minimize web scraping. `cansim` cache is not persistent between sessions, so do this either at the start of each session, or set the cache path permanently in your `Rprofile` file (more on editing `Rprofile` in <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-4-census-data-cancensus-setup/" target="_blank">Part 4</a> of these series).

Run (or add to `Rprofile`):

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/options.html'>options</a></span><span class='o'>(</span>cansim.cache_path <span class='o'>=</span> <span class='s'>"your cache path"</span><span class='o'>)</span>
</code></pre>

</div>

If your code is going to be executed on different machines, keep in mind that Linux and Windows paths to cache will not be the same:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'># Linux path example:
options(cansim.cache_path = "/home/username/Documents/R/.cansim_cache")

# Windows path example:
options(cansim.cache_path = "C:\Users\username\Documents\R\cansim_cache")
</code></pre>

</div>

