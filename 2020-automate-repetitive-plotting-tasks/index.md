---
title: 'Writing Functions to Automate Repetitive Plotting Tasks in ggplot2'
date: '2020-07-08T00:00:00'
slug: 'writing-functions-to-automate-repetitive-plotting-tasks'
excerpt: 'Sooner or later (usually sooner) a data analyst runs into a situation where one needs to make multiple visualizations on the same subject, which can be slow and time-consuming. By writing simple functions, it is possible to automate most of the work, thus greatly simplifying and speeding up repetitive plotting tasks.'
status: 'publish'
output: hugodown::md_document
categories: 
  - Data visualization
  - Programming
tags:
  - Canadian Census data
  - functional programming
  - data visualization
  - ggplot2
comment_status: open
ping_status: open
bibliography: 'penguins.bib'
csl: 'chicago-author-date-17th-ed.csl'
link-citations: yes
rmd_hash: 37ced5ebaf3707ba

---

<a name="intro"></a> Introduction
---------------------------------

There are often situations when you need to perform repetitive plotting tasks. For example, you'd like to plot the same kind of data (e.g. the same economic indicator) for several states, provinces, or cities. Here are some ways you can address this:

-   You can try to fit all the data into the same plot. Often it works just fine, especially if the data is <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-6-visualizing-census-data/#making-an-ordered-bar-plot" target="_blank">simple and can easily fit into the plot</a>.
-   Another option is to create a <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-6-visualizing-census-data/#making-a-faceted-donut-plot" target="_blank">faceted plot</a>, broken down by whatever grouping variable you choose (e.g. by city or region).

But what if the data is too complex to fit into a single plot? Or maybe there are just too many levels in your grouping variable -- for example, if you try to plot family income data for all 50 U.S. states, a plot made up of 50 facets would be virtually unreadable. Same goes for a plot with all 50 states on its X axis.

Yet another example of a repetitive plotting task is when you'd like to use your own custom plot theme for your plots.

Both use cases -- making multiple plots on the same subject, and using the same theme for multiple plots -- require the same R code to run over and over again. Of course, you can simply duplicate your code (with necessary changes), but this is tedious and not optimal, putting it mildly. In case of plotting data for all 50 U.S. states, would you copy and paste the same chunk of code 50 times?

Fortunately, there is a much better way -- simply write a function that will iteratively run the code as many times as you need.

<a name="multiple-plots-same-subject"></a> Making Multiple Plots on the Same Subject
------------------------------------------------------------------------------------

Lets' start with a more complex use case -- making multiple plots on the same subject. To illustrate this, I will be using the 'education' dataset that contains education levels of people aged 25 to 64, broken down by gender, according to 2016 Canadian Census. You may consider this post to be a continuation of <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-6-visualizing-census-data/" target="_blank">Part 6</a> of the Working with Statistics Canada Data in R series.

You can find the code that retrieves the data using the specialized [`cancensus`](https://cran.r-project.org/package=cancensus) package [here](https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/). If you are not interested in Statistics Canada data, you can simply download the dataset and read it into R:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/download.file.html'>download.file</a></span><span class='o'>(</span>url <span class='o'>=</span> <span class='s'>"https://dataenthusiast.ca/wp-content/uploads/2020/12/education.csv"</span>, 
              destfile <span class='o'>=</span> <span class='s'>"education.csv"</span><span class='o'>)</span>

<span class='nv'>education</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/utils/read.table.html'>read.csv</a></span><span class='o'>(</span><span class='s'>"education.csv"</span>, stringsAsFactors <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
</code></pre>

</div>

Let's take a look at the first 20 lines of the 'education' dataset (all data for the 'Canada' region):

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/head.html'>head</a></span><span class='o'>(</span><span class='nv'>education</span>, <span class='m'>20</span><span class='o'>)</span>

<span class='c'>#&gt; <span style='color: #949494;'># A tibble: 20 x 5</span></span>
<span class='c'>#&gt;    region vector        count gender level                        </span>
<span class='c'>#&gt;    <span style='color: #949494;font-style: italic;'>&lt;fct&gt;</span><span>  </span><span style='color: #949494;font-style: italic;'>&lt;fct&gt;</span><span>         </span><span style='color: #949494;font-style: italic;'>&lt;dbl&gt;</span><span> </span><span style='color: #949494;font-style: italic;'>&lt;fct&gt;</span><span>  </span><span style='color: #949494;font-style: italic;'>&lt;fct&gt;</span><span>                        </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 1</span><span> Canada v_CA16_5100 1</span><span style='text-decoration: underline;'>200</span><span>105 Male   None                         </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 2</span><span> Canada v_CA16_5101  </span><span style='text-decoration: underline;'>969</span><span>690 Female None                         </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 3</span><span> Canada v_CA16_5103 2</span><span style='text-decoration: underline;'>247</span><span>025 Male   High school or equivalent    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 4</span><span> Canada v_CA16_5104 2</span><span style='text-decoration: underline;'>247</span><span>565 Female High school or equivalent    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 5</span><span> Canada v_CA16_5109 1</span><span style='text-decoration: underline;'>377</span><span>775 Male   Apprenticeship or trades     </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 6</span><span> Canada v_CA16_5110  </span><span style='text-decoration: underline;'>664</span><span>655 Female Apprenticeship or trades     </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 7</span><span> Canada v_CA16_5118 1</span><span style='text-decoration: underline;'>786</span><span>060 Male   College or equivalent        </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 8</span><span> Canada v_CA16_5119 2</span><span style='text-decoration: underline;'>455</span><span>920 Female College or equivalent        </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'> 9</span><span> Canada v_CA16_5121  </span><span style='text-decoration: underline;'>240</span><span>035 Male   University below bachelor    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>10</span><span> Canada v_CA16_5122  </span><span style='text-decoration: underline;'>340</span><span>850 Female University below bachelor    </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>11</span><span> Canada v_CA16_5130  </span><span style='text-decoration: underline;'>151</span><span>210 Male   Cert. or dipl. above bachelor</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>12</span><span> Canada v_CA16_5131  </span><span style='text-decoration: underline;'>211</span><span>250 Female Cert. or dipl. above bachelor</span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>13</span><span> Canada v_CA16_5127 1</span><span style='text-decoration: underline;'>562</span><span>155 Male   Bachelor's degree            </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>14</span><span> Canada v_CA16_5128 2</span><span style='text-decoration: underline;'>027</span><span>925 Female Bachelor's degree            </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>15</span><span> Canada v_CA16_5133   </span><span style='text-decoration: underline;'>74</span><span>435 Male   Degree in health**           </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>16</span><span> Canada v_CA16_5134   </span><span style='text-decoration: underline;'>78</span><span>855 Female Degree in health**           </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>17</span><span> Canada v_CA16_5136  </span><span style='text-decoration: underline;'>527</span><span>335 Male   Master's degree              </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>18</span><span> Canada v_CA16_5137  </span><span style='text-decoration: underline;'>592</span><span>850 Female Master's degree              </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>19</span><span> Canada v_CA16_5139  </span><span style='text-decoration: underline;'>102</span><span>415 Male   Doctorate*                   </span></span>
<span class='c'>#&gt; <span style='color: #BCBCBC;'>20</span><span> Canada v_CA16_5140   </span><span style='text-decoration: underline;'>73</span><span>270 Female Doctorate*</span></span>
</code></pre>

</div>

Our goal is to plot education levels (as percentages) for both genders, and for all regions. This is a good example of a repetitive plotting task, as we'll be making one plot for each region. Overall, there are 6 regions, so we'll be making 6 plots:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/levels.html'>levels</a></span><span class='o'>(</span><span class='nv'>education</span><span class='o'>$</span><span class='nv'>region</span><span class='o'>)</span>

<span class='c'>#&gt; [1] "Canada"     "Halifax"    "Toronto"    "Calgary"    "Vancouver"  "Whitehorse"</span>
</code></pre>

</div>

Ideally, our plot should also reflect the hierarchy of education levels.

### <a name="preparing-data"></a> Preparing the Data

The data, as retrieved from Statistics Canada in <a href="https://dataenthusiast.ca/2020/statcan-data-in-r-5-retrieving-census-data/" target="_blank">Part 5</a> of the Working with Statistics Canada Data in R series, is not yet ready for plotting: it doesn't have percentages, only counts. Also, education levels are almost, but not quite, in the correct order: the 'Cert. or dipl. above bachelor' is before 'Bachelor's degree', while it should of course *follow* the Bachelor's degree.

So let's apply some final touches to our dataset, after which it will be ready for plotting. First, lets load tidyverse:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://tidyverse.tidyverse.org'>tidyverse</a></span><span class='o'>)</span>
</code></pre>

</div>

Then let's calculate percentages and re-level the `levels` variable:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># prepare 'education' dataset for plotting</span>
<span class='nv'>education</span> <span class='o'>&lt;-</span> <span class='nv'>education</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>region</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>percent <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/Round.html'>round</a></span><span class='o'>(</span><span class='nv'>count</span><span class='o'>/</span><span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span><span class='nv'>count</span><span class='o'>)</span><span class='o'>*</span><span class='m'>100</span>, <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>mutate</span><span class='o'>(</span>level <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/factor.html'>factor</a></span><span class='o'>(</span><span class='nv'>level</span>, <span class='c'># put education levels in logical order</span>
                        levels <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"None"</span>, 
                                   <span class='s'>"High school or equivalent"</span>, 
                                   <span class='s'>"Apprenticeship or trades"</span>, 
                                   <span class='s'>"College or equivalent"</span>, 
                                   <span class='s'>"University below bachelor"</span>, 
                                   <span class='s'>"Bachelor's degree"</span>, 
                                   <span class='s'>"Cert. or dipl. above bachelor"</span>, 
                                   <span class='s'>"Degree in health**"</span>, 
                                   <span class='s'>"Master's degree"</span>, 
                                   <span class='s'>"Doctorate*"</span><span class='o'>)</span><span class='o'>)</span><span class='o'>)</span>
</code></pre>

</div>

Note that we needed to group the data by the `region` variable to make sure our percentages get calculated correctly, i.e. by region. If you are not sure if the dataset has been grouped already, you can check this with the [`dplyr::is_grouped_df()`](https://dplyr.tidyverse.org/reference/grouped_df.html) function.

### <a name="repetitive-plotting-function"></a> Writing Functions to Generate Multiple Plots

Now our data is ready to be plotted, so let's write a function that will sequentially generate our plots -- one for each region. Pay attention to the comments in the code:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'>## plot education data</span>

<span class='c'># a function for sequential graphing of data by region</span>
<span class='nv'>plot.education</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='nv'>x</span> <span class='o'>=</span> <span class='nv'>education</span><span class='o'>)</span> <span class='o'>&#123;</span>
  
  <span class='c'># a vector of names of regions to loop over</span>
  <span class='nv'>regions</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/unique.html'>unique</a></span><span class='o'>(</span><span class='nv'>x</span><span class='o'>$</span><span class='nv'>region</span><span class='o'>)</span>
  
  <span class='c'># a loop to produce ggplot2 graphics </span>
  <span class='kr'>for</span> <span class='o'>(</span><span class='nv'>i</span> <span class='kr'>in</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq_along</a></span><span class='o'>(</span><span class='nv'>regions</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>&#123;</span>
    
    <span class='c'># make plots; note data = args in each geom</span>
    <span class='nv'>plot</span> <span class='o'>&lt;-</span> <span class='nv'>x</span> <span class='o'>%&gt;%</span> 
      <span class='nf'>ggplot</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>level</span>, fill <span class='o'>=</span> <span class='nv'>gender</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>geom_col</span><span class='o'>(</span>data <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>x</span>, 
                             <span class='nv'>region</span> <span class='o'>==</span> <span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, 
                             <span class='nv'>gender</span> <span class='o'>==</span> <span class='s'>"Male"</span><span class='o'>)</span>, 
               <span class='nf'>aes</span><span class='o'>(</span>y <span class='o'>=</span> <span class='nv'>percent</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>geom_col</span><span class='o'>(</span>data <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>x</span>, 
                             <span class='nv'>region</span> <span class='o'>==</span> <span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, 
                             <span class='nv'>gender</span> <span class='o'>==</span> <span class='s'>"Female"</span><span class='o'>)</span>, 
               <span class='c'># multiply by -1 to plot data left of 0 on the X axis</span>
               <span class='nf'>aes</span><span class='o'>(</span>y <span class='o'>=</span> <span class='o'>-</span><span class='m'>1</span><span class='o'>*</span><span class='nv'>percent</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>  
      <span class='nf'>geom_text</span><span class='o'>(</span>data <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>x</span>, 
                              <span class='nv'>region</span> <span class='o'>==</span> <span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, 
                              <span class='nv'>gender</span> <span class='o'>==</span> <span class='s'>"Male"</span><span class='o'>)</span>, 
                <span class='nf'>aes</span><span class='o'>(</span>y <span class='o'>=</span> <span class='nv'>percent</span>, label <span class='o'>=</span> <span class='nv'>percent</span><span class='o'>)</span>, 
                hjust <span class='o'>=</span> <span class='o'>-</span><span class='m'>.1</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>geom_text</span><span class='o'>(</span>data <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>x</span>, 
                              <span class='nv'>region</span> <span class='o'>==</span> <span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, 
                              <span class='nv'>gender</span> <span class='o'>==</span> <span class='s'>"Female"</span><span class='o'>)</span>, 
                <span class='nf'>aes</span><span class='o'>(</span>y <span class='o'>=</span> <span class='o'>-</span><span class='m'>1</span><span class='o'>*</span><span class='nv'>percent</span>, label <span class='o'>=</span> <span class='nv'>percent</span><span class='o'>)</span>, 
                hjust <span class='o'>=</span> <span class='m'>1.1</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>expand_limits</span><span class='o'>(</span>y <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='o'>-</span><span class='m'>17</span>, <span class='m'>17</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>scale_y_continuous</span><span class='o'>(</span>breaks <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/seq.html'>seq</a></span><span class='o'>(</span><span class='o'>-</span><span class='m'>15</span>, <span class='m'>15</span>, by <span class='o'>=</span> <span class='m'>5</span><span class='o'>)</span>,
                         labels <span class='o'>=</span> <span class='nv'>abs</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># axes labels as absolute values</span>
      <span class='nf'>scale_fill_manual</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Gender"</span>,
                        values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Male"</span> <span class='o'>=</span> <span class='s'>"deepskyblue2"</span>,
                                   <span class='s'>"Female"</span> <span class='o'>=</span> <span class='s'>"coral1"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>coord_flip</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>theme_bw</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>14</span>, face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                      hjust <span class='o'>=</span> <span class='m'>.5</span>,
                                      margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>5</span>, b <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span>,
            plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>12</span>, hjust <span class='o'>=</span> <span class='m'>0</span>, 
                                        margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span>,
            panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"grey88"</span><span class='o'>)</span>,
            panel.grid.minor <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
            legend.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>13</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
            legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>12</span><span class='o'>)</span>,
            axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>12</span>, color <span class='o'>=</span> <span class='s'>"black"</span><span class='o'>)</span>,
            axis.title.x <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span>,
                                        size <span class='o'>=</span> <span class='m'>13</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
            axis.title.y <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>r <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span>,
                                        size <span class='o'>=</span> <span class='m'>13</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
      <span class='nf'>labs</span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>"Education level"</span>, 
           y <span class='o'>=</span> <span class='s'>"Percent of population"</span>, 
           fill <span class='o'>=</span> <span class='s'>"Gender"</span>,
           title <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste0</a></span><span class='o'>(</span><span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>, <span class='s'>": "</span>, <span class='s'>"Percentage of Population by Highest Education Level, 2016"</span><span class='o'>)</span>,
           caption <span class='o'>=</span> <span class='s'>"* Doesn’t include honorary doctorates.\n** A degree in medicine, dentistry, veterinary medicine, or optometry.\nData: Statistics Canada 2016 Census."</span><span class='o'>)</span>
    
    <span class='c'># create folder to save the plots to</span>
    <span class='kr'>if</span> <span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.exists</a></span><span class='o'>(</span><span class='s'>"output"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>&#123;</span> <span class='o'>&#125;</span> 
      <span class='kr'>else</span> <span class='o'>&#123;</span><span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.create</a></span><span class='o'>(</span><span class='s'>"output"</span><span class='o'>)</span><span class='o'>&#125;</span>
    
    <span class='c'># save plots to the 'output' folder</span>
    <span class='nf'>ggsave</span><span class='o'>(</span>filename <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/paste.html'>paste0</a></span><span class='o'>(</span><span class='s'>"output/"</span>,
                             <span class='nv'>regions</span><span class='o'>[</span><span class='nv'>i</span><span class='o'>]</span>,
                             <span class='s'>"_plot_education.png"</span><span class='o'>)</span>,
           plot <span class='o'>=</span> <span class='nv'>plot</span>,
           width <span class='o'>=</span> <span class='m'>11</span>, height <span class='o'>=</span> <span class='m'>8.5</span>, units <span class='o'>=</span> <span class='s'>"in"</span><span class='o'>)</span>
    
    <span class='c'># print each plot to screen</span>
    <span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>plot</span><span class='o'>)</span>
  <span class='o'>&#125;</span>
<span class='o'>&#125;</span>
</code></pre>

</div>

Let's now look in detail at the key sections of this code. First, we start with creating a vector of regions' names for our function to loop over, and then we follow with a simple for-loop: `for (i in seq_along(regions))`. We put our plotting code inside the loop's curly brackets `{ }`.

Note the `data =` argument in each geom: `region == regions[i]` tells `ggplot()` to take the data that corresponds to each element of the `regions` vector, for each new iteration of the for-loop.

Since we want our plot to reflect the hierarchy of education levels and to show the data by gender, the best approach would be to plot the data as a pyramid, with one gender being to the left of the center line, and the other -- to the right. This is why each geom is plotted twice, with the [`dplyr::filter()`](https://dplyr.tidyverse.org/reference/filter.html) function used to subset the data.

The `y = -1*percent` argument to the `aes()` function tells the geom to plot the data to the left of the 0 center line. It has to be accompanied by `labels = abs` argument to `scale_y_continuous()`, which tells this function to use absolute values for the Y axis labels, since you obviously can't have a negative percentage of people with a specific education level.

Note also the `expand_limits(y = c(-17, 17))`, which ensures that axis limits stay the same in all plots generated by our function. This is one of those rare cases when `expand_limits()` is preferable to `coord_flip()`, since with `expand_limits()` axis limits stay the same in all auto-generated plots. However, keep in mind that `expand_limits()` trims observations outside of the set range from the data, so it should be used with caution. More on this <a href="https://dataenthusiast.ca/2019/statcan-data-in-r-part-3-visualizing-cansim-data/#axes-and-scales" target="_blank">here</a> and [here](https://www.datanovia.com/en/blog/ggplot-axis-limits-and-scales/#key-ggplot2-r-functions).

Next, `coord_flip()` converts bar plot into a pyramid, so that education levels are on the Y axis, and percentages are on the X axis.

Finally, note how our for-loop uses `regions[i]` inside the `labs()` function to iteratively add the names of the regions to the plots' titles, and to correctly name each file when saving our plots with `ggsave()`.

To generate the plots, run:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>plot.education</span><span class='o'>(</span><span class='o'>)</span>
</code></pre>

</div>

Here is one of our plots:

<div class="highlight">

<img src="figs/Canada_plot_education.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

If you did everything correctly, there should be five more graphics like this one in your "output" folder -- one for each region in our dataset.

<a name="making-custom-themes"></a> Making Custom Plot Themes
-------------------------------------------------------------

The other way how you can simplify repetitive plotting tasks, is by making your own custom plot themes. Since every plot theme in `ggplot2` is a function, you can easily save your favorite [theme settings](https://ggplot2.tidyverse.org/reference/theme.html) as a custom-made function. Making a theme is easier than writing functions to generate multiple plots, as you won't have to write any loops.

Suppose, you'd like to save the theme of our education plots, and to use it in other plots. To do this, simply wrap theme settings in `function()`:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'>## Save custom theme as a function ##</span>
<span class='nv'>theme_custom</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>&#123;</span>
  <span class='nf'>theme_bw</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># note ggplot2 theme is used as a basis</span>
  <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>10</span>, face <span class='o'>=</span> <span class='s'>"bold"</span>,
                                  hjust <span class='o'>=</span> <span class='m'>.5</span>,
                                  margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>5</span>, b <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span>,
        plot.caption <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span>, hjust <span class='o'>=</span> <span class='m'>0</span>, 
                                    margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>15</span><span class='o'>)</span><span class='o'>)</span>,
        panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>colour <span class='o'>=</span> <span class='s'>"grey88"</span><span class='o'>)</span>,
        panel.grid.minor <span class='o'>=</span> <span class='nf'>element_blank</span><span class='o'>(</span><span class='o'>)</span>,
        legend.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>9</span><span class='o'>)</span>,
        axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>8</span><span class='o'>)</span>,
        axis.title.x <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>t <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span>,
                                    size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span>,
        axis.title.y <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>margin <span class='o'>=</span> <span class='nf'>margin</span><span class='o'>(</span>r <span class='o'>=</span> <span class='m'>10</span><span class='o'>)</span>,
                                    size <span class='o'>=</span> <span class='m'>9</span>, face <span class='o'>=</span> <span class='s'>"bold"</span><span class='o'>)</span><span class='o'>)</span>
<span class='o'>&#125;</span>
</code></pre>

</div>

Note that this code takes one of `ggplot2` themes as a basis, and then alters some of its elements to our liking. You can change any theme like this: a `ggplot2` theme, a custom theme from another package such as [`ggthemes`](https://cran.r-project.org/package=ggthemes), or your own custom theme.

Let's now use the saved theme in a plot. Usually it doesn't matter what kind of data we are going to visualize, as themes tend to be rather universal. Note however, that sometimes the data and the type of visualization do matter. For example, our `theme_custom()` won't work for a pie chart, because our theme has grid lines and labelled X and Y axes.

To illustrate how this theme fits an entirely different kind of data, let's plot some data about penguins. Why penguins? Because I love [Linux](https://en.wikipedia.org/wiki/Tux_(mascot))!

The data was originally presented in (Gorman, Williams, and Fraser [2014](#ref-Gorman2014)) and recently released as the [`palmerpenguins`](https://cran.r-project.org/package=palmerpenguins) package. It contains various measurements of 3 species of penguins (discovered via [@allison\_horst](https://twitter.com/allison_horst/status/1270046399418138625)). The package is quite educational: for example, I learned that Gentoo is not only a [Linux](https://en.wikipedia.org/wiki/Gentoo_Linux), but also a [penguin](https://en.wikipedia.org/wiki/Gentoo_penguin)!

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://allisonhorst.github.io/palmerpenguins/'>palmerpenguins</a></span><span class='o'>)</span>
</code></pre>

</div>

Let's now make a scatterplot showing the relationship between the bill length and body mass in the three species of penguins from `palmerpenguins`. Let's also add regression lines with 95% confidence intervals to our plot, and apply our custom-made theme:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'>## Plot penguins data with a custom theme</span>
<span class='nv'>plot_penguins</span> <span class='o'>&lt;-</span> 
  <span class='nv'>penguins</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>group_by</span><span class='o'>(</span><span class='nv'>species</span><span class='o'>)</span> <span class='o'>%&gt;%</span> 
  <span class='nf'>ggplot</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>x <span class='o'>=</span> <span class='nv'>bill_length_mm</span>,
             y <span class='o'>=</span> <span class='nv'>body_mass_g</span>,
             color <span class='o'>=</span> <span class='nv'>species</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_point</span><span class='o'>(</span>size <span class='o'>=</span> <span class='m'>1</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>geom_smooth</span><span class='o'>(</span><span class='nf'>aes</span><span class='o'>(</span>fill <span class='o'>=</span> <span class='nv'>species</span><span class='o'>)</span>, 
              formula <span class='o'>=</span> <span class='nv'>y</span> <span class='o'>~</span> <span class='nv'>x</span>, <span class='c'># optional: removes message</span>
              method <span class='o'>=</span> <span class='s'>"lm"</span>, 
              alpha <span class='o'>=</span> <span class='m'>.3</span>, <span class='c'># alpha level for conf. interval</span>
              na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>+</span> 
  <span class='c'># Note that you need identical name, values, and labels (if any)</span>
  <span class='c'># in both manual scales to avoid legend duplication:</span>
  <span class='c'># this merges two legends into one.</span>
  <span class='nf'>scale_color_manual</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Species"</span>,
                     values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Adelie"</span> <span class='o'>=</span> <span class='s'>"orange2"</span>,
                                <span class='s'>"Chinstrap"</span> <span class='o'>=</span> <span class='s'>"dodgerblue"</span>,
                                <span class='s'>"Gentoo"</span> <span class='o'>=</span> <span class='s'>"orchid"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>scale_fill_manual</span><span class='o'>(</span>name <span class='o'>=</span> <span class='s'>"Species"</span>,
                    values <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='s'>"Adelie"</span> <span class='o'>=</span> <span class='s'>"orange2"</span>,
                               <span class='s'>"Chinstrap"</span> <span class='o'>=</span> <span class='s'>"dodgerblue"</span>,
                               <span class='s'>"Gentoo"</span> <span class='o'>=</span> <span class='s'>"orchid"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme_custom</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span> <span class='c'># here is our custom theme</span>
  <span class='nf'>labs</span><span class='o'>(</span>x <span class='o'>=</span> <span class='s'>"Bill length, mm"</span>,
       y <span class='o'>=</span> <span class='s'>"Body mass, grams"</span>,
       title <span class='o'>=</span> <span class='s'>"Body Mass to Bill Length in Adelie, Chinstrap, and Gentoo Penguins"</span>,
       caption <span class='o'>=</span> <span class='s'>"Data: Gorman, Williams, and Fraser 2014"</span><span class='o'>)</span>
</code></pre>

</div>

As usual, let's save the plot to the 'output' folder and print it to screen:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>ggsave</span><span class='o'>(</span><span class='s'>"output/plot_penguins.png"</span>, 
       <span class='nv'>plot_penguins</span>,
       width <span class='o'>=</span> <span class='m'>11</span>, height <span class='o'>=</span> <span class='m'>8.5</span>, units <span class='o'>=</span> <span class='s'>"in"</span><span class='o'>)</span>
</code></pre>

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/print.html'>print</a></span><span class='o'>(</span><span class='nv'>plot_penguins</span><span class='o'>)</span>

</code></pre>
<img src="figs/print-plot-penguins-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

### <a name="updating-custom-themes"></a> Updating Plot Themes

Now, suppose your organization uses a green-colored theme for their website and reports, so your penguin data plot needs to fit the overall style. Fortunately, updating a custom theme is very easy: you re-assign those theme elements you'd like to change, e.g. to use a different color:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='c'># further change some elements of our custom theme</span>
<span class='nv'>theme_custom_green</span> <span class='o'>&lt;-</span> <span class='kr'>function</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>&#123;</span>
  <span class='nf'>theme_custom</span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'>theme</span><span class='o'>(</span>plot.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        plot.caption <span class='o'>=</span>  <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        panel.border <span class='o'>=</span> <span class='nf'>element_rect</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        axis.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        axis.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        axis.ticks <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        legend.title <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        legend.text <span class='o'>=</span> <span class='nf'>element_text</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"darkgreen"</span><span class='o'>)</span>,
        panel.grid.major <span class='o'>=</span> <span class='nf'>element_line</span><span class='o'>(</span>color <span class='o'>=</span> <span class='s'>"#00640025"</span><span class='o'>)</span><span class='o'>)</span>
<span class='o'>&#125;</span>
</code></pre>

</div>

Upd.: Note the use of an 8-digit hex color code in the last line: it is the hex value for the "darkgreen" color with an alpha-level of 25. This is how you can change the transparency of grid lines so that they don't stand out too much, since `element_line()` doesn't take the `alpha` argument. Keep in mind that if you want to use a color other than "gray" (or "grey") for grid lines, you'd have to use actual hex values, not color names. Setting `element_line(color = “darkgreen25”)` would throw an error. You can find more about hex code colors with alpha values [here](https://alligator.io/css/hex-code-colors-alpha-values). Thanks to [@PhilSmith26](https://twitter.com/PhilSmith26) for the tip!

Then simply replace `theme_custom()` in the code above with `theme_custom_green()`. No other changes needed!

<div class="highlight">

<img src="figs/plot-penguins-green-1.png" width="100%" style="display: block; margin: auto auto auto 0;" />

</div>

And last but not least, here is the citation for the penguins data:

<div id="refs" class="references">

<div id="ref-Gorman2014">

Gorman, Kristen B., Tony D. Williams, and William R. Fraser. 2014. "Ecological sexual dimorphism and environmental variability within a community of Antarctic penguins (Genus Pygoscelis)." *PLoS ONE* 9 (3). <https://doi.org/10.1371/journal.pone.0090081>.

</div>

</div>

