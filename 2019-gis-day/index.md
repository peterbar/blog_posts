---
title: "GIS Day!"
date: "2019-11-14T00:00:00"
slug: "gis-day"
excerpt: "A collection of links to some great R-spatial resources, sources of geospatial data, and other GIS-related stuff that you may find useful."
status: "publish"
output: hugodown::md_document
categories: R-spatial
tags:
  - GIS
  - R-spatial
comment_status: open
ping_status: open
rmd_hash: e1f7d07c6649994b

---

Yesterday was a <a href="https://www.gisday.com/en-us/discover-gis" target="_blank">GIS Day</a>! Congratulations to everyone who loves maps, GIS, and working with spatial data! And as you may have guessed, I am one of those people. So, to mark the day, here are some links to great sources of geospatial data and other GIS-related stuff that you may find useful:

<a href="https://wiki.openstreetmap.org/wiki/Shapefiles#Download_shapefiles" target="_blank">OpenStreetMap (OSM) data</a>: a list of sources of geospatial data from <a href="https://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> project.

<a href="https://gadm.org/index.html" target="_blank">GADM</a>: Global Administrative Areas Database. GADM goal is to map the administrative areas of all countries, at all levels of sub-division at high spatial resolution. Very R-friendly: stores data as .rds files formatted for <a href="https://cran.r-project.org/package=sf" target="_blank">sf</a> and <a href="https://cran.r-project.org/package=sp" target="_blank">sp</a> packages, in Geopackage (.gpkg) format, as shapefiles (.shp), and as KMZ files.

<a href="https://canadiangis.com/" target="_blank">Canadian GIS</a>: a collection of Canadian GIS and geospatial resources.

<a href="https://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/bound-limit-2016-eng.cfm" target="_blank">Statistics Canada 2016 Census Boundary Files</a>: Statistics Canada official geospatial data repository.

<a href="https://censusmapper.ca/" target="_blank">CensusMapper</a>: an API and an online tool to create interactive custom maps based on Statistics Canada census data. CensusMapper API is used in the <a href="https://github.com/mountainMath/cancensus" target="_blank"><code>cancensus</code></a> R package.

<a href="https://boundingbox.klokantech.com/" target="_blank">Bounding Box Tool</a>: what it says on the tin. Can be very handy when you are making maps and need coordinates for a bounding box.

<a href="https://geocompr.robinlovelace.net/" target="_blank">Geocomputation with R</a>: an excellent (and very up-to-date) textbook on using R to work with geospatial data. Has a free online version and a print version.

<a href="https://www.r-spatial.org/" target="_blank">r-spatial.org</a>: a website and blog about using R to analyze spatial and spatio-temporal data.

<a href="http://www.geopackage.org/" target="_blank">GeoPackage</a>: open source, free, cross-platform, easy to use, and compact format for geospatial information -- a <a href="https://www.gis-blog.com/geopackage-vs-shapefile/" target="_blank">newer, better</a> alternative to shapefiles.

( ! ) My choice of, and my opinions on these sources/products, are entirely unsolicited and are based exclusively on my own experience of using them.

