---
title: "COVID-19 Canada Data Explorer"
date: "2020-04-18T00:00:00"
slug: "covid-19-canada-data-explorer"
excerpt: "An interactive dashboard to explore the Government of Canada's official statistics on the spread of the COVID-19 disease. Built with R Shiny, Leaflet, and plotly. Auto-updated every six hours."
status: "publish"
output: hugodown::md_document
categories: 
  - "COVID-19"
  - R-spatial
  - Dashboards
  - Data visualization
tags:
  - "COVID-19"
  - coronavirus
  - "SARS-CoV-2"
  - public health
  - dashboards
  - data visualization
  - Shiny
  - Leaflet
  - plotly
comment_status: open
ping_status: open
rmd_hash: 9a191b2030148a7e

---

<div class="highlight">

<div class="figure" style="text-align: center">

<a href="https://dataenthusiast.ca/apps/covid_ca/" target="_blank"><img src="figs/covid-dashboard-3.png" alt="COVID-19 Canada Data Explorer. Click picture to open." width="100%" /></a>
<p class="caption">
COVID-19 Canada Data Explorer. Click picture to open.
</p>

</div>

</div>

**Update**: the COVID-19 Canada Data Explorer was <a href="https://www.macdonaldlaurier.ca/covid-19-canada-can-meet-global-challenge/" target="_blank">endorsed by the Macdonald-Laurier Institute</a> -- one of Canada's leading public policy think tanks!

**Update 2**: My public policy brief <a href="https://www.schoolofpublicpolicy.sk.ca/research/publications/policy-brief/designing-covid-19-data-tools.php" target="_blank">Designing COVID-19 Data Tools</a>, co-authored with <a href="https://www.schoolofpublicpolicy.sk.ca/about-us/faculty/ken-coates.php" target="_blank">Ken Coates</a> and <a href="https://artsandscience.usask.ca/profile/CHolroyd#/profile" target="_blank">Carin Holroyd</a>, was published by the <a href="https://www.schoolofpublicpolicy.sk.ca/index.php" target="_blank">Johnson Shoyama Graduate School of Public Policy</a>, University of Saskatchewan. The brief uses COVID-19 Canada Data Explorer as an example, and makes a comparative overview of COVID-19 data analysis and visualization tools available from Canada's federal and provincial governments.

**Update 3**: <a href="https://milano-r.github.io/erum2020-covidr-contest/petr-baranovskiy-covid-ca-data-explorer.html" target="_blank"><img src="https://badgen.net/https/runkit.io/erum2020-covidr/badge/branches/master/petr-baranovskiy-covid-ca-data-explorer?cache=300" alt="eRum2020::CovidR" /></a>

This is a more or less final version, so here is the <a href="https://gitlab.com/peterbar/covid_ca" target="_blank">source code</a> for the app. And <a href="https://gitlab.com/peterbar/covid_ca_base" target="_blank">here</a> is the code used to retrieve and pre-process geospatial data.

<p style="text-align: center;">
\*\*\*
</p>

In the times of the pandemic, the data community can help in many ways, including by developing instruments to track and break down the data on the spread of the dreaded coronavirus disease. The <a href="https://dataenthusiast.ca/apps/covid_ca/" target="_blank">COVID-19 Canada Data Explorer app</a> was built with R, including <a href="https://shiny.rstudio.com/" target="_blank"><code>Shiny</code></a>, <a href="https://rstudio.github.io/leaflet/" target="_blank"><code>Leaflet</code></a>, and <a href="https://plotly.com/r/" target="_blank"><code>plotly</code></a>, to process the [official dataset](https://health-infobase.canada.ca/src/data/covidLive/covid19.csv) available from the Government of Canada. They do have <a href="https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html" target="_blank">their own</a> data visualization tool, but it is very basic. You can do so much more with the available data!

The data is downloaded from Canada.ca at 6-hour intervals to minimize the load on the repository -- there is likely a high demand due to the pandemic. This means that there may be a delay of up to six hours from the time Canada.ca update their data to the moment it is updated on my server.

I hope that my app will help public health professionals, policymakers, and really anyone to stay informed about the course of SARS-CoV-2 epidemic in Canada.

