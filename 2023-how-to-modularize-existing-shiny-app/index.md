---
title: 'How to Modularize an Existing Shiny App'
date: '2023-07-15T23:33:00'
slug: 'how-to-modularize-existing-shiny-app'
output: hugodown::md_document
status: 'publish'
excerpt: 'A tutorial on modularizing existing R Shiny apps - how and why do it and what to keep in mind. Differs from many other tutorials in that it follows a top-down approach: instead of using an example of writing a simple modular app from scratch, I took a complex real-life app and broke it down into modules.'
categories:
- Shiny
- Programming
tags:
- Shiny
- Shiny modules
- dashboards
comment_status: open
ping_status: open
rmd_hash: a9f44bb988c84b00

---

## <a name="intro"></a> Introduction

There are multiple tutorials available online on writing modular Shiny apps. So why one more? Well, when I just started with building modular apps myself, these didn't do much for me. So I really only learned how to write modules when I had an opportunity to team up with an experienced R Shiny developer. The reason I guess is that Shiny modules *is* an advanced topic, and you typically get to writing modules only when you finally need to scale your apps - and keep opportunities for further scaling open. This typically means when your app goes into production. By then you probably have already developed multiple apps, and switching over to a way of thinking required to write modules may be challenging. If you don't know what modules are, I recommend starting [here](https://mastering-shiny.org/scaling-modules.html) and then coming back to this post. Otherwise, read on.

So, I decided to try a different approach and instead of building a simple modular app from scratch, to go in the opposite direction by breaking down a complex real-life [app](https://dataenthusiast.ca/apps/calgary_crime/) into modules. Here's the app's original [non-modular code](https://gitlab.com/peterbar/calgary_crime_data_app/-/tree/nonmodular). Note a single [`app.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/nonmodular/app.R) file that contains the entire app. [`static_assets.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/nonmodular/static_assets.R) includes some object definitions which I moved to a separate file for convenience. [`calgary_crime_data_prep.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/nonmodular/calgary_crime_data_prep.R) is not part of the app; it is a data retrieval and cleaning script executed once a month with [cron](https://www.adminschoice.com/crontab-quick-reference). Running the script each time the app launches would have made it extremely slow and would use way too much bandwidth, as the script downloads and processes 150+ Mb of data on each run.

### <a name="why-modules"></a> Why Use Modules, Again?

In short, four main reasons:

1.  To simplify scaling your app by adding more functionality. Would be much easier to write one more module than ading even more code to the 675 lines in the non-modular version of [`app.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/nonmodular/app.R) while also keeping track of all the different IDs in the same [namespace](https://mastering-shiny.org/scaling-modules.html#namespacing).

2.  To re-use the code. This is a big one - modules are *functions*, and as such they abstract some logic and allow to re-use it multiple times instead of copy-pasting the same code. Note how each module is focused on one specific task.

3.  By organizing the code, modules make it easier to reason about the code. Editing, expanding, and debugging a well-structured app is less of a challenge than doing the same with one long, messy sheet of code. Note how much shorter [each module](https://gitlab.com/peterbar/calgary_crime_data_app/-/tree/modular_apps_post/modules) is compared to the single `app.R` file.

4.  You can have multiple developers working simultaneously on the same app, each one working on a specific module.

## <a name="workflow"></a> Workflow

So, I already have an R Shiny app. It's a long sheet of code in the `app.R` file. How do I break it into modules?

### <a name="app-structure"></a> Consider App Structure

First, let's think about how you'd like to structure your app in the most general sense. For example, if your app uses a `navbarPage` layout, it can be structured around tab panels, like [this](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/app.R#L57):

<a name="define-ui"></a>

    # Define UI
    ui <- fluidPage(

      # UI definition begins
      navbarPage(
        title = "Calgary Crime Data Explorer",
        theme = shinythemes::shinytheme("readable"),
        
        # Map UI
        tabPanel(
          title = "Spatial Analysis",
          icon = icon("map-marked-alt"),
          HTML(html_fix), # load Leaflet legend NA positioning fix
          map_ui("map")
        ),
        # Trend UI 
        tabPanel(
          title = "Trend Analysis",
          icon = icon("chart-line"),
          trend_ui("trend")
        ),
        # About UI
        tabPanel(
          title = "About", 
          icon = icon("question"),
          includeMarkdown("about.md")
        )
      )
      
    ) # UI definition ends

That's it, that's the whole UI definition. Everything else is inside `map_ui` and `trend_ui` modules. Same with the server function, which is even shorter:

<a name="define-server"></a>

    server <- function(input, output, session) {
      map_server("map")
      trend_server("trend")
    } 

But that's just the top-level structuring. Remember that you can call modules from within modules! This allows you to structure the app pretty much in any way you choose.

### <a name="reusable-ui-code"></a> Identify Reusable UI Code

To break the "Coder's Block" (like writer's block, but for coders), just look for any place in the app where you have copied and pasted the same code. Instead of copy-pasting, abstract this code's logic as a function. To make things simple, start with shorter code blocks, maybe just repetitive pieces of UI with no server logic in them. [For example](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/category_ui.R):

    category_ui  <- function(id) {
      
      ns <- shiny::NS(id) # Namespace!
      
      selectInput(inputId = ns("my_category"),
                  label = h5("Choose category:"),
                  choices = categories,
                  selected = categories[1])

    }

Now you can use `category_ui()` [wherever](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/map_mod.R#L48) you may need to choose a category:

    ...
    category_ui("map"),
    ...

Moreover, if in the future you decide to create more modules which rely on category selection, you'll be able to just call `category_ui()` instead of copying this whole code block.

One very important thing to keep in mind when writing modules, is that [**modules must be namespaced**](https://mastering-shiny.org/scaling-modules.html#namespacing)! Note `ns("my_category")`. See [namespacing](#namespacing) below for details.

### <a name="standalone-server-functions"></a> Write Standalone Server Functions

The next step is to write standalone server-side functions. By standalone I mean these technically are not complete modules that have both UI and server components, even though they are created with [`shiny::moduleServer()`](https://rdrr.io/pkg/shiny/man/moduleServer.html).

This task is a bit more complex than abstracting parts of the UI, but not by much. First, identify code sections that perform a specific task and convert them into server functions with [`shiny::moduleServer()`](https://rdrr.io/pkg/shiny/man/moduleServer.html). For example, [this function](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/plotly_fn.R#L8) makes an [STL decomposition](https://otexts.com/fpp3/stl.html) plot:

    make_stl_plot <- function(id, dataset, my_area, my_category, plot_title, components) {
      
      moduleServer(id, function(input, output, session) {
        
        filtered_data <- dataset %>% 
          ungroup() %>% # df needs to be ungrouped; else plotly may work incorrectly
          filter(name == my_area, 
                 category == my_category) %>% 
          mutate(month = tsibble::yearmonth(month),
                 month = as.Date(month))
        
          timetk::plot_stl_diagnostics(.data = filtered_data,
                                       .date_var = month,
                                       .value = count,
                                       .title = plot_title,
                                       .line_color = "#1B9E77",
                                       .message = FALSE,
                                       .feature_set = components) %>% 
          layout(margin = list(t = 80, pad = 5)) %>%
          plotly_build()
        
      })
      
    }

The result is a module server function which is [called from another module](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/trend_mod.R#L125):

    ...
    my_plot <- reactive({
      ...
      plotly_obj <- make_stl_plot("trend", 
                                  dataset = crime_dataset(),
                                  my_area = input$my_area,
                                  my_category = input$my_category,
                                  plot_title = plot_title(),
                                  components = input$my_components)
      ...
    })

Note the syntax you use when building functions with [`shiny::moduleServer()`](https://rdrr.io/pkg/shiny/man/moduleServer.html), which can feel confusing: first goes the `function(id, ...)` and then [`shiny::moduleServer()`](https://rdrr.io/pkg/shiny/man/moduleServer.html) is called from within the main `function(id, ...)` call. I.e. you create a module **not** like this:

    # WRONG!
    make_stl_plot <- moduleServer(id, dataset, my_area, my_category, plot_title, components) {...}

But like this:

    make_stl_plot <- function(id, dataset, my_area, my_category, plot_title, components) {

      moduleServer(id, 
                   # your custom function:
                   function(input, output, session) {
                     ... 
                   }
      )
      
    }

Note where the server function arguments go: `function(id, dataset, my_area, my_category, plot_title, components)`. `moduleServer(id, function(input, output, session)` part is standard and should not change. Note the `id` argument: it is required for [namespacing](https://mastering-shiny.org/scaling-modules.html#namespacing) to work and is passed from the main function call to `moduleServer()`. Other arguments -- `dataset, my_area, my_category, plot_title, components` -- are custom arguments and are passed to your custom function (here it is the function that builds an STL plot).

### <a name="write-whole-modules"></a> Write Whole Modules

Finally, let's get to making "proper" modules. These are not too different from server-side functions created with [`shiny::moduleServer()`](https://rdrr.io/pkg/shiny/man/moduleServer.html), but they have *both* UI and server components. One way of thinking about modules is that these are smaller Shiny apps (although they can't be run independently without modification). Note that it is also possible to develop an app independently and then add it as a module to another app, provided it takes the same inputs. For this tutorial, names of [files](https://gitlab.com/peterbar/calgary_crime_data_app/-/tree/modular_apps_post/modules) containing abstracted UI logic end with `_ui.R`, files containing functions end with `_fn.R`, and proper modules end with `_mod.R`. This naming convention is just for the readers' convenience.

To decide which part of your app can be turned into a module, remember that a module should have a self-contained UI and server logic, and should reflect your app's [structure](#app-structure). In this example the structure is based on the app's tabs, so the two proper modules as of the time of writing this are [`map_mod.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/tree/modular_apps_post/modules/map_mod.R) and [`trend_mod.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/tree/modular_apps_post/modules/trend_mod.R).

### <a name="bring-it-together"></a> Bring it All Together

The last and by far the easiest step is to bring it all together in the `app.R` file, which in case of modular apps usually ends up pretty short. You simply need to do three things:

1.  Source modules, functions, and scripts: `dir(path = "modules", full.names = TRUE) |> map(~ source(.))`

2.  Call UI functions in the app's [UI definition](#define-ui), e.g.: `map_ui("map")`.

3.  Call server functions in the app's [server definition](#define-server), e.g.: `map_server("map")`.

## <a name="modular-development-tips"></a> Some Ideas for Modular Apps Development

Finally, here are some tips and things to keep in mind when developing modular apps with R Shiny. Some of these I haven't seen explained elsewhere, and some (e.g. [namespacing](#namespacing)) I'd like to illustrate with examples.

### <a name="namespacing"></a> Namespacing

Remember that modules need to be namespaced:

    trend_ui <- function(id) {
      
      ns <- shiny::NS(id)
      
      tagList(
        ...
        category_ui("trend"),
        checkboxInput(inputId = ns("extract_trends"),
                      label = h5("Extract trend with STL decomposition")),
        ...
      )
    }

This includes *all input and output IDs*, e.g.: `inputId = ns("extract_trends")`, `plotlyOutput(ns("my_plotly"), ...)`, as well as [UIs](#reusable-ui-code) and [standalone server functions](#standalone-server-functions).

Note however [`category_ui("trend")`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/trend_mod.R#L21). Why no `ns()` call? Well, that's because it is already namespaced in [`category_ui.R`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/category_ui.R). And what about [`make_stl_plot("trend", ...)`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/trend_mod.R#L125) function call? It is namespaced at the output ID level: [`plotlyOutput(ns("my_plotly"), ...)`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/trend_mod.R#L41). If you follow the reactive graph backwards from the [`my_plotly` output object](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/trend_mod.R#L147), you will soon get back to `make_stl_plot("trend", ...)`. If this seems confusing, *just remember that you must namespace input and output IDs*.

Also, what is `ns <- shiny::NS(id)`? `ns(id)` is an optional shorthand for `shiny::NS(id)`. If you don't use the shorthand, you'd have to use the full form `NS(id, "name")` each time you need to namespace an ID. I.e. instead of `ns("extract_trends")` you'd need to write `NS(id, "extract_trends")`. This arguably saves you a bit of typing.

### <a name="event-handlers-in-modules"></a> Event Handlers in Modules

Figuring how to use event handlers in modules took me some time, and I wasn't able to find good examples elsewhere. So this might be the most valuable part of this post.

Let's start with a simple [`openReadme()`](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/readme_fn.R) function that opens a markdown file when the user clicks the "Readme" button:

    openReadme <- function(input, output, filename) {
      
        observeEvent(input$readme, ignoreInit = TRUE, {
          showModal(
            modalDialog(
              div(
                tags$head(tags$style(".modal-dialog{width:900px}")),
                fluidPage(includeMarkdown(filename))
              ), easyClose = TRUE
            )
          )
        })
      
    }

The function contains an observer that executes when `input$readme` changes. The input comes from `actionButton(inputId = ns("readme"), ...)`. Note that the `actionButton()` input ID is (and must be) namespaced, while the `openReadme()` function does not have `ns()` anywhere in it. Function call `openReadme(input, output, "readme_trend.md")` also is not expressly namespaced, instead the function takes `input` and `output` arguments, which are standard for server functions. `filename` is a custom argument (name of the file to be opened).

Importantly, unlike other server functions, event handling functions **can not** be created with `moduleServer()`:

    # WRONG!
    openReadme <- function(id, filename) {
      
      moduleServer(id, function(input, output, session) {

        observeEvent(input$readme, ignoreInit = TRUE, {
          showModal(
            modalDialog(
              div(
                tags$head(tags$style(".modal-dialog{width:900px}")),
                fluidPage(includeMarkdown(filename)) # "readme_spatial.md" "readme_trend.md"
              ), easyClose = TRUE
            )
          )
        })
        
      })
      
    }

[Download handlers](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/downloaders_fn.R) are built in the same way:

<a name="app-state"></a>

    downloadHTML <- function(input, output, app_state) {
      output$download_html <-
        downloadHandler(
          filename = function() { paste0(app_state$title(), ".html") },
          content = function(file) { htmlwidgets::saveWidget(app_state$widget(), file = file) }
        )
    }

I am not sure why event handlers can't be made with `moduleServer()`. Maybe they deal with namespacing in some non-standard way? If anyone knows why, please tell me in the comments.

### <a name="passing-reactives-to-nested-modules"></a> Passing Reactives to Nested Modules

Update: following a suggestion in the comments (by Mkranj), I decided to add this short section to focus a bit more on how to pass reactives to nested modules.

With modular apps, you often need to pass reactive values to nested modules, usually from a higher-level (parent) module to an inner (child) module. In that case, you should *not* pass the *value*, but rather pass a *reactive expression*. In practical terms, this means passing the reactive without a parenthesis: `my_reactive` instead of `my_reactive()`. Adding parenthesis causes the reactive to evaluate, and evaluation happens in server scope when the server runs. So if you have `my_reactive()` in the parent module and pass the results of its evaluation to the nested module, the consumer module will receive the *static* value returned from the evaluation of `my_reactive()` in the parent module. But we typically need the child module to be able to use reactive inputs. That's why reactive expressions should be evaluated (ended with a parenthesis) in the final consumer in the nested module. If this sounds a bit abstract, you'll find a detailed example in the [next section](#managing-complex-app-states).

### <a name="managing-complex-app-states"></a> Managing Complex Apps with Multiple Reactives

Did you wonder what is the [`app_state`](#app-state) argument in `downloadHTML <- function(input, output, app_state) {...}`? Creating an app state reactive object is a great way to manage complex apps (inspired by [this post](https://www.r-bloggers.com/2021/07/top-3-coding-best-practices-from-the-shiny-contest/)). Instead of keeping track of multiple reactives, assign them to a list of reactive values, and then you can pass `app_state` to functions instead of passing each reactive separately:

    # Init empty list of reactive values
    app_state <- reactiveValues()

    # Update app_state when reactives change
    observe({
      app_state$title <- my_map_file_title
      app_state$widget <- leaflet_map
      app_state$dataset <- mapping_selection
    }) |> bindEvent(c(my_map_file_title(), leaflet_map(), mapping_selection()))

Note that reactives are *not* evaluated here, and should be passed to consumers **as reactive expressions** that will be evaluated downstream in the consumer function. To illustrate, uncomment [this code block](https://gitlab.com/peterbar/calgary_crime_data_app/-/blob/modular_apps_post/modules/map_mod.R#L289) and run the app. Here's what it will return in the console:

    [1] "Reactive expression (not evaluated):"
    reactive({
        title_string() %>% 
          str_remove_all(c("<.*?>")) %>% 
          str_trim() %>% 
          str_to_lower() %>% 
          str_replace_all(" ", "_")
    }) 

    [1] "Evaluated reactive:"
    [1] "all_crime_counts_by_community_from_january_2017_to_may_2023"

A reactive object is evaluated when it ends with a parenthesis, e.g.: `my_map_file_title()`. Remember that a reactive is a function, an object of class [closure](https://adv-r.hadley.nz/environments.html?q=closure#function-environments). If passed to a consumer function, *evaluated* reactive will be passed as a value, such as character string `"all_crime_counts_by_community_from_january_2017_to_may_2023"`. And since it has already been evaluated once on app launch, the resulting value won't update when inputs change. In a Shiny app we generally do *not* want this behavior because we need the consumer function to use *reactive* inputs. In other words, we need `downloadHTML()` to download what the app *currently* shows instead of a static picture of what it was showing when it launched.

Note how in the `downloadHTML()` consumer function `app_state$title()` and `app_state$widget()` are both evaluated (they end with a parenthesis):

    downloadHTML <- function(input, output, app_state) {
      output$download_html <-
        downloadHandler(
          filename = function() { paste0(app_state$title(), ".html") },
          content = function(file) { htmlwidgets::saveWidget(app_state$widget(), file = file) }
        )
    }

Finally, you may need a way to make sure you correctly pass objects as *evaluated* reactives vs reactive *expressions*, or that you pass objects of proper class or type to server functions[^1]. If you simply rely on Shiny to tell you if you've made a mistake, you'll face rather mysterious error messages. Instead, there's a neat way to check for these with [`stopifnot()`](https://rdrr.io/r/base/stopifnot.html), as described [here](https://mastering-shiny.org/scaling-modules.html?q=namespace#server-inputs).

[^1]: Remember that a module is also a function.

